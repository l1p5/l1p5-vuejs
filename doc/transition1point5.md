# Transition 1point5

Documentation technique concernant le module transition 1point 5.

# Note sur le déploiement

## Appliquer les migrations

```bash
python manage.py migrate
```

# Stockage des fichiers

On autorise le téléversement de fichiers (charte, images ...). Le répertoire de
stockage est controllé par la clé de configuration `MEDIA_ROOT` dans la
configuration de django.


Pour l'instant elle est fixée dans le code à
```
MEDIA_ROOT="django-data"
```

## Politique de téléversement

Deux mécanismes sont mis en place:
- un quota global
- un filtrage sur les types de fichiers

Quelques éléments concernant chacun dans la suite mais l'idée est que leur
vérification se fait à la fois dans la partie cliente et la partie serveur.
Néanmoins le dernier mot est donné au `backend` (mais sans passer par les
`settings` -- exprès).

### Quota

Chaque fois qu'un fichier est lié à une action il consomme un quota égal à sa taille.
Le quota est vérifié à 2 niveaux:
- côté frontend, la sauvegarde d'une action est bloquée si la taille de fichier liés dépasse le quota max
- côté backend, lorsque qu'une action est sauvée le quota est mis à jour. Les
  fichiers qui feraient dépasser le quota maximal sont ignorées.

Le quota courant et maximal sont retourné par une fontion API.
Le quota maximal est hardcodé dans le backend pour le moment (10MB).

### Filtrage de types mime

Chaque fois qu'un fichier est lié à une action son type mime est vérifié
Cela est vérifié à 2 niveaux
- côté frontend,
[`accept`](https://developer.mozilla.org/fr/docs/Web/HTML/Element/Input/file#accept)
est positionné sur la liste des types mime autorisées par les `settings`
- côté backend, lorsque qu'une action est sauvée les type mime des fichiers liés
sont vérifiés. Si un fichier ne respecte pas une liste prédéfinie il est ignoré.

Les type mimes sont fixés à `application/pdf,image/png,image/jpeg` pour
l'instant.  Comme les quota la liste des types mime autorisée sont dupliquée
dans le code actuellement (dans les settings pour la partie `frontend` et
hardcodé pour la partie `backend`)