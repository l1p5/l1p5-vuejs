%entity.name	Lab_OF_l1p5-test-0@l1p5.org
%year	2022
%uuid	9c6b2426-1ded-4ff2-80e3-a956238a6d76
%nb.Chercheurs	10
%nb.Enseignants-Ch.	10
%nb.ITA	10
%nb.Doctorants/Post-Docs	10
name	is.subtotal	submitted	emission.kg.co2e	uncertainty.kg.co2e
Empreinte carbone des bâtiments	true	false	733339	86345	
Empreinte carbone des usages	true	false	568025	24965	
Chauffage	false	false	436378	21334	
Électricité	false	false	129637	12964	
Fluides frigorigènes	false	false	0	0	
Eau	false	false	2010	221	
Empreinte carbone des constructions	false	false	165314	82657	
Empreinte carbone du matériel informatique	false	false	16444	5103	
Empreinte carbone des achats	false	false	1415	314	
Empreinte carbone des déplacements	true	false	41582	11296	
Déplacements domicile-travail	false	false	4428	2657	
Déplacements professionnels	true	false	37154	10979	
Les véhicules	false	false	5193	2104	
Les missions	false	false	31961	10775	
Empreinte carbone de l'alimentation	false	false	7098	2511	
Empreinte carbone des activités de recherche	false	false	699099	197762	
Empreinte carbone totale	true	false	1498978	216160	