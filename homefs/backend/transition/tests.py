from abc import ABCMeta, abstractmethod
from datetime import datetime, timedelta
import io

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, modify_settings, override_settings, tag
from django.test import TestCase
from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status

from pathlib import Path

from backend.carbon.models import GHGI

from backend.test_utils import (
    PermissionsTestCase,
    ensure_entity_created,
    ensure_user_created,
    ensure_lab_created,
    make_authenticated_client,
    modify_conf,
)

from backend.users.models import L1P5Group, L1P5User
from backend.transition.models import (
    DRAFT,
    PUBLISHED,
    SUBMITTED,
    PublicAction,
    PublicActionFile,
    Tag,
)
from backend.core.models.core import Boundary, Location, Members, PositionTitle
from backend.transition.utils import get_counts
from backend.core.models import Laboratory, Entity
from backend.utils import force_init


REQUEST_KWARGS = dict(content_type="application/json")


def make_dummy_boundary():
    pts = PositionTitle.objects.all()
    boundary = Boundary.objects.create(budget=42)
    Location.objects.create(
        name="TestCity",
        country="TestCity",
        latitude=42.0,
        longitude=42.0,
        boundary=boundary,
    )
    # add some members
    for pt in pts:
        m = Members(boundary=boundary, position=pt, number=1)
        m.save()
    return boundary


def make_file_data(name="test_file.pdf", content="test content"):
    f = io.StringIO(content)
    # beware file extension is validated
    f.name = name

    return {f.name: f}


def make_action(entity, admin_status):
    # add an action
    action = PublicAction(
        entity=entity,
        title=f"Test action",
        text=f"Test body action",
    )
    action.save(update_fields=["admin_status"])
    tag, _ = Tag.objects.get_or_create(descriptor="tag:ges:buildings")
    action.tags.set([tag])
    # purpose: get the status back
    action.admin_status = admin_status
    action.save(update_fields=["admin_status"])

    # add a file
    uploaded_file = SimpleUploadedFile(
        "test.pdf", b"plop", content_type="application/pdf"
    )
    file = PublicActionFile(action=action, name="test.pdf", file=uploaded_file)
    file.save()
    return action


class TestPermissionsWrapper:
    class TestPermissions(PermissionsTestCase, metaclass=ABCMeta):
        """Make sure unauthorized client get unexpectedly access to some resources."""

        def setUp(self) -> None:
            super().setUp()
            # create some resource for the user
            self.entity = self.make_resources(self.user)
            self.ghgi = GHGI(entity=self.entity, year=2042)
            self.ghgi.save()

            self.make_members(self.ghgi)

            self.draft_action = make_action(self.entity, DRAFT)
            self.submitted_action = make_action(self.entity, SUBMITTED)
            self.published_action = make_action(self.entity, PUBLISHED)

            # TODO(msimonin): handle a file

        @abstractmethod
        def make_resources(self, user):
            ...

        def make_members(self, ghgi):
            """Dummy but effective, 1 guy for each positiontitle"""
            b = make_dummy_boundary()
            ghgi.boundary = b
            ghgi.save()

        def test_mgmt_public_actions(self):
            self.assert_permissions(
                "/api/transition/actions/",
                authenticated_rc=status.HTTP_200_OK,
                # we return None if the user is authenticated but no laboratory
                authenticated_cb=lambda s, r, d: s.assertFalse(
                    r.content, f"[{d['desc']}] None should be returned"
                ),
            )

        def test_mgmt_public_action_draft(self):
            # Draft
            self.assert_permissions(
                f"/api/transition/actions/{self.draft_action.id}/",
                method="get",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.draft_action.id}/",
                method="post",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.draft_action.id}/",
                method="post",
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_200_OK,
                # we can't delete twice the action so we don't test the following
                reviewer_rc=None,
                admin_rc=None,
            )

        def test_mgmt_public_action_submitted(self):
            # submitted
            self.assert_permissions(
                f"/api/transition/actions/{self.submitted_action.id}/",
                method="get",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.submitted_action.id}/",
                method="post",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.submitted_action.id}/",
                method="post",
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_200_OK,
                # we can't delete twice the action so we don't test the following
                reviewer_rc=None,
                admin_rc=None,
            )

        def test_mgmt_public_action_published(self):
            # submitted
            self.assert_permissions(
                f"/api/transition/actions/{self.published_action.id}/",
                method="get",
                authenticated_rc=status.HTTP_200_OK,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.published_action.id}/",
                method="post",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
            )

            self.assert_permissions(
                f"/api/transition/actions/{self.published_action.id}/",
                method="post",
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_200_OK,
                # we can't delete twice the action so we don't test the following
                reviewer_rc=None,
                admin_rc=None,
            )

        def test_mgmt_public_file(self):
            self.assert_permissions(
                f"/api/transition/actions/{self.published_action.id}/files/",
                method="post",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_201_CREATED,
                reviewer_rc=status.HTTP_201_CREATED,
                admin_rc=status.HTTP_201_CREATED,
            )

        @tag("plop")
        def test_reviewer_get_quota(self):
            self.assert_permissions(
                f"/api/reviewer/quota/{self.entity.id}/",
                method="get",
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_200_OK,
                reviewer_rc=status.HTTP_200_OK,
                admin_rc=status.HTTP_200_OK,
            )

        def test_public_actions_published(self):
            # results are paginated
            one_action_cb = lambda s, r, d: s.assertEqual(
                1, r.json()["count"], f"[{d['desc']}] count must be 1"
            )
            self.assert_permissions(
                f"/api/public/actions/",
                unauthenticated_rc=status.HTTP_200_OK,
                unauthenticated_cb=one_action_cb,
                authenticated_rc=status.HTTP_200_OK,
                authenticated_cb=one_action_cb,
                user_rc=status.HTTP_200_OK,
                user_cb=one_action_cb,
                reviewer_rc=status.HTTP_200_OK,
                reviewer_cb=one_action_cb,
                admin_rc=status.HTTP_200_OK,
                admin_cb=one_action_cb,
            )

        def test_public_action_published(self):
            # if it's a draft >user can get it
            self.assert_permissions(
                f"/api/public/actions/{self.draft_action.id}/",
                unauthenticated_rc=status.HTTP_403_FORBIDDEN,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_200_OK,
                reviewer_rc=status.HTTP_200_OK,
                admin_rc=status.HTTP_200_OK,
            )

            # if it's published anyone can
            self.assert_permissions(
                f"/api/public/actions/{self.published_action.id}/",
                unauthenticated_rc=status.HTTP_200_OK,
                authenticated_rc=status.HTTP_200_OK,
                user_rc=status.HTTP_200_OK,
                reviewer_rc=status.HTTP_200_OK,
                admin_rc=status.HTTP_200_OK,
            )

            # if the action doesn't exist -> 404
            self.assert_permissions(
                f"/api/public/actions/12345/",
                unauthenticated_rc=status.HTTP_404_NOT_FOUND,
                authenticated_rc=status.HTTP_404_NOT_FOUND,
                user_rc=status.HTTP_404_NOT_FOUND,
                reviewer_rc=status.HTTP_404_NOT_FOUND,
                admin_rc=status.HTTP_404_NOT_FOUND,
            )

        def test_public_action_add_file(self):
            data = make_file_data()
            # django uses multipart/form-data by default
            from django.test.client import MULTIPART_CONTENT

            for action in [
                self.draft_action,
                self.submitted_action,
                self.published_action,
            ]:
                self.assert_permissions(
                    f"/api/transition/actions/{action.id}/files/",
                    method="post",
                    data=data,
                    content_type=MULTIPART_CONTENT,
                    # yes that means that a user can add a file on a published action
                    user_rc=status.HTTP_201_CREATED,
                    reviewer_rc=status.HTTP_201_CREATED,
                    admin_rc=status.HTTP_201_CREATED,
                )

        def test_public_action_draft_get_file(self):
            action_id = self.draft_action.id
            # we know there's one file
            file_id = self.draft_action.files.first().id
            self.assert_permissions(
                f"/api/transition/actions/{action_id}/files/{file_id}/"
            )

        def test_public_action_submitted_get_file(self):
            action_id = self.submitted_action.id
            # we know there's one file
            file_id = self.submitted_action.files.first().id
            self.assert_permissions(
                f"/api/transition/actions/{action_id}/files/{file_id}/"
            )

        def test_public_action_published_get_file(self):
            action_id = self.published_action.id
            # we know there's one file
            file_id = self.published_action.files.first().id
            self.assert_permissions(
                f"/api/transition/actions/{action_id}/files/{file_id}/",
                unauthenticated_rc=status.HTTP_200_OK,
                authenticated_rc=status.HTTP_200_OK,
            )

        def test_reviewer_actions_submitted(self):
            self.assert_permissions(
                f"/api/reviewer/actions/",
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_403_FORBIDDEN,
                reviewer_rc=status.HTTP_200_OK,
                reviewer_cb=lambda s, r, _: s.assertEqual(1, len(r.json())),
                admin_rc=status.HTTP_200_OK,
                admin_cb=lambda s, r, _: s.assertEqual(1, len(r.json())),
            )

        def test_reviewer_action_get(self):
            # toggle action status
            self.assert_permissions(
                f"/api/reviewer/actions/{self.draft_action.id}/",
                method="post",
                user_rc=status.HTTP_403_FORBIDDEN,
                reviewer_rc=status.HTTP_403_FORBIDDEN,
                admin_rc=status.HTTP_403_FORBIDDEN,
            )
            self.assert_permissions(
                f"/api/reviewer/actions/{self.submitted_action.id}/",
                method="post",
                user_rc=status.HTTP_403_FORBIDDEN,
                reviewer_rc=status.HTTP_200_OK,
                admin_rc=status.HTTP_200_OK,
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.published_action.id}/",
                method="post",
                user_rc=status.HTTP_403_FORBIDDEN,
                reviewer_rc=status.HTTP_200_OK,
                admin_rc=status.HTTP_200_OK,
            )

        def test_reviewer_action_put(self):
            self.assert_permissions(
                f"/api/reviewer/actions/{self.draft_action.id}/",
                method="put",
            )
            self.assert_permissions(
                f"/api/reviewer/actions/{self.submitted_action.id}/",
                method="put",
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.published_action.id}/",
                method="put",
            )

        def test_reviewer_action_get(self):
            self.assert_permissions(
                f"/api/reviewer/actions/{self.draft_action.id}/",
            )
            self.assert_permissions(
                f"/api/reviewer/actions/{self.submitted_action.id}/",
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.published_action.id}/",
            )

        def test_has_reviewer_permissions(self):
            check_cb = lambda b: lambda s, r, d: s.assertTrue(
                r.json()["has_reviewer_permissions"] == b,
                f"[{d['desc']}] has_reviewer_permissions must be {b}",
            )
            self.assert_permissions(
                f"/api/transition/has_reviewer_permissions/",
                unauthenticated_rc=status.HTTP_200_OK,
                unauthenticated_cb=check_cb(False),
                authenticated_rc=status.HTTP_200_OK,
                authenticated_cb=check_cb(False),
                user_rc=status.HTTP_200_OK,
                user_cb=check_cb(False),
                reviewer_cb=check_cb(True),
                admin_cb=check_cb(True),
            )

        def test_reviewer_messages_draft(self):
            self.assert_permissions(
                f"/api/reviewer/actions/{self.draft_action.id}/messages/",
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.draft_action.id}/messages/",
                method="post",
            )

        def test_reviewer_messages_submitted(self):
            self.assert_permissions(
                f"/api/reviewer/actions/{self.submitted_action.id}/messages/",
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.submitted_action.id}/messages/",
                method="post",
            )

        def test_reviewer_messages_published(self):
            self.assert_permissions(
                f"/api/reviewer/actions/{self.published_action.id}/messages/",
            )

            self.assert_permissions(
                f"/api/reviewer/actions/{self.published_action.id}/messages/",
                method="post",
            )


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
class TestPermissionsLaboratory(TestPermissionsWrapper.TestPermissions):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        laboratory = ensure_lab_created(user)
        return laboratory


@tag("entity")
@modify_conf(ENTITY_CLASS="Entity")
class TestPermissionsEntity(TestPermissionsWrapper.TestPermissions):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        entity = ensure_entity_created(user)
        return entity


class TestHasSubmittedGHGIsWrapper:
    class TestHasSubmittedGHGIs(PermissionsTestCase, metaclass=ABCMeta):
        def setUp(self) -> None:

            # this creates several user
            super().setUp()

            # create some resource for the user
            self.entity = self.make_resources(self.user)
            self.ghgi = GHGI(entity=self.entity, year=2042)
            self.ghgi.save()

        @abstractmethod
        def make_resources(self, user):
            ...

        def test_has_submitted_ghgis_not_submitted(self):
            check_cb = lambda b: lambda s, r, d: s.assertTrue(
                r.json()["has_submitted_ghgis"] == b,
                f"[{d['desc']}] has_submitted_ghgis must be {b}",
            )

            self.assert_permissions(
                f"/api/transition/has_submitted_ghgis/",
                unauthenticated_rc=status.HTTP_200_OK,
                unauthenticated_cb=check_cb(False),
                authenticated_rc=status.HTTP_200_OK,
                authenticated_cb=check_cb(False),
                user_rc=status.HTTP_200_OK,
                user_cb=check_cb(False),
                reviewer_cb=check_cb(False),
                admin_cb=check_cb(False),
            )

        def test_has_submitted_one_submitted(self):
            check_cb = lambda b: lambda s, r, d: s.assertTrue(
                r.json()["has_submitted_ghgis"] == b,
                f"[{d['desc']}] has_submitted_ghgis is not {b}",
            )

            modules = {
                "electricity": False,
                "heatings": False,
                "refrigerants": False,
                "water": False,
                "construction": False,
                "commutes": False,
                "travels": False,
                "devices": False,
                "purchases": False,
                "vehicles": False,
            }

            for module in modules.keys():
                submitted = dict(**modules)
                submitted[module] = True
                # create a new ghgi and submit some of its modules
                ghgi = GHGI(
                    entity=self.entity,
                    year=2042,
                    submitted=submitted,
                )
                ghgi.save()

                # one submitted module => True for the user and False for the other user
                self.assert_permissions(
                    f"/api/transition/has_submitted_ghgis/",
                    unauthenticated_rc=status.HTTP_200_OK,
                    unauthenticated_cb=check_cb(False),
                    authenticated_rc=status.HTTP_200_OK,
                    authenticated_cb=check_cb(False),
                    user_rc=status.HTTP_200_OK,
                    user_cb=check_cb(True),
                    reviewer_cb=check_cb(False),
                    admin_cb=check_cb(False),
                )

                ghgi.delete()


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
class TestHasSubmittedGHGIsLaboratory(
    TestHasSubmittedGHGIsWrapper.TestHasSubmittedGHGIs
):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        return ensure_lab_created(user)


@tag("entity")
@modify_conf(ENTITY_CLASS="Entity")
class TestHasSubmittedGHGIsEntity(TestHasSubmittedGHGIsWrapper.TestHasSubmittedGHGIs):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        return ensure_entity_created(user)


class TestActionOnActionWrapper:
    class TestActionOnAction(TestCase, metaclass=ABCMeta):
        def setUp(self) -> None:
            self.user, self.api_user = make_authenticated_client("user@l1p5.org")

            self.entity = self.make_resources(self.user)

            # a reviewer
            self.user_reviewer, self.api_reviewer = make_authenticated_client(
                "reviewer@l1p5.org"
            )
            reviewer_group = L1P5Group.objects.get(name="reviewer")
            self.user_reviewer.groups.set([reviewer_group])
            # refresh permission cache
            self.user_reviewer = L1P5User.objects.get(email=self.user_reviewer.email)

        @abstractmethod
        def make_resources(self, user):
            ...

        def test_reminder_in_the_past(self):
            # user creates an action
            data = dict(title="plop", text="plip")
            response = self.api_user.post(
                reverse("mgmt-public-actions"), data, **REQUEST_KWARGS
            )
            action = response.json()

            # submit the action
            self.api_user.post(reverse("mgmt-public-action", args=[action["id"]]))

            # it appears on the list of action to be reviewed
            response = self.api_reviewer.get(reverse("reviewer-actions"))
            submitted_actions = response.json()
            self.assertTrue(action["id"] in [a["id"] for a in submitted_actions])

            # set a reminder in the past
            data = dict(
                reminder=(datetime.now() - timedelta(weeks=3)).strftime("%Y-%m-%d")
            )
            response = self.api_reviewer.post(
                reverse("reviewer-reminder", args=[action["id"]]),
                data,
                **REQUEST_KWARGS,
            )

            # it's not on the action to be reviewed
            response = self.api_reviewer.get(reverse("reviewer-actions"))
            submitted_actions = response.json()
            self.assertFalse(action["id"] in [a["id"] for a in submitted_actions])

            # but on the reminder list
            response = self.api_reviewer.get(reverse("reviewer-reminders"))
            reminded_actions = response.json()
            self.assertTrue(action["id"] in [a["id"] for a in reminded_actions])

            # but if user submit again the action it goes back to the reviewable actions
            self.api_user.post(reverse("mgmt-public-action", args=[action["id"]]))

            response = self.api_reviewer.get(reverse("reviewer-actions"))
            submitted_actions = response.json()
            self.assertTrue(action["id"] in [a["id"] for a in submitted_actions])

            # and not on the reminder list
            response = self.api_reviewer.get(reverse("reviewer-reminders"))
            reminded_actions = response.json()
            self.assertFalse(action["id"] in [a["id"] for a in reminded_actions])

        def test_reminder_in_the_future(self):
            # user creates an action
            data = dict(title="plop", text="plip")
            response = self.api_user.post(
                reverse("mgmt-public-actions"), data, **REQUEST_KWARGS
            )
            action = response.json()

            # submit the action
            self.api_user.post(reverse("mgmt-public-action", args=[action["id"]]))

            # it appears on the list of action to be reviewed
            response = self.api_reviewer.get(reverse("reviewer-actions"))
            submitted_actions = response.json()
            self.assertTrue(action["id"] in [a["id"] for a in submitted_actions])

            # set a reminder in the future
            data = dict(
                reminder=(datetime.now() + timedelta(weeks=3)).strftime("%Y-%m-%d")
            )
            response = self.api_reviewer.post(
                reverse("reviewer-reminder", args=[action["id"]]),
                data,
                **REQUEST_KWARGS,
            )

            # it's not on the action to be reviewed
            response = self.api_reviewer.get(reverse("reviewer-actions"))
            submitted_actions = response.json()
            self.assertFalse(action["id"] in [a["id"] for a in submitted_actions])

            # but still in the reminder list
            response = self.api_reviewer.get(reverse("reviewer-reminders"))
            reminded_actions = response.json()
            self.assertTrue(action["id"] in [a["id"] for a in reminded_actions])

        def test_upload_delete_file(self):
            data = dict(title="plop", text="plip")
            response = self.api_user.post(
                f"/api/transition/actions/", data, **REQUEST_KWARGS
            )
            self.assertEqual(status.HTTP_201_CREATED, response.status_code)
            r = response.json()
            self.assertEqual("plop", r["title"])
            self.assertEqual("plip", r["text"])
            self.assertEqual("adminstatus:draft", r["admin_status"])

            # get the created action
            response = self.api_user.get(f"/api/transition/actions/{r['id']}/")
            self.assertEqual(status.HTTP_200_OK, response.status_code)
            r = response.json()
            self.assertEqual("plop", r["title"])
            self.assertEqual("plip", r["text"])

            # Upload a file
            name = "test.pdf"
            content = "test content"
            data = make_file_data(name=name, content=content)
            # django uses multipart/form-data by default
            from django.test.client import MULTIPART_CONTENT

            response = self.api_user.post(
                f"/api/transition/actions/{r['id']}/files/",
                data,
                content_type=MULTIPART_CONTENT,
            )
            self.assertEqual(status.HTTP_201_CREATED, response.status_code)
            r = response.json()
            self.assertEqual(1, len(r["files"]))

            # test that the file exist on the fs on the right place
            from django.conf import settings

            filepath = (
                Path(settings.MEDIA_ROOT)
                / f"{self.entity.id}"
                / "actions"
                / f"{r['id']}"
                / name
            )
            self.assertTrue(filepath.exists())
            self.assertEqual(content, filepath.read_text())

            # test that the file has been removed from the fs
            response = self.api_user.delete(
                f"/api/transition/actions/{r['id']}/files/{r['files'][0]['id']}/",
                **REQUEST_KWARGS,
            )
            self.assertEqual(status.HTTP_200_OK, response.status_code)
            self.assertFalse(filepath.exists())

        def test_counts(self):
            response = self.api_reviewer.get(
                reverse("reviewer-counts"), **REQUEST_KWARGS
            )
            r = response.json()
            self.assertDictEqual(
                dict(reminded=0, reminded_late=0, reviewable=0, published=0), r
            )

            # user creates an action
            data = dict(title="plop", text="plip")
            response = self.api_user.post(
                reverse("mgmt-public-actions"), data, **REQUEST_KWARGS
            )
            action = response.json()

            # submit the action
            self.api_user.post(reverse("mgmt-public-action", args=[action["id"]]))

            response = self.api_reviewer.get(
                reverse("reviewer-counts"), **REQUEST_KWARGS
            )
            r = response.json()
            self.assertDictEqual(
                dict(reminded=0, reminded_late=0, reviewable=1, published=0), r
            )

            # set a reminder in the past
            data = dict(
                reminder=(datetime.now() - timedelta(weeks=3)).strftime("%Y-%m-%d")
            )
            response = self.api_reviewer.post(
                reverse("reviewer-reminder", args=[action["id"]]),
                data,
                **REQUEST_KWARGS,
            )

            response = self.api_reviewer.get(
                reverse("reviewer-counts"), **REQUEST_KWARGS
            )
            r = response.json()
            self.assertDictEqual(
                dict(reminded=1, reminded_late=1, reviewable=0, published=0), r
            )

            # reviewer publish
            _ = self.api_reviewer.post(
                reverse("reviewer-one-action", args=[action["id"]])
            )

            # still in reminded but published
            response = self.api_reviewer.get(
                reverse("reviewer-counts"), **REQUEST_KWARGS
            )
            r = response.json()
            self.assertDictEqual(
                dict(reminded=1, reminded_late=1, reviewable=0, published=1), r
            )

        def tearDown(self) -> None:
            self.user.delete()


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
@override_settings(L1P5_ENTITY_CLASS="Laboratory", L1P5_BOUNDARY_IS_EVENT=False)
class TestActionOnActionLaboratory(TestActionOnActionWrapper.TestActionOnAction):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        lab = ensure_lab_created(user)
        return lab


@tag("entity")
@modify_conf(ENTITY_CLASS="Entity")
class TestActionOnActionEntity(TestActionOnActionWrapper.TestActionOnAction):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        entity = ensure_entity_created(user)
        return entity


class TestPublicEntitiesWrapper:
    class TestPublicEntities(PermissionsTestCase, metaclass=ABCMeta):
        def setUp(self) -> None:
            super().setUp()
            # create a user with a ghgi
            # a user with some resources
            self.entity = self.make_resources(self.user)

        @abstractmethod
        def make_resources(self, user):
            ...

        def test_public_entities(self):
            response = self.api_unauthenticated.get(
                reverse("get_public_entities"), **REQUEST_KWARGS
            )
            entities = response.json()
            self.assertCountEqual([], entities)

            make_action(self.entity, admin_status=PUBLISHED)
            response = self.api_unauthenticated.get(
                reverse("get_public_entities"), **REQUEST_KWARGS
            )
            entities = response.json()
            self.assertEqual(1, len(entities))

            # for
            response = self.api_unauthenticated.get(
                reverse("get_public_entities"), **REQUEST_KWARGS
            )
            entities = response.json()
            self.assertEqual(1, len(entities))


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
class TestPublicEntitiesLaboratory(TestPublicEntitiesWrapper.TestPublicEntities):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        return ensure_lab_created(user)


class TestGetCountWrapper:
    class TestGetCount(TestCase):
        @abstractmethod
        def make_resources(self):
            ...

        def test_get_counts(self):
            c = get_counts()
            self.assertDictEqual(dict(nActions=0, nEntities=0), c)

            # just an entity (no action)
            entity = self.make_resources()
            c = get_counts()
            self.assertDictEqual(dict(nActions=0, nEntities=0), c)

            # create an action
            action = PublicAction(
                entity=entity,
                title=f"Test action",
                text=f"Test body action",
            )
            action.save(update_fields=["admin_status"])
            c = get_counts()
            self.assertDictEqual(dict(nActions=0, nEntities=0), c)

            action.admin_status = PUBLISHED
            action.save(update_fields=["admin_status"])

            c = get_counts()
            self.assertDictEqual(dict(nActions=1, nEntities=1), c)


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
class TestGetCountLaboratory(TestGetCountWrapper.TestGetCount):
    def setUp(self):
        force_init()
        super().setUp()

    def make_resources(self):
        u = ensure_user_created("test@test.com")
        e = ensure_lab_created(u)
        return e


@tag("entity")
@modify_conf(ENTITY_CLASS="Entity")
class TestGetCountEntity(TestGetCountWrapper.TestGetCount):
    def setUp(self):
        force_init()
        super().setUp()

    def make_resources(self):
        u = ensure_user_created("test@test.com")
        e = ensure_entity_created(u)
        return e
