import datetime
import logging
import os


from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.dispatch import receiver
from django.forms import ValidationError
from ..core.models import Laboratory

# django uses this
import mimetypes

logger = logging.getLogger(__name__)

ACCEPTED_MIME_TYPES = ["application/pdf", "image/jpeg", "image/png", "application/xml"]

REVIEWER_GROUP_NAME = "reviewer"
_CAN_REVIEW = "can_review"
# fqdn of the permissions use it when using has_perm
CAN_REVIEW = f"transition.{_CAN_REVIEW}"

DEFAULT_PLAN_NAME = "plan d'action"

# should match the descriptors in the frontend app
# see src/models/transition/constants.js
DRAFT = "adminstatus:draft"
SUBMITTED = "adminstatus:submitted"
PUBLISHED = "adminstatus:published"

ADMIN_STATUS_CHOICES = [
    (DRAFT, "Draft"),
    (SUBMITTED, "Submitted"),
    (PUBLISHED, "Published"),
]

# Our custom mimetypes
# .lss is limesurveys file which is xml file
mimetypes.add_type("application/xml", ".lss", strict=True)

# quota - hardcoded here on purpose
# FIXME(msimonin): make it a configuration setting
ONE_MB = 2**20
QUOTA_GLOBAL = 50 * ONE_MB
QUOTA_ACTION = 5 * ONE_MB


# get the files quota for a given lab
def global_quota(entity):
    lab = entity.__class__.objects.get(id=entity.id)
    quota_public_actions = sum([a.quota for a in lab.actions.iterator()])
    return dict(
        current=quota_public_actions,
        quota_global=QUOTA_GLOBAL,
        quota_action=QUOTA_ACTION,
    )


def global_quota_empty():
    return dict(current=0, quota_global=QUOTA_GLOBAL, quota_action=QUOTA_ACTION)


def upload_to(action_file, filename):
    return f"{action_file.action.descriptor}/{filename}"


def validate_filename(filename):
    mime, _ = mimetypes.guess_type(filename)
    if mime not in ACCEPTED_MIME_TYPES:
        raise ValidationError(f"Wrong filetype for {filename}")


def mime_type_validator(value):
    validate_filename(value.file.name)


class Tag(models.Model):
    # doc: unique implies a db_index
    descriptor = models.CharField(max_length=100, null=False, blank=False, unique=True)

    # keep track where the tag first appeared (just in case)
    created = models.DateField(auto_now_add=True)


class PublicAction(models.Model):
    title = models.CharField(max_length=100, default=DEFAULT_PLAN_NAME)
    text = models.TextField(null=False, blank=False)
    created = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)

    # start date is mandatory
    start = models.DateField(null=False, blank=False, default=datetime.date(2023, 9, 1))
    # end date depends whether the action is ... ended
    end = models.DateField(null=True, blank=True)

    # wheter the action has been terminated but abandoned
    abandoned = models.BooleanField(null=False, default=False)

    # contact: might differ from the referent email
    contact = models.EmailField(max_length=190, null=True, blank=True)

    # space separated of frontend tags
    # see src/models/transition/constants.js
    tags = models.ManyToManyField(Tag, related_name="actions")

    # define the generic entity (laboratory, ...)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField()
    entity = GenericForeignKey("content_type", "object_id")

    reviewer = models.ForeignKey(
        "users.L1P5User",
        related_name="assigned_actions",
        null=True,
        on_delete=models.SET_NULL,
    )

    # reminder date (for reviewer)
    reminder = models.DateField(null=True, blank=True)

    admin_status = models.CharField(
        max_length=50, choices=ADMIN_STATUS_CHOICES, default=DRAFT, db_index=True
    )

    def save(self, *args, update_fields=None, **kwargs):
        """Save an action.

        Reset the valid flag automatically
        """
        if update_fields is None or "admin_status" not in update_fields:
            self.admin_status = DRAFT
        super().save(*args, **kwargs)

    @property
    def quota(self):
        # missing files counts as zero !
        q = 0
        for af in self.files.iterator():
            try:
                q = q + af.file.size
            except Exception as e:
                logger.error(f"counting the size of the file as 0 due to {e}")
        return q

    @property
    def descriptor(self):
        return f"{self.entity.id}/actions/{self.id}"

    @property
    def owner(self):
        return self.entity.referent

    @property
    def display_contact(self):
        if not self.contact:
            return self.owner.email
        return self.contact


class PublicActionFile(models.Model):
    action = models.ForeignKey(
        PublicAction, related_name="files", on_delete=models.CASCADE, null=False
    )
    # accept mime types could land in settings eventually
    # but we hardcode them here in purpose

    name = models.CharField(max_length=100)
    file = models.FileField(upload_to=upload_to, validators=[mime_type_validator])

    @property
    def owner(self):
        raise NotImplemented

    @property
    def admin_status(self):
        raise NotImplemented

    @property
    def descriptor(self):
        return upload_to(self, self.name)

    @property
    def owner(self):
        return self.action.owner

    @property
    def admin_status(self):
        return self.action.admin_status

    class Meta:
        constraints = [
            models.UniqueConstraint(
                name="unique_public_action_file_name", fields=["action", "name"]
            )
        ]


class Message(models.Model):
    origin = models.CharField(max_length=100, null=False, blank=False)
    date = models.DateField(auto_now_add=True)
    message = models.CharField(max_length=10000)

    action = models.ForeignKey(
        PublicAction, related_name="messages", on_delete=models.CASCADE, null=False
    )


# see https://stackoverflow.com/questions/16041232/django-delete-filefield
# django don't delete associated files
# https://docs.djangoproject.com/en/1.11/ref/models/fields/#django.db.models.fields.files.FieldFile.delete
@receiver(models.signals.post_delete, sender=PublicActionFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    try:
        if instance.file:
            if os.path.isfile(instance.file.path):
                os.remove(instance.file.path)
    except Exception as e:
        if instance.file:
            logger.error(f"Failed to remove {instance.file.path} from FS", e)
        logging.error("auto_delete_file_on_delete failed")
