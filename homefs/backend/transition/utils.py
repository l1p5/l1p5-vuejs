import logging
from typing import Union

from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from backend.users.models import L1P5User
from backend.transition.models import (
    CAN_REVIEW,
    PUBLISHED,
    SUBMITTED,
    PublicAction,
    PublicActionFile,
)
from backend.utils import get_entity_type
from backend.core.models.core import Entity
from backend.core.models.laboratory import Laboratory


logger = logging.getLogger(__name__)


ORIGIN = "noreply@labos1point5.org"


def fail_silently(f):
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            logger.error(f"{f.__name__} raised {e}, but proceeding")

    return wrapped


def can_get(
    user: L1P5User, a: Union[PublicAction, PublicActionFile], allow_public=False
):
    if allow_public and a.admin_status == PUBLISHED:
        return True

    if a.owner == user or user.has_perm(CAN_REVIEW):
        return True

    return False


def can_act(user: L1P5User, a: Union[PublicAction, PublicActionFile]):
    """make sure public action are returned only if the user is the owner or reviewer."""
    return can_get(user, a, allow_public=False)


def create_add_message(host, action):
    """Email body sent when a message has been added to an action.

    This is received either by the reviewer or the user's action.
    """
    subject = "[T1P5] Un nouveau message est arrivé"
    body = render_to_string("add_message.html", context=dict(host=host, id=action.id))
    return subject, body


def create_submit_message(host, action):
    """Email body sent when an action has been submitted.

    This is received only by one of several reviewers
    """
    subject = f"[T1P5] Un nouvelle action a été soumise ({action.title})"
    body = render_to_string("submit_action.html", context=dict(host=host, id=action.id))
    return subject, body


def create_publish_action(host, action):
    """Email body sent when an action has been published

    This is received only by the author of the action
    """
    subject = f"[T1P5] Votre action ({action.title}) a été publiée"
    body = render_to_string(
        "publish_action.html", context=dict(host=host, id=action.id, title=action.title)
    )
    return subject, body


def get_reviewers_emails(action):
    to = []
    if action.reviewer:
        to = [action.reviewer]
    else:
        reviewers = L1P5User.objects.filter(groups__name="reviewer")
        to = [r.email for r in reviewers]

    return to


def notify_user_add_message(request, action):
    """user -> reviewer(s)"""
    # distinguish between two cases:
    # one reviewer is assigned: send a single mail
    # no reviewer is assigned: send mass mail to all reviewers

    subject, body = create_add_message(request.get_host(), action)
    # avoid sending as the user (can be confusing from the user perspective)
    # origin = action.display_contact
    origin = ORIGIN
    recipients = get_reviewers_emails(action)

    email = EmailMultiAlternatives(subject, body, origin, to=recipients)
    # TODO(msimonin): sends true HTML
    # (or no HTML at all)
    email.attach_alternative(body, "text/html")
    email.send(fail_silently=True)


@fail_silently
def notify_reviewer_add_message(request, action):
    """reviewer -> user"""
    subject, body = create_add_message(request.get_host(), action)

    recipients = [action.display_contact]
    origin = ORIGIN

    email = EmailMultiAlternatives(subject, body, origin, to=recipients)
    # TODO(msimonin): sends true HTML
    # (or no HTML at all)
    email.attach_alternative(body, "text/html")
    email.send(fail_silently=True)


def notify_submit_action(request, action):
    """user -> reviewer(s)"""
    subject, body = create_submit_message(request.get_host(), action)
    origin = ORIGIN
    recipients = get_reviewers_emails(action)
    email = EmailMultiAlternatives(subject, body, origin, to=recipients)
    # TODO(msimonin): sends true HTML
    # (or no HTML at all)
    email.attach_alternative(body, "text/html")
    email.send(fail_silently=True)


def notify_publish_action(request, action):
    """reviewer(s) -> user"""
    subject, body = create_publish_action(request.get_host(), action)
    origin = ORIGIN
    recipients = [action.display_contact]

    email = EmailMultiAlternatives(subject, body, origin, to=recipients)
    # TODO(msimonin): sends true HTML
    # (or no HTML at all)
    email.attach_alternative(body, "text/html")
    email.send(fail_silently=True)


def related_query_entity(entity_type):
    # Instead of using generic relation here
    # we could filter on the content type id
    # filter(content_type=ContentType.objects.get_for_model(EntityClass).id)
    qe = lambda x: x
    if entity_type == Laboratory:
        qe = lambda x: f"thelaboratory__{x}"
    elif entity_type == Entity:
        qe = lambda x: f"theentity__{x}"
    else:
        raise ValueError("Unknown entity type")

    return qe


def get_counts():
    """
    Returns:
    {
        nActions: number of published actions for the current entity
        nEntities: number of Entities with at least one published action
    }
    """
    EntityType = get_entity_type()

    return dict(
        nActions=PublicAction.objects.filter(
            admin_status=PUBLISHED,
            content_type=ContentType.objects.get_for_model(EntityType).id,
        ).count(),
        nEntities=EntityType.objects.select_related("actions")
        .filter(actions__admin_status=PUBLISHED)
        .distinct()
        .count(),
    )
