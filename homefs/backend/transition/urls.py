"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.urls import path
from . import views as transitionviews

URL_PATTERNS = [
    ## user: actions on plans
    path(
        "api/transition/quota/",
        transitionviews.get_transition_quota,
        name="get_transition_quota",
    ),
    path("api/transition/get_count/", transitionviews.get_count, name="get_count"),
    path(
        "api/transition/has_reviewer_permissions/",
        transitionviews.has_reviewer_permissions,
        name="has_reviewer_permissions",
    ),
    ## user: actions on public actions
    path(
        "api/transition/actions/",
        transitionviews.mgmt_public_actions,
        name="mgmt-public-actions",
    ),
    path(
        "api/transition/actions/<int:action_id>/",
        transitionviews.mgmt_public_action,
        name="mgmt-public-action",
    ),
    path(
        "api/transition/actions/<int:action_id>/files/",
        transitionviews.mgmt_public_files,
        name="mgmt-public-files",
    ),
    path(
        "api/transition/actions/<int:action_id>/files/<int:file_id>/",
        transitionviews.mgmt_public_file,
        name="mgmt-public-file",
    ),
    ## public: read only on public actions
    path(
        "api/public/actions/",
        transitionviews.public_actions_published,
        name="get_published_initiatives",
    ),
    path(
        "api/public/actions/<int:action_id>/",
        transitionviews.public_action_published,
        name="get_one_published_initiative",
    ),
    path(
        "api/public/entities/",
        transitionviews.public_entities,
        name="get_public_entities",
    ),
    ## reviewer/admin actions
    path(
        "api/reviewer/quota/<int:entity_id>/",
        transitionviews.reviewer_get_quota,
        name="review-get-quota",
    ),
    path(
        "api/reviewer/actions/",
        transitionviews.reviewer_actions_submitted,
        name="reviewer-actions",
    ),
    path(
        "api/reviewer/reminders/",
        transitionviews.reviewer_reminders,
        name="reviewer-reminders",
    ),
    path(
        "api/reviewer/actions/my/",
        transitionviews.reviewer_my_actions,
        name="reviewer-my-actions",
    ),
    path(
        "api/reviewer/actions/<int:action_id>/",
        transitionviews.reviewer_action,
        name="reviewer-one-action",
    ),
    path(
        "api/reviewer/actions/<int:action_id>/reminder/",
        transitionviews.reviewer_reminder,
        name="reviewer-reminder",
    ),
    path(
        "api/reviewer/actions/<int:action_id>/messages/",
        transitionviews.reviewer_messages,
        name="reviewer-messages",
    ),
    path(
        "api/reviewer/counts/", transitionviews.reviewer_counts, name="reviewer-counts"
    ),
    # move me to carbon once merged into master
    path(
        "api/transition/has_submitted_ghgis/",
        transitionviews.has_submitted_ghgis,
        name="has-submitted-ghgis",
    ),
]
