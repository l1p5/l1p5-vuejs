from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework.permissions import BasePermission

from backend.core.models import Settings
from backend.core.models.core import Entity, PositionTitle
from backend.core.models.laboratory import Laboratory


MANDATORY_CONFIGURATION_KEYS = [
    "L1P5_ACTIVE_MODULES",
    "L1P5_ACTIVE_APPS",
    "L1P5_WARNING_YEARS",
    "L1P5_MODULES_COLOR",
    "L1P5_GEONAMES_USERNAME",
    "L1P5_ALLOWED_COUNTRIES",
    "L1P5_ENTITY_CLASS",
    "L1P5_NUMBER_OF_WORKED_WEEKS",
    "L1P5_FILE_FORMATS",
    "L1P5_ACCEPTED_MIME_TYPES",
    "L1P5_POSITION_TITLES",
    "L1P5_CF_LABEL_MAPPING",
    "L1P5_TRAVEL_POSITION_LABELS",
    "L1P5_TRAVEL_POSITION_DICTIONARY",
    "L1P5_I18N",
]


class EntityNotSet(Exception):
    ...


class EntityUnknown(Exception):
    ...


class hasSuperPower(BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and (request.user.is_staff or request.user.is_superuser)
        )


def entity_get(**kwargs):
    entity = entity_filter(**kwargs)
    return entity.first()


def get_entity_or_404(**kwargs):
    """Util function that mimics get_object_or_404 function."""
    entity = entity_get(**kwargs)
    if entity is None:
        raise Http404("No entity found.")
    return entity


def get_entity_type():
    """Get the entity type based on the settings

    Raises if the entity can't be found
    """
    try:
        t = Settings.objects.get(name="ENTITY_CLASS", section="global").value
    except ObjectDoesNotExist:
        raise EntityNotSet()
    if t == "Laboratory":
        return Laboratory
    elif t == "Entity":
        return Entity
    else:
        raise EntityUnknown()


def entity_filter(**kwargs):
    EntityClass = get_entity_type()
    return EntityClass.objects.filter(**kwargs)


def force_init():
    # this forces all the settings
    force_settings = [s["name"] for s in settings.L1P5_SETTINGS]
    return init(force_settings)


def init(force_settings):
    """Initialize the app. Idempotent.

    Read the configuration and create the initial resources
    Must be called prior to starting the app.
    """
    # create/update the initial settings (idempotent)
    new_settings, updated_settings = [], []
    for setting in settings.L1P5_SETTINGS:
        s = Settings.objects.filter(section=setting["section"], name=setting["name"])
        if not s:
            # create it regardless if it's forced
            Settings.objects.create(**setting)
            created = True
            new_settings.append(setting["name"])
        elif s.first().name in force_settings:
            # we have a setting that need to be forced
            s.update(**setting)
            updated_settings.append(setting["name"])

    # position tile is a bit special since it triggers some further updates
    # so we apply the following only if this special setting has been touched
    new_pt, updated_pt = [], []
    if "POSITION_TITLES" not in new_settings + updated_settings:
        return new_pt, updated_pt, new_settings, updated_settings

    # it has potentially changed so we create/update the associated data in the
    # DB
    for pt in Settings.get_value(name="POSITION_TITLES"):
        pt, created = PositionTitle.objects.update_or_create(
            name=pt["name"], defaults=dict(quotity=pt["quotity"])
        )
        if created:
            new_pt.append(pt.name)
        else:
            updated_pt.append(pt.name)

    return new_pt, updated_pt, new_settings, updated_settings
