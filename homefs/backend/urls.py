import django.contrib.auth.views as auth_views
from django.conf import settings
from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import OperationalError, ProgrammingError
from django.urls import include, path, re_path
from rest_framework import routers

from backend.core.models import Settings

from .carbon import urls as carbonurls
from .core import views as coreviews
from .job import views as jobviews
from .scenario import urls as scenariourls
from .transition import urls as transitionurls
from .users import views as usersviews

import logging

LOGGER = logging.getLogger(__name__)

router = routers.DefaultRouter()

urlpatterns = [

    # admin
    path('django-admin/', admin.site.urls),

    # http://localhost:8000/
    path('', coreviews.index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # handle users
    path('api/is_super_user/', usersviews.is_super_user, name='is_super_user'),
    path('api/get_users/', usersviews.get_users, name='get_all_users'),
    path('api/update_roles/', usersviews.update_roles, name='update_permissions'),
    path('api/update_is_admin/', usersviews.update_is_admin, name='update_is_admin'),
    path('api/update_user/', usersviews.update_user, name='update_user'),
    path('api/delete_user/', usersviews.delete_user, name='delete_user'),
    path('api/send_activation_email/', usersviews.send_activation_email, name='send_activation_email'),
    path('api/auth/registration/', usersviews.RegisterView.as_view()),
    path('api/user_exists/', usersviews.user_exists, name='user_exists'),
    path('api/reset_password/', usersviews.PasswordResetView.as_view(), name='rest_password_reset'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('accounts/activate_account/<uidb64>/<token>/', usersviews.activate_account, name='activate_account'),

    # core module
    path('api/get_settings/', coreviews.get_settings, name='get_settings'),
    path('api/save_settings/', coreviews.save_settings, name='save_settings'),
    path('api/save_entity/', coreviews.save_entity, name='save_entity'),
    path('api/get_entity/', coreviews.get_entity, name='get_entity'),    
    path('api/get_all_tag_categories/', coreviews.get_all_tag_categories, name='get_all_tag_categories'),    
    path('api/update_or_create_tag_category/', coreviews.update_or_create_tag_category, name='update_or_create_tag_category'),    
    path('api/delete_tag_category/', coreviews.delete_tag_category, name='delete_tag_category'),    
    path('api/get_administrations/', coreviews.get_administrations, name='get_administrations'),
    path('api/get_disciplines/', coreviews.get_disciplines, name='get_disciplines'),

    # job api
    path('api/jobs/', jobviews.jobs, name='jobs'),
    path('api/jobs/<int:job_id>/', jobviews.one_job, name='one_job'),

    # authentication and JWT Token
    path('api/token/', usersviews.L1P5TokenObtainPairView.as_view(), name='token_obtain_pair'),
    #path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Entry point of the VueJS web application
    re_path(r'^.*$', coreviews.index_view, name='entry_point')
]

active_apps = []

# Here we rely on the database to know which "apps" are available 
# Some app map a django application in this case we disable here the
# corresponding routes. We don't rely on the django INSTALLED_APPS
# We lookup in the Settings database to known which apps are configured
# but in some situations this isn't possible so we fallback in an empty list.
try:
    active_apps = Settings.objects.get(name="ACTIVE_APPS", section="global").value
except OperationalError as e:
    # Possible reason:
    # - the l1p5_settings table hasn't be created yet (migrate hasn't run yet)
    LOGGER.info("Active Apps not found, falling back")
except ObjectDoesNotExist as e:
    # Posible reason:
    # - the l1p5_settings is not populated (init hasn't been run)
    LOGGER.info("Active Apps not found, falling back")
except ProgrammingError as e:
    # Posible reason:
    # - the l1p5_settings is not populated (init hasn't been run)
    LOGGER.info("Active Apps not found, falling back")

if active_apps:
    # Dynamicaly add path from active apps
    for app in filter(lambda app: app.startswith('application/'), active_apps):
        appname = app.split('/')[1]
        urlpatterns = globals()[appname + 'urls'].URL_PATTERNS + urlpatterns
else :
    urlpatterns = carbonurls.URL_PATTERNS + urlpatterns
    urlpatterns = scenariourls.URL_PATTERNS + urlpatterns
    urlpatterns = transitionurls.URL_PATTERNS + urlpatterns

if settings.ALLOW_ADMIN:
    urlpatterns = [path('django-admin/', admin.site.urls)] + urlpatterns

