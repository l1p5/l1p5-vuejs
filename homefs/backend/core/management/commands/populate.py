import random
from typing import Any, Iterable, List
from django.core.management.base import BaseCommand, CommandError

from backend.test_utils import (
    create_test_users,
    ensure_entity_created,
    ensure_lab_created,
)
from backend.utils import get_entity_type
from backend.users.models import L1P5User


from django.core.files import File

from backend.transition.models import PublicActionFile
from backend.carbon.models import GHGI, CommuteSection, SurveyAnswer
from backend.core.models.laboratory import Laboratory
from backend.core.models.core import Boundary, Entity, Members, PositionTitle, Settings


# we only need names of the position titles
L1P5_CF_NAMES = Settings.get_value(name="CF_LABEL_MAPPING")


def populate_transition(users: List[L1P5User]):
    from backend.transition.models import DRAFT, SUBMITTED, PUBLISHED, PublicAction, Tag

    STATUSES = [DRAFT, SUBMITTED, PUBLISHED]
    TAGS = [
        "tag:ges:buildings",
        "tag:ges:devices",
        "tag:ges:travels",
    ]

    LOREM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non
risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies
sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a,
semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim
est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu
massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut
in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue.
Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed
dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci
luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed
pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales
hendrerit."""

    entity_type = get_entity_type()
    for i_user, user in enumerate(users):
        # get the entity whose user is referent
        entity = entity_type.objects.filter(referent=user).first()
        for i in range(20):
            tags_descriptors = list(
                set(
                    [TAGS[i % len(TAGS)]]
                    + [TAGS[i_user % len(TAGS)]]
                    + [TAGS[(i_user + 1) % len(TAGS)]]
                )
            )
            public_action = PublicAction(
                entity=entity,
                title=f"Public action {i} - {entity.name}",
                text=f"Action {i}. {LOREM}",
                admin_status=STATUSES[i % len(STATUSES)],
            )
            tags = set()
            for descriptor in tags_descriptors:
                tag, _ = Tag.objects.get_or_create(descriptor=descriptor)
                tags.add(tag)
            public_action.save(update_fields="admin_status")
            public_action.tags.set(tags)

            from io import BytesIO

            p = PublicActionFile(
                action=public_action,
                file=File(BytesIO(b"test"), name="test.txt"),
                name="test.txt",
            )
            p.save()


def make_members(ghgi):
    """Dummy but effective, 1 guy for each positiontitle"""
    b = Boundary(budget=1)
    b.save()

    for position in PositionTitle.objects.all():
        Members.objects.create(boundary=b, position=position, number=1)

    ghgi.boundary = b
    ghgi.save()


def populate_carbon_laboratory(users):
    for user in users:
        # first create the entities
        # based on the settings
        l = ensure_lab_created(
            referent=user,
            name=f"Lab_OF_{user.email}",
            latitude=random.randint(-90, 90),
            longitude=random.randint(-180, 180),
        )

        # create ghgi (boundary and entity are opaque)
        g = GHGI(entity=l, year=2042)
        g.save()

        make_members(g)

        km = 20
        nWorkingDay = 1
        for idx, pt in enumerate([name for name in L1P5_CF_NAMES]):
            position = L1P5_CF_NAMES[pt]
            seq_id = f"{position[0:2]}-{idx}"
            s = SurveyAnswer(
                ghgi=g, seqID=seq_id, position=position, nWorkingDay=nWorkingDay
            )
            s.save()
            cs = CommuteSection(survey=s, mode="car", engine="diesel", distance=km)
            cs.save()

        # create a ghgi
        g = GHGI(entity=l, year=2043)
        g.save()

        make_members(g)

        km = 20
        nWorkingDay = 2
        for idx, pt in enumerate([name for name in L1P5_CF_NAMES]):
            position = L1P5_CF_NAMES[pt]
            seq_id = f"{position[0:2]}-{idx}"
            s = SurveyAnswer(
                ghgi=g,
                seqID=seq_id,
                nWorkingDay=nWorkingDay,
                position=position,
                nWorkingDay2=nWorkingDay / 2,
            )
            s.save()
            cs1 = CommuteSection(
                survey=s,
                mode="car",
                distance=km,
                isDay2=False,
                pooling=1,
                engine="diesel",
            )
            cs1.save()
            cs2 = CommuteSection(
                survey=s, mode="bus", distance=10, isDay2=True, pooling=False
            )
            cs2.save()

        g = GHGI(entity=l, year=2044)
        g.save()

        make_members(g)

        for idx, pt in enumerate([name for name in L1P5_CF_NAMES]):
            position = L1P5_CF_NAMES[pt]
            seq_id = f"{position[0:2]}-{idx}"
            s = SurveyAnswer(
                ghgi=g,
                seqID=seq_id,
                nWorkingDay=random.randint(1, 5),
                position=position,
            )
            cs = CommuteSection(
                survey=s,
                mode="car",
                engine="diesel",
                distance=random.randint(1, 30),
                isDay2=False,
            )
            s.save()
            cs.save()


def populate_carbon_entity(users):
    for user in users:
        # first create the entities
        # based on the settings
        e = ensure_entity_created(
            referent=user,
            name=f"Entity_OF_{user.email}",
            latitude=random.randint(-90, 90),
            longitude=random.randint(-180, 180),
        )

        # create ghgi (boundary and entity are opaque)
        g = GHGI(entity=e, year=2042)
        g.save()

        make_members(g)

        km = 20
        nWorkingDay = 1
        for pt in [name for name in L1P5_CF_NAMES]:
            position = L1P5_CF_NAMES[pt]
            s = SurveyAnswer(ghgi=g, position=position, nWorkingDay=nWorkingDay)
            s.save()
            cs = CommuteSection(survey=s, mode="car", engine="diesel", distance=km)
            cs.save()

            # create a ghgi
        g = GHGI(entity=e, year=2043)
        g.save()

        make_members(g)

        km = 20
        nWorkingDay = 2
        for pt in [name for name in L1P5_CF_NAMES]:
            position = L1P5_CF_NAMES[pt]
            s = SurveyAnswer(
                ghgi=g,
                nWorkingDay=nWorkingDay,
                position=position,
                nWorkingDay2=nWorkingDay / 2,
            )
            s.save()
            cs1 = CommuteSection(
                survey=s,
                mode="car",
                distance=km,
                isDay2=False,
                pooling=1,
                engine="diesel",
            )
            cs1.save()
            cs2 = CommuteSection(
                survey=s, mode="bus", distance=10, isDay2=True, pooling=False
            )
            cs2.save()

        g = GHGI(entity=e, year=2044)
        g.save()

        make_members(g)

        for pt in [name for name in L1P5_CF_NAMES]:
            position = L1P5_CF_NAMES[pt]
            s = SurveyAnswer(
                ghgi=g, nWorkingDay=random.randint(1, 5), position=position
            )
            cs = CommuteSection(
                survey=s,
                mode="car",
                engine="diesel",
                distance=random.randint(1, 30),
                isDay2=False,
            )
            s.save()
            cs.save()


def populate_carbon(users):

    entity = get_entity_type()
    if entity == Laboratory:
        populate_carbon_laboratory(users)
    elif entity == Entity:
        populate_carbon_entity(users)
    else:
        raise ValueError(f"Don't know how to populate with {entity}")


class Command(BaseCommand):
    """Populate the Database with some data.

    => TESTING Purpose only
    """

    def add_arguments(self, parser):
        parser.add_argument(
            "args",
            metavar="app_label",
            nargs="+",
            help="One or more application label.",
        )
        parser.add_argument("--nusers", help="number of users to create", default=2)
        # self

    def handle(self, *app_labels, **options):
        from django.apps import apps

        if get_entity_type() is None:
            raise ValueError("Must run init force to set the entity type")

        try:
            app_configs = [apps.get_app_config(app_label) for app_label in app_labels]
        except (LookupError, ImportError) as e:
            raise CommandError(
                "%s. Are you sure your INSTALLED_APPS setting is correct?" % e
            )
        nusers = int(options["nusers"])
        _, users = create_test_users(admins_nb=1, users_nb=nusers)
        for app_config in app_configs:
            self.handle_app_config(users, app_config, **options)

    def handle_app_config(
        self, users: List[L1P5User], app_config: Any, **options: Any
    ) -> None:
        import sys

        populate_fnc = getattr(
            sys.modules[__name__], f"populate_{app_config.label}", None
        )
        if not populate_fnc:
            return
        populate_fnc(users)
