from .core import (
    SettingsSerializer,
    EntitySerializer,
    TagSerializer,
    TagCategorySerializer,
    TaggedItemSerializer,
)

from .laboratory import (
    AdministrationSerializer,
    DisciplineSerializer,
    LaboratoryDisciplineSerializer,
    LaboratorySerializer,
)

from .factory import GenericEntitySerializer
