from rest_framework import serializers

from backend.job.models import Job


class PartialJobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = ("id", "name", "created", "finished", "rc", "error", "uuid")


class FullJobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = "__all__"
