from . import l1p5

# post-process some of the settings
## - apply some default parameters

_L1P5_DEFAULT_ACTIVE_SCENARIOS = [
    "FleetElectrification",
    "AnnualCollectiveQuota",
    "CarElectrification",
    "Carpooling",
    "ChangeHeating",
    "IncreaseDevicesLifetime",
    "IncreaseInstrumentsLifetime",
    "InsulateBuilding",
    "LimitPlane",
    "ReduceHeating",
    "ReducePurchase",
    "ReplaceCar",
    "ReplacePlane",
    "ReplacePlaneNational",
    "SecondHandPurchase",
    "SelfConsumption",
    "Teleworking",
    "ChangeDiet",
]

_L1P5_DEFAULT_MODULES_COLOR = {
    "water": "#931c63",
    "construction": "#af4284",
    "heatings": "#c84896",
    "refrigerants": "#fdc2e5",
    "vehicles": "#fd8e62",
    "travels": "#67c3a5",
    "commutes": "#8ea0cc",
    "devices": "#a6d953",
    "purchases": "#ffda2c",
    "ractivities": "#bf4040",
}

_L1P5_DEFAULT_NUMBER_OF_WORKED_WEEKS = dict(default=41)

_L1P5_DEFAULT_FILE_FORMATS = ["ges1p5"]

_L1P5_DEFAULT_ACCEPTED_MIME_TYPES = "application/pdf,image/*,.lss"

_L1P5_DEFAULT_SIGNATURE = "Labos 1point5"

_MESSAGING_PLATFORM_URL = (
    "https://team.picasoft.net/signup_user_complete/?id=6r7cx4f1ufbu3n7ie95fytod3y."
)

_L1P5_DEFAULT_EXTRA_ACTIVATE_EMAIL = f"Vous pouvez également rejoindre la plateforme de discussion instantanée Labos 1point5 ici : {_MESSAGING_PLATFORM_URL}"

## - inject some mandatory values

## These add the identiy mapping on the labels
l1p5.L1P5_TRAVEL_POSITION_DICTIONARY.update(
    {k: k for k in l1p5.L1P5_TRAVEL_POSITION_LABELS}
)
l1p5.L1P5_TRAVEL_PURPOSE_DICTIONARY.update(
    {k: k for k in l1p5.L1P5_TRAVEL_PURPOSE_LABELS}
)

entity_class = l1p5.L1P5_ENTITY_CLASS
if entity_class not in ["Laboratory", "Entity"]:
    raise ValueError(f"Unknown ENTITY_CLASS {entity_class}")


# l1p5.L1P5_CF_LABEL_MAPPING is consistent with l1p5.L1P5_POSITION_TITLES
pt = set([p["name"] for p in l1p5.L1P5_POSITION_TITLES])
cf = set(l1p5.L1P5_CF_LABEL_MAPPING.keys())
if pt != cf:
    raise ValueError(
        "l1p5.L1P5_CF_LABEL_MAPPING keys must correspond to the position title names"
    )

# l1p5.L1P5_TRAVEL_POSITION_DICTIONARY values is a subset of l1p5.L1P5_TRAVEL_POSITION_LABELS
dic = set(l1p5.L1P5_TRAVEL_POSITION_DICTIONARY.values())
lab = set(l1p5.L1P5_TRAVEL_POSITION_LABELS)
if not dic.issubset(lab):
    a = dic.difference(lab)
    raise ValueError(
        f"{a} labels are used in l1p5.L1P5_TRAVEL_POSITION_DICTIONARY but not defined in l1p5.L1P5_TRAVEL_POSITION_LABELS"
    )

# l1p5.L1P5_TRAVEL_PURPOSE_DICTIONARY values is a subset of l1p5.L1P5_TRAVEL_PURPOSE_LABELS
dic = set(l1p5.L1P5_TRAVEL_POSITION_DICTIONARY.values())
lab = set(l1p5.L1P5_TRAVEL_POSITION_LABELS)
if not dic.issubset(lab):
    a = dic.difference(lab)
    raise ValueError(
        f"{a} labels are used in l1p5.L1P5_TRAVEL_PURPOSE_DICTIONARY but not defined in l1p5.L1P5_TRAVEL_PURPOSE_LABELS"
    )


# Only settings exposed to the app.
# This will be stored in the database, some of the value can be tuned from the
# web ui.
L1P5_SETTINGS = [
    {
        "section": "global",
        "name": "LOGO_URL",
        "value": l1p5.L1P5_LOGO_URL,
        "type": 1,
    },
    {
        "section": "global",
        "name": "ACTIVE_APPS",
        "value": getattr(l1p5, "L1P5_ACTIVE_APPS", []),
        "type": 1,
        "component": "ActiveApps",
    },
    {
        "section": "global",
        "name": "WARNING_YEARS",
        "value": getattr(l1p5, "L1P5_WARNING_YEARS", []),
        "type": 2,
        "component": "WarningYears",
    },
    {
        "section": "global",
        "name": "ACTIVE_MODULES",
        "value": getattr(l1p5, "L1P5_ACTIVE_MODULES", []),
        "type": 1,
        "component": "ActiveModules",
    },
    {
        "section": "global",
        "name": "ACTIVE_SCENARIOS",
        "value": getattr(l1p5, "L1P5_ACTIVE_SCENARIOS", _L1P5_DEFAULT_ACTIVE_SCENARIOS),
        "type": 1,
    },
    {
        "section": "global",
        "name": "MODULES_COLORS",
        "value": getattr(l1p5, "L1P5_MODULES_COLOR", _L1P5_DEFAULT_MODULES_COLOR),
        "type": 1,
        "component": "ModulesColors",
    },
    {
        "section": "global",
        "name": "GEONAMES_USERNAME",
        "value": l1p5.L1P5_GEONAMES_USERNAME,
        "type": 1,
        "component": "DefaultInput",
    },
    {
        "section": "global",
        "name": "ALLOWED_COUNTRIES",
        "value": getattr(l1p5, "L1P5_ALLOWED_COUNTRIES", []),
        "type": 1,
        "component": "AllowedCountries",
    },
    {
        "section": "global",
        "name": "ENTITY_CLASS",
        "value": l1p5.L1P5_ENTITY_CLASS,
        "type": 1,
    },
    {
        "section": "global",
        "name": "FAKE_GHGI",
        "value": getattr(l1p5, "L1P5_FAKE_GHGI", None),
        "type": 1,
    },
    {
        "section": "commute",
        "name": "NUMBER_OF_WORKED_WEEKS",
        "value": getattr(
            l1p5, "L1P5_NUMBER_OF_WORKED_WEEKS", _L1P5_DEFAULT_NUMBER_OF_WORKED_WEEKS
        ),
        "type": 2,
    },
    {
        "section": "travel",
        "name": "FILE_FORMATS",
        "value": getattr(l1p5, "L1P5_FILE_FORMATS", _L1P5_DEFAULT_FILE_FORMATS),
        "type": 2,
    },
    {
        "section": "transition",
        "name": "ACCEPTED_MIME_TYPES",
        "value": getattr(
            l1p5, "L1P5_ACCEPTED_MIME_TYPES", _L1P5_DEFAULT_ACCEPTED_MIME_TYPES
        ),
        "type": 2,
    },
    {
        "section": "global",
        "name": "POSITION_TITLES",
        "value": l1p5.L1P5_POSITION_TITLES,
        "type": 2,
    },
    {
        "section": "global",
        "name": "SIGNATURE",
        "value": getattr(l1p5, "L1P5_SIGNATURE", _L1P5_DEFAULT_SIGNATURE),
        "type": 2,
        "component": "DefaultInput",
    },
    {
        "section": "global",
        "name": "EXTRA_ACTIVATE_EMAIL",
        "value": getattr(
            l1p5, "L1P5_EXTRA_ACTIVATE_EMAIL", _L1P5_DEFAULT_EXTRA_ACTIVATE_EMAIL
        ),
        "type": 2,
        "component": "DefaultInput",
    },
    {
        "section": "commute",
        "name": "CF_LABEL_MAPPING",
        "value": l1p5.L1P5_CF_LABEL_MAPPING,
        "type": 2,
    },
    {
        "section": "travel",
        "name": "TRAVEL_POSITION_LABELS",
        "value": l1p5.L1P5_TRAVEL_POSITION_LABELS,
        "type": 2,
    },
    {
        "section": "travel",
        "name": "TRAVEL_POSITION_DICTIONARY",
        "value": l1p5.L1P5_TRAVEL_POSITION_DICTIONARY,
        "type": 2,
    },
    {
        "section": "travel",
        "name": "TRAVEL_PURPOSE_LABELS",
        "value": l1p5.L1P5_TRAVEL_PURPOSE_LABELS,
        "type": 2,
    },
    {
        "section": "travel",
        "name": "TRAVEL_PURPOSE_DICTIONARY",
        "value": l1p5.L1P5_TRAVEL_PURPOSE_DICTIONARY,
        "type": 2,
    },
    {"section": "global", "name": "I18N", "value": l1p5.L1P5_I18N, "type": 2},
]
