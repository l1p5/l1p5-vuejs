These two files are special:

- `fakeGHGI` is used to generate a fake GHGI for use in Scenario. It's based on the lab fr version and the activity correspond roughly to those of an average lab in France.

- `fakeConf` is the corresponding application conf. It's only used in some frontend tests. It should fit the lab-fr version and remain consistent with the `fakeGHGI`. In short

⚠️ Don't edit manually `fakeConf.json` !

The file is generated with the following command:

```
curl http://localhost:8000/api/get_settings/ | jq . > src/utils/tests/mockConf.json
```