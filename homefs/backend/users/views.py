import copy

from django.db import IntegrityError
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView

from .serializers import L1P5UserRegistrationSerializer
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.exceptions import APIException
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.shortcuts import render

from .models import L1P5Group, L1P5User
from backend.core.models import Settings

from .serializers import (
    L1P5TokenObtainPairSerializer,
    PasswordResetSerializer,
    L1P5UserSerializer,
)

from ..utils import (
    hasSuperPower,
)


class DuplicatedEmailError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = "error.duplicated-email"


class PasswordResetView(generics.GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """

    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"detail": "L'email a été envoyé."}, status=status.HTTP_200_OK)


class RegisterView(CreateAPIView):
    serializer_class = L1P5UserRegistrationSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            final_user = copy.deepcopy(self.request.data)
            final_user["is_active"] = False
            user = serializer.save(final_user)
            data = {
                "email": self.request.data["email"],
                "password": self.request.data["password1"],
            }
            headers = self.get_success_headers(serializer.data)
            mail_subject = "Activation compte Labos 1point5"
            current_site = get_current_site(request)
            signature = Settings.get_value(name="SIGNATURE")
            extra_mail = Settings.get_value(name="EXTRA_ACTIVATE_EMAIL")
            message = render_to_string(
                "registration/acc_active_email.html",
                {
                    "domain": current_site.domain,
                    "site_name": current_site.name,
                    "uid": urlsafe_base64_encode(force_bytes(user.id)),
                    "user": user,
                    "token": default_token_generator.make_token(user),
                    "protocol": "https",
                    "signature": signature,
                    "extra_mail": extra_mail,
                },
            )
            email = EmailMessage(mail_subject, message, to=[user.email])
            email.send()
            return Response(status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class L1P5TokenObtainPairView(TokenObtainPairView):
    serializer_class = L1P5TokenObtainPairSerializer


@api_view(["POST"])
@permission_classes([])
def send_activation_email(request):
    user = L1P5User.objects.get(email=request.data)
    mail_subject = "Activation compte Labos 1point5"
    current_site = get_current_site(request)
    signature = Settings.get_value(name="SIGNATURE")
    extra_mail = Settings.get_value(name="EXTRA_ACTIVATE_EMAIL")
    message = render_to_string(
        "registration/acc_active_email.html",
        {
            "domain": current_site.domain,
            "site_name": current_site.name,
            "uid": urlsafe_base64_encode(force_bytes(user.id)),
            "user": user,
            "token": default_token_generator.make_token(user),
            "protocol": "https",
            "signature": signature,
            "extra_mail": extra_mail,
        },
    )
    email = EmailMessage(mail_subject, message, to=[user.email])
    email.send()
    return Response(status=status.HTTP_201_CREATED)


def activate_account(request, uidb64, token):
    """
    View to activate account
    """
    validlink = False
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = L1P5User.objects.get(id=uid)
    except (TypeError, ValueError, OverflowError, L1P5User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        validlink = True
        user.is_active = True
        user.save()
    return render(request, "registration/acc_activated.html", {"validlink": validlink})


@api_view(["POST"])
@permission_classes([])
def user_exists(request):
    if L1P5User.objects.filter(email=request.data["email"]).exists():
        return Response({"exists": True}, status=status.HTTP_200_OK)
    else:
        return Response({"exists": False}, status=status.HTTP_200_OK)


@api_view(["GET"])
@permission_classes([])
def is_super_user(request):
    if request.user.is_authenticated:
        user = L1P5User.objects.get(email=request.user)
        return Response({"is_super_user": user.is_superuser})
    return Response(L1P5User.objects.none())


class CustomPagination(PageNumberPagination):
    """Add some counters."""

    def get_paginated_response(self, data):
        return Response(
            {
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "results": data,
                "count_users": self.page.paginator.count,
                "count_inactives": L1P5User.objects.filter(is_active=False).count(),
                "count_admins": L1P5User.objects.filter(is_superuser=True).count(),
                "count_reviewers": L1P5User.objects.filter(
                    groups__name="reviewer"
                ).count(),
            }
        )


@api_view(["GET"])
@permission_classes([hasSuperPower])
def get_users(request):
    # the entity name
    email = request.query_params.get("email")
    sort = request.query_params.get("sort")
    sort_order = request.query_params.get("sort_order")
    roles = request.query_params.getlist("roles[]")
    users = L1P5User.objects.prefetch_related("groups").filter()

    if email:
        users = users.filter(email__icontains=email)

    if roles:
        users = users.filter(groups__name__in=roles)

    sort_prefix = ""
    if sort_order == "desc":
        sort_prefix = "-"

    if sort:
        if sort == "entity":
            sort = "entity__name"
        users = users.order_by(f"{sort_prefix}{sort}")

    serializer = L1P5UserSerializer(users, many=True)
    paginator = CustomPagination()
    paginator.page_size_query_param = "perPage"
    result_page = paginator.paginate_queryset(users, request)
    serializer = L1P5UserSerializer(result_page, many=True, context={"request": None})
    return paginator.get_paginated_response(serializer.data)


@api_view(["POST"])
@permission_classes([hasSuperPower])
def update_is_admin(request):
    data = request.data.copy()
    email = data.pop("email")
    value = data.pop("value")
    L1P5User.objects.filter(email=email).update(is_superuser=value)

    # reload and return user
    # that'll refresh the permission cache
    u = L1P5User.objects.get(email=email)
    serializer = L1P5UserSerializer(u)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([hasSuperPower])
def update_user(request):
    data = request.data.copy()
    email = data.pop("email")
    item = data.pop("item")
    # only update email so far
    try:
        L1P5User.objects.filter(email=email).update(
            email=item["email"], is_active=item["isActive"]
        )
    except IntegrityError:
        raise DuplicatedEmailError()
    # reload and return user
    # that'll refresh the permission cache
    u = L1P5User.objects.get(email=item["email"])
    serializer = L1P5UserSerializer(u)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([hasSuperPower])
def delete_user(request):
    data = request.data.copy()
    email = data.pop("email")
    user = L1P5User.objects.get(email=email)
    user.delete()
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
@permission_classes([hasSuperPower])
def update_roles(request):
    """Update the permissions

    data: {
        email: email
        roles: [array] of {"reviewer"}
        # we have a single role for now
    }
    """
    data = request.data.copy()
    email = data.pop("email")
    roles = data.pop("roles")
    # add user whose email is email to the group described in roles
    user = get_object_or_404(L1P5User, email=email)
    user_groups = []
    for role in roles:
        groups = L1P5Group.objects.filter(name=role)
        # ignore role that doesn't exis
        if len(groups) != 1:
            continue
        group = groups[0]
        user_groups.append(group)
    user.groups.set(user_groups)

    # reload and return user
    # that'll refresh the permission cache
    u = L1P5User.objects.get(email=email)
    serializer = L1P5UserSerializer(u)
    return Response(serializer.data, status=status.HTTP_200_OK)
