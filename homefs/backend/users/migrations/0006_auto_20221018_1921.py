# Generated by Django 3.2.7 on 2022-10-18 19:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20220330_1133'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipe',
            name='membres',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='disciplineCNU',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='genre',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='statut',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='tutelles',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='user',
        ),
        migrations.DeleteModel(
            name='Discipline',
        ),
        migrations.DeleteModel(
            name='Equipe',
        ),
        migrations.DeleteModel(
            name='Genre',
        ),
        migrations.DeleteModel(
            name='Membre',
        ),
        migrations.DeleteModel(
            name='Statut',
        ),
    ]
