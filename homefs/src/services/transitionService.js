import api from '@/services/api'
import _ from 'lodash'
import { entityFactory } from '../models/core/entity/factory'

/**
 * Strip the axios baseURL because it's contained in the url
 * returned by Django
 * @param {*} url
 * @returns
 */
function buildUrl (url) {
  return url.replace(api.defaults.baseURL, '')
}

function buildFilterParams (filters) {
  let params = {}
  if (filters.bounds) {
    let [lat1, lon1] = filters.bounds[0]
    let [lat2, lon2] = filters.bounds[1]
    params.bounds = [lat1, lon1, lat2, lon2]
  }
  if (filters.administrations && filters.administrations.length > 0) {
    params.administrations = filters.administrations.map((a) => a.name)
  }
  if (filters.administrationsOp) {
    params.administrationsOp = filters.administrationsOp
  }
  if (filters.disciplines) {
    // disciplines is the list of the ids
    params.disciplines = filters.disciplines
  }
  if (filters.disciplinesOp) {
    params.disciplinesOp = filters.disciplinesOp
  }
  if (filters.tags) {
    params.tags = filters.tags.map((t) => t.descriptor)
  }
  if (filters.tagsOp) {
    params.tagsOp = filters.tagsOp
  }

  if (filters.sort) {
    params.sort = filters.sort
  }

  if (filters.status) {
    params.status = filters.status
  }
  if (filters.title) {
    params.title = filters.title
  }
  if (filters.entityId) {
    params.entity_id = filters.entityId
  }

  return params
}

export default {
  _saveFilesForAction (actionId, files) {
    let formData = new FormData()
    for (const file of files) {
      formData.append(file.name, file)
    }

    return api.post(`transition/actions/${actionId}/files/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  },

  getCount () {
    return api.get(`transition/get_count/`).then((response) => {
      return response.data
    })
  },
  savePublicAction (payload, files) {
    if (payload.id === null) {
      // don't push the id in the resulting formdata
      payload = _.omit(payload, 'id')
    }
    // save the action
    return api
      .post(`transition/actions/`, payload)
      .then((response) => {
        let action = response.data
        if (files.length <= 0) {
          // skip the second request
          return response
        }
        return this._saveFilesForAction(action.id, files)
      })
      .then((response) => response.data)
  },
  deletePublicAction (actionId) {
    return api
      .delete(`transition/actions/${actionId}/`)
      .then((response) => response.data)
  },
  getQuota () {
    return api.get('transition/quota/').then((response) => response.data)
  },
  deletePublicActionFile (file) {
    // strip the base url since it's contained in file.url
    return api.delete(buildUrl(file.url))
  },
  getFile (file) {
    return api
      .get(buildUrl(file.url), { responseType: 'blob' })
      .then((response) => {
        return response.data
      })
  },
  getPublicActions () {
    return api.get(`/transition/actions/`).then((response) => response.data)
  },
  submitPublicAction (actionId) {
    return api
      .post(`transition/actions/${actionId}/`)
      .then((response) => response.data)
  },
  /**
   *
   * Reviewer stage
   *
   *
   */
  reviewerGetQuota (labId) {
    return api
      .get(`/reviewer/quota/${labId}/`)
      .then((response) => response.data)
  },
  reviewerGetActions () {
    return api.get('/reviewer/actions/').then((response) => response.data)
  },
  reviewerGetAction (actionId) {
    return api
      .get(`/reviewer/actions/${actionId}/`)
      .then((response) => response.data)
  },
  reviewerUpdateAction (action, files) {
    // save the action
    return api
      .put(`reviewer/actions/${action.id}/`, action)
      .then((response) => {
        let action = response.data
        if (files.length <= 0) {
          // skip the second request
          return response
        }
        return this._saveFilesForAction(action.id, files)
      })
      .then((response) => response.data)
  },
  reviewerAssignAction (actionId) {
    return api
      .post(`/reviewer/actions/my/`, { id: actionId })
      .then((response) => response.data)
  },
  reviewerTogglePublishAction (actionId) {
    return api
      .post(`/reviewer/actions/${actionId}/`)
      .then((response) => response.data)
  },
  reviewerGetMessages (actionId) {
    return api
      .get(`reviewer/actions/${actionId}/messages/`)
      .then((response) => response.data)
  },
  reviewerSaveMessage (actionId, message) {
    return api
      .post(`reviewer/actions/${actionId}/messages/`, { message: message })
      .then((response) => response.data)
  },
  reviewerSaveReminder (actionId, reminder) {
    return api.post(`reviewer/actions/${actionId}/reminder/`, {
      reminder: reminder
    })
  },
  reviewerGetReminders () {
    return api.get('/reviewer/reminders/').then((response) => response.data)
  },
  reviewerGetCounts () {
    return api.get('/reviewer/counts/').then((response) => response.data)
  },

  /**
   *
   * Anonymous stage
   */
  hasReviewerPermissions () {
    return api
      .get(`/transition/has_reviewer_permissions/`)
      .then((response) => response.data)
  },
  getNextPublishedActions (url) {
    let u = new URL(url)
    let queryParams = u.search
    return api
      .get('/public/actions/' + queryParams)
      .then((response) => response.data)
  },
  getPublishedActions (filters) {
    let params = buildFilterParams(filters)
    return api
      .get('/public/actions/', { params: params })
      .then((response) => response.data)
  },
  getOnePublishedAction (actionId) {
    return api
      .get(`/public/actions/${actionId}/`)
      .then((response) => response.data)
  },
  // FIXME: this probably doesn't need to be here
  hasSubmittedGHGIs () {
    return api
      .get('/transition/has_submitted_ghgis/')
      .then((response) => response.data)
  },
  getEntities (pattern) {
    let params = { name: pattern }
    return api.get('/public/entities/', { params: params }).then((response) => {
      let entities = response.data.map((e) => {
        let EntityClass = entityFactory(e.type)
        return EntityClass.createFromObj(e.data)
      })
      return entities
    })
  }
}
