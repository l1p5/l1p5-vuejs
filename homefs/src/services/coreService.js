import api from '@/services/api'

export default {
  getSettings () {
    return api.get(`get_settings/`).then((response) => response.data)
  },
  saveSettings (payload) {
    return api.post(`save_settings/`, payload).then((response) => response.data)
  },
  getAdministrations () {
    return api.get(`get_administrations/`).then((response) => response.data)
  },
  getDisciplines () {
    return api.get(`get_disciplines/`).then((response) => response.data)
  },
  saveEntity (payload) {
    return api.post(`save_entity/`, payload).then((response) => response.data)
  },
  getEntity (payload) {
    return api.get(`get_entity/`).then((response) => response.data)
  },
  getAllTagCategories (payload) {
    return api.get(`get_all_tag_categories/`).then((response) => response.data)
  },
  updateOrCreateTagCategory (payload) {
    return api
      .post(`update_or_create_tag_category/`, payload)
      .then((response) => response.data)
  },
  deleteTagCategory (payload) {
    return api
      .post(`delete_tag_category/`, payload)
      .then((response) => response.data)
  }
}
