import Vue from 'vue'
import VueRouter from 'vue-router'
import { useCoreStore } from '../stores/core.js'
import MainNavbar from '@/layout/MainNavbar.vue'
import MainFooter from '@/layout/MainFooter.vue'
import { requireAuthenticated } from '@/router/utils'
import { Apps1point5 } from '@/models/ActiveApps.js'

import carbonRoutes from './carbon.js'
import scenarioRoutes from './scenario.js'
import transitionRoutes from './transition.js'

import '../../node_modules/nprogress/nprogress.css'

Vue.use(VueRouter)

const appsRoutes = {
  [Apps1point5.CARBON_APPLICATION]: carbonRoutes,
  [Apps1point5.SCENARIO_APPLICATION]: scenarioRoutes,
  [Apps1point5.TRANSITION_APPLICATION]: transitionRoutes
}

const router = new VueRouter({
  mode: 'history',
  hashbang: false,
  linkExactActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
      // Or for Vue 3:
      // return {el: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('@/views/Home.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Plateforme Apps 1point5',
        titleen: 'Apps 1point5 plateform',
        subtitlefr:
          'Bienvenue sur la plateforme applicative Apps 1point5 qui offre une suite logicielle permettant de faire un bilan de gaz à effet de serre, de simuler des scénarios de réduction de l’empreinte carbone et de faire connaître les initiatives en lien avec la réduction de l’empreinte environnementale, en particulier sur le climat.',
        subtitleen:
          'Welcome to the Apps 1point5 application platform, which offers a software suite that allows you to perform a greenhouse gas inventory, to simulate carbon footprint reduction scenarios and to publicize initiatives related to the reduction of your environmental footprint, particularly on the climate.',
        home: true
      }
    },
    {
      path: '/administration/',
      name: 'administration',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Administration',
        titleen: 'Administration'
      }
    },
    {
      path: '/administration/:name',
      name: 'administration-named',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titlefr: 'Administration',
        titleen: 'Administration'
      }
    },
    {
      path: '/politique-de-confidentialite',
      name: 'politique-de-confidentialite',
      components: {
        default: () => import('@/views/PrivacyPolicy.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Politique de confidentialité',
        titleen: 'Privacy policy'
      }
    },
    {
      path: '/labos1point5-charter',
      name: 'labos1point5-charter',
      components: {
        default: () => import('@/views/Labos1point5Charter.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Charte Labos 1point5',
        titleen: 'Labos 1point5 charter'
      }
    },
    {
      path: '/commutes-simulator',
      name: Apps1point5.COMMUTES_SIMULATOR,
      components: {
        default: () => import('@/views/CommutesSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Commutes simulator'
      }
    },
    {
      path: '/foods-simulator',
      name: Apps1point5.FOODS_SIMULATOR,
      components: {
        default: () => import('@/views/FoodsSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Foods simulator'
      }
    },
    {
      path: '/travels-simulator',
      name: Apps1point5.TRAVELS_SIMULATOR,
      components: {
        default: () => import('@/views/TravelsSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Travels simulator'
      }
    },
    {
      path: '/purchases-simulator',
      name: 'purchases-simulator',
      components: {
        default: () => import('@/views/PurchasesSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Simulateur',
        titleen: 'Purchases simulator'
      }
    },
    {
      path: '/ecolabware',
      name: Apps1point5.ECOLABWARE_SIMULATOR,
      components: {
        default: () => import('@/views/EcoLabware.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'EcoLabware',
        titleen: 'EcoLabware'
      }
    },
    {
      path: '/documentation',
      name: 'documentation',
      components: {
        default: () => import('@/views/Documentation.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Documentation',
        titleen: 'Documentation'
      }
    },
    {
      path: '/reset-password',
      name: 'reset-password',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Mot de passe oublié ?',
        titleen: 'Forgot your password ?'
      }
    },
    {
      path: '/reset-password/accounts/reset/:id/:token',
      name: 'reset-password-link',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Mot de passe oublié ?',
        titleen: 'Forgot your password ?'
      }
    },
    {
      path: '/activation-compte/accounts/activate_account/:id/:token',
      name: 'activation-compte',
      components: {
        default: () => import('@/views/ActivationCompte.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Activation de compte',
        titleen: 'Account activation'
      }
    },
    {
      path: '/not-authorized',
      name: 'not-authorized',
      components: {
        default: () => import('@/views/401.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Accès non autorisé',
        titleen: 'Access restricted'
      }
    },
    {
      path: '*',
      name: 'not-found',
      components: {
        default: () => import('@/views/404.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titlefr: 'Page non trouvée',
        titleen: 'Page not found'
      }
    },
    ...carbonRoutes,
    ...scenarioRoutes,
    ...transitionRoutes
  ]
})

router.beforeEach((to, from, next) => {
  let coreStore = useCoreStore()
  coreStore.getSettings().then(() => {
    let isims = coreStore.inactiveSimulators.map((sim) => sim.name)
    if (isims.includes(to.name)) {
      next('/')
    }
    let iapps = coreStore.inactiveApps.map((app) => app.name)
    for (let app of iapps) {
      let r = appsRoutes[app].map((route) => route.name)
      if (r.includes(to.name)) {
        next('/')
      }
    }
    next()
  })
})

router.beforeResolve((to, from, next) => {
  if (to.name && from.name !== null) {
    router.app.$nprogress.start()
  }
  next()
})

router.afterEach((to, from) => {
  setTimeout(() => router.app.$nprogress.done(), 500)
})

export default router
