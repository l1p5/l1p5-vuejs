import router from './index'
import pinia from '@/stores/initPinia.js'
import { useAuthenticationStore } from '@/stores/authentication.js'

/**
 * Handle some axios error by redirecting the app to dedicated error page
 *
 * @param {*} axios error
 */
function axiosRedirectError (error) {
  switch (error.response.status) {
    case 404:
      router.replace('/not-found').catch(() => {})
      break
    case 401:
      router.replace('/not-authorized').catch(() => {})
      break
  }
}

const requireAuthenticated = (to, from, next) => {
  const authStore = useAuthenticationStore(pinia)
  if (!authStore.isAuthenticated) {
    next('/')
  } else {
    next()
  }
}

export { axiosRedirectError, requireAuthenticated }
