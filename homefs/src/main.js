import Vue from 'vue'
import App from '@/App.vue'
import pinia from '@/stores/initPinia.js'
import router from '@/router'

import '@/utils/filters.js'
import '@/plugins/veevalidate.js'
import '@/utils/veerules.js'
import '@/plugins/buefy.js'
import '@/plugins/axios.js'
import { i18n } from '@/plugins/i18n.js'
import FlagIcon from 'vue-flag-icon'
import NProgress from 'vue-nprogress'
import VueClipboard from 'vue-clipboard2'
import VuePapaParse from 'vue-papa-parse'

import { useCoreStore } from '@/stores/core'

import { structureMessages, DEFAULT_STRUCTURE_KEY } from './locales/entity'

Vue.use(FlagIcon)
Vue.use(VueClipboard)
Vue.use(VuePapaParse)

Vue.use(NProgress)
const nprogress = new NProgress()

Vue.config.productionTip = false

// Handle window scrolling for navbar and backtotop
let pxShow = 400
// let scrollSpeed = 500
window.addEventListener('scroll', function () {
  if (window.scrollY > 75) {
    let elements = document.getElementsByClassName('navbar')
    if (elements.length === 1) {
      elements[0].classList.add('navbar-fixed')
    }
  } else {
    let elements = document.getElementsByClassName('navbar')
    if (elements.length === 1) {
      elements[0].classList.remove('navbar-fixed')
    }
  }
  if (window.scrollY >= pxShow) {
    document.getElementById('backtotop').classList.add('visible')
  } else {
    document.getElementById('backtotop').classList.remove('visible')
  }
})

new Vue({
  nprogress,
  router,
  i18n,
  pinia,
  render: (h) => h(App)
}).$mount('#app')

/**
 * replacing the $t injected method
 * https://github.com/kazupon/vue-i18n/blob/68955423d4cbc62c7add4669ea8a166dd0c64ad0/src/extend.js#L11-L14
 * It look up the entity configured in the store and apply the right translation for the structure key
 *
 * e.g: 'Hello {structure}' will be replaced by
 *
 * 'Hello laboratoire' if the ENTITY_CLASS is Entity and locale is fr
 * 'Hello laboratory' if the ENTITY_CLASS is Entity and locale is en
 * 'Hello structure' if the ENTITY_CLASS is Entity
 *
 * This is based on
 */
Vue.prototype.$t = function (key, ...values) {
  let messages = structureMessages[DEFAULT_STRUCTURE_KEY]
  try {
    // not sure the store is initially ready
    // so be a bit defensive here and fallback in default messages in case of
    // trouble
    const coreStore = useCoreStore()
    messages = structureMessages[coreStore.entityClass]
  } catch (error) {
    console.error(error)
  }
  // rationale:
  // make sure we don't break named-formatting
  // https://kazupon.github.io/vue-i18n/guide/formatting.html#named-formatting
  let fstValue = {}
  if (values.length > 0) {
    fstValue = values[0]
  }
  const i18n = this.$i18n
  Object.assign(fstValue, messages[i18n.locale])
  return i18n._t(
    key,
    i18n.locale,
    i18n._getMessages(),
    this,
    fstValue,
    ...values.slice(1)
  )
}
