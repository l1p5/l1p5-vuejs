/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import GHGI from '@/models/carbon/GHGI.js'
import Modules from '@/models/Modules.js'

import { defineStore } from 'pinia'
import { useCoreStore } from './core'
import { useAdminStore } from './admin'
import { makeGHGI } from '@/utils/ghgi'
import { Boundary } from '../models/carbon/Boundary'

export const useGHGIStore = defineStore('ghgi', {
  state: () => ({
    item: new GHGI({ boundary: new Boundary() }),
    initiated: false,
    computed: false
  }),
  getters: {
    options: (state) => {
      const core = useCoreStore()
      return core.options
    },
    moduleObj: (state) => (type) => {
      const core = useCoreStore()
      return core.module(type)
    },
    items: (state) => (module) => {
      return state.item[module].toArray()
    },
    collection: (state) => (module) => {
      return state.item[module]
    },
    hasItems: (state) => (module) => {
      return state.item[module].length > 0
    },
    otherItems:
      (state) =>
        (module, source = null) => {
          if (source) {
            return state.item[module].filter(
              (item) => item.isOther() && item.source === source
            )
          } else {
            return state.item[module].filter((item) => item.isOther())
          }
        },
    validItems:
      (state) =>
        (module, source = null) => {
          if (source) {
            return state.item[module].filter(
              (item) => item.isValid() && item.source === source
            )
          } else {
            return state.item[module].filter((item) => item.isValid())
          }
        },
    invalidItems:
      (state) =>
        (module, source = null) => {
          if (source) {
            return state.item[module].filter(
              (item) => item.isInvalid() && item.source === source
            )
          } else {
            return state.item[module].filter((item) => item.isInvalid())
          }
        },
    incompleteItems: (state) => (module) => {
      return state.item[module].filter((item) => item.isIncomplete())
    },
    countItems:
      (state) =>
        ({ module, accessor }) => {
          return state.item[module].countItems(accessor)
        },
    isClonedSurvey: (state) => state.item.surveyCloneYear !== null,
    workforce: (state) => {
      return state.item.boundary.workforce
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    /**
     * Used to create a fresh GHGI
     */
    init () {
      const core = useCoreStore()
      if (!this.initiated) {
        let boundary = Boundary.fromPositionTitles(
          core.positionTitles,
          core.surveyLabelMapping
        )
        this.item = new GHGI({
          boundary,
          travelPositionLabels: core.travelPositionLabels,
          travelPurposeLabels: core.travelPurposeLabels,
          activeModulesObjs: core.activeModulesObjs
        })
        this.initiated = true
      }
    },
    addItem (data) {
      this.item[data.module].add(data.item)
    },
    addTags (data) {
      for (let citem of data.items) {
        if ('items' in citem && citem.items.length) {
          citem.items.forEach((el) => {
            this.item[data.module].lookup(el).addTags(data.tags)
          })
        } else {
          this.item[data.module].lookup(citem).addTags(data.tags)
        }
      }
    },
    updateItem (data) {
      this.item[data.module].update(data.item)
    },
    updateItems (data) {
      this.item[data.module].reset()
      for (let item of data.items) {
        this.item[data.module].add(item)
      }
    },
    deleteItems (data) {
      for (let item of data.items) {
        this.item[data.module].delete(item)
      }
    },
    deleteSource (data) {
      if (data.source) {
        this.item[data.module].deleteSource(data.source)
      } else {
        this.item[data.module].reset()
      }
    },
    deleteInvalidItems (module) {
      this.item[module].deleteInvalid()
    },
    reduce (module) {
      // in place modification
      this.item[module].reduce()
    },
    sortItems (module) {
      this.item[module].sortItems()
    },
    saveItems (module) {
      let moduleObj = this.moduleObj(module)
      this.compute([], true)
      let allData = {
        [module]: this.collection(module)
          .reduceItems()
          .map((obj) => moduleObj.dumps(obj)),
        ghgi_id: this.item.id,
        synthesis: this.item.synthesis
      }
      return moduleObj
        .save(allData)
        .then((data) => {
          let loadFnc = moduleObj.loads
          // FIXME(msimonin): use the right model class based on module type
          let objs = data.map(loadFnc)
          this.updateItems({
            module: module,
            items: objs
          })
          this.forceComputation()
          return objs
        })
        .catch((error) => {
          throw error
        })
    },
    compute (tags = [], force = false) {
      if (force) {
        this.forceComputation()
      }
      if (!this.computed) {
        this.item.compute(this.options, this.item.year, tags)
        this.computed = true
      }
    },
    saveSurveyConfig (config) {
      let allData = {
        surveyMessage: config.surveyMessage,
        surveyTags: config.surveyTags,
        ghgi_id: this.item.id
      }
      return carbonService
        .saveSurveyConfig(allData)
        .then((data) => {
          this.item.surveyMessage = config.surveyMessage
          this.item.surveyTags = config.surveyTags
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    set (id) {
      return carbonService
        .getGHGIConsumptions({ ghgi_id: id })
        .then((data) => {
          let core = useCoreStore()
          this.item = makeGHGI(
            data,
            core.activeModulesObjs,
            core.surveyLabelMapping
          )
          // NOTE(msimonin): why do we update the lab in the other store ?
          const admin = useAdminStore()
          admin.updateEntity(data.entity)

          this.compute([], true)
          this.initiated = true
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    setFromUUID (uuid) {
      return carbonService
        .getGHGIConsumptionsByUUID({ uuid: uuid })
        .then((data) => {
          let core = useCoreStore()
          this.item = makeGHGI(
            data,
            core.activeModulesObjs,
            core.surveyLabelMapping
          )
          const admin = useAdminStore()
          admin.updateEntity(data.entity)

          this.compute([], true)
          this.initiated = true
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    /**
     * Create or update a GHGI
     * - creation if ghgi.id is null
     * - update otherwise
     * @param {GHGI} ghgi the GHGI to add/udpate
     * @returns {Promise<GHGI>} Promise object that represents the new/updated
     *  GHGI
     */
    async saveGHGI () {
      let core = useCoreStore()
      return carbonService
        .saveGHGI({
          id: this.item.id,
          year: this.item.year,
          description: this.item.description,
          boundary: this.item.boundary.toDatabase(),
          submitted: this.item.submitted,
          synthesis: this.item.synthesis
        })
        .then((data) => {
          this.item = makeGHGI(
            data,
            core.activeModulesObjs,
            core.surveyLabelMapping
          )
          this.initiated = true
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    updateSubmitted (module) {
      this.item.updateSubmitted(module)
      return carbonService
        .updateSubmitted({
          ghgi_id: this.item.id,
          submitted: this.item.submitted
        })
        .then((data) => {
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    updateAllSubmitted (value) {
      this.item.updateAllSubmitted(value)
      return carbonService
        .updateSubmitted({
          ghgi_id: this.item.id,
          submitted: this.item.submitted
        })
        .then((data) => {
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    resetGHGI () {
      this.$reset()
    },
    forceComputation () {
      this.computed = false
    },
    cloneSurvey (surveyCloneYear) {
      let allData = {
        ghgi_id: this.item['id'],
        surveyCloneYear: surveyCloneYear
      }
      return carbonService
        .cloneSurvey(allData)
        .then((data) => {
          this.item.surveyCloneYear = surveyCloneYear

          this.updateItems({
            module: Modules.COMMUTES,
            items: data.map(this.moduleObj(Modules.COMMUTES).loads)
          })
          this.updateItems({
            module: Modules.FOODS,
            items: data.map(this.moduleObj(Modules.FOODS).loads)
          })
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    updateSurveyActive (module = null) {
      this.compute([], true)
      this.item.activateDesactivateSurvey(module)
      return carbonService
        .updateSurveyActive({
          ghgi_id: this.item.id,
          commutesActive: this.item.commutesActive,
          foodsActive: this.item.foodsActive,
          synthesis: this.item.synthesis
        })
        .then((data) => {
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    updateYear (year) {
      this.item.year = year
      // TODO : to refactor
      const admin = useAdminStore()
      admin.updateGHGIYear(this.item.id, year)
    },
    updateSurveyMessage (message) {
      this.item.surveyMessage = message
    },
    updateSurveyTags (tags) {
      this.item.surveyTags = tags
    },
    updateNBudget (budget) {
      this.item.boundary.budget = budget
    },
    updateDescription (description) {
      this.item.description = description
    }
  }
})
