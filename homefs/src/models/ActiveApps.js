/**
 * Container for all the active apps
 *
 */
class ActiveApps {
  constructor () {
    this.apps = []
  }

  /**
   * Register a new app.
   * By doing so the vue component will show up in the web ui.
   *
   * Note that you can't add two apps with the same name.
   *
   * @param {Apps1point5} app  - app to register
   * @throws {Error} the app name already active
   */
  register (app) {
    let a = this.apps.find((a) => a.name === app.name)
    if (!a) {
      this.apps.push(app)
      return this
    }
    throw new Error(`can't add the app ${app.name}, this app already exists`)
  }

  /**
   * Build-up an ActiveApps based on the settings
   *
   * @param {Object[]} settings
   * @returns {ActiveApps}
   */
  static fromSettings (settings) {
    // get the names of the apps
    let names = settings.filter(
      (obj) => obj.section === 'global' && obj.name === 'ACTIVE_APPS'
    )[0].value
    return ActiveApps.fromNames(names)
  }

  /**
   * Build-up an ActiveApps baed on the names
   *
   * @param {Object[]}} names
   * @returns {ActiveApps}
   */
  static fromNames (names) {
    let apps = new this()
    for (let app of ALL_APPS.filter((a) => names.includes(a.appname))) {
      apps.register(app)
    }
    return apps
  }
}

class Apps1point5 {
  constructor ({
    appname = '',
    icon = null,
    userLink = '',
    publicLink = null,
    hasDocumentation = false
  } = {}) {
    if (!appname || !userLink | !publicLink) {
      throw Error('Fields name, userLink, publicLink must be set')
    }
    this.appname = appname
    this.userLink = userLink
    this.publicLink = publicLink
    this.icon = icon
    this.hasDocumentation = hasDocumentation
  }

  get name () {
    return this.appname.split('/')[1]
  }

  get type () {
    return this.appname.split('/')[0]
  }

  get isSimulator () {
    return this.type === Apps1point5.SIMULATOR
  }

  static get CARBON_APPLICATION () {
    return 'carbon'
  }

  static get SCENARIO_APPLICATION () {
    return 'scenario'
  }

  static get TRANSITION_APPLICATION () {
    return 'transition'
  }

  static get COMMUTES_SIMULATOR () {
    return 'commutes-simulator'
  }

  static get FOODS_SIMULATOR () {
    return 'foods-simulator'
  }

  static get TRAVELS_SIMULATOR () {
    return 'travels-simulator'
  }

  static get ECOLABWARE_SIMULATOR () {
    return 'ecolabware'
  }

  static get APPLICATION () {
    return 'application'
  }

  static get SIMULATOR () {
    return 'simulator'
  }
}

/**
 * Definition of all possible apps that can be activated
 *
 */
const ALL_APPS = [
  new Apps1point5({
    appname: Apps1point5.APPLICATION + '/' + Apps1point5.CARBON_APPLICATION,
    icon: 'thermometer',
    userLink: '/administration/all-ghgi',
    publicLink: '/ges-1point5',
    hasDocumentation: true
  }),
  new Apps1point5({
    appname: Apps1point5.APPLICATION + '/' + Apps1point5.SCENARIO_APPLICATION,
    icon: 'crosshairs',
    userLink: '/administration/all-scenarios',
    publicLink: '/scenario-1point5',
    hasDocumentation: true
  }),
  new Apps1point5({
    appname: Apps1point5.APPLICATION + '/' + Apps1point5.TRANSITION_APPLICATION,
    icon: 'lightbulb-o',
    userLink: '/transition-1point5',
    publicLink: '/transition-1point5',
    hasDocumentation: true
  }),
  new Apps1point5({
    appname: Apps1point5.SIMULATOR + '/' + Apps1point5.COMMUTES_SIMULATOR,
    userLink: '/commutes-simulator',
    publicLink: '/commutes-simulator'
  }),
  new Apps1point5({
    appname: Apps1point5.SIMULATOR + '/' + Apps1point5.FOODS_SIMULATOR,
    userLink: '/foods-simulator',
    publicLink: '/foods-simulator'
  }),
  new Apps1point5({
    appname: Apps1point5.SIMULATOR + '/' + Apps1point5.TRAVELS_SIMULATOR,
    userLink: '/travels-simulator',
    publicLink: '/travels-simulator'
  }),
  new Apps1point5({
    appname: Apps1point5.SIMULATOR + '/' + Apps1point5.ECOLABWARE_SIMULATOR,
    userLink: '/ecolabware',
    publicLink: '/ecolabware',
    hasDocumentation: true
  })
]

export { ActiveApps, Apps1point5, ALL_APPS }
