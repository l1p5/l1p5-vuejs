import { Entity } from './Entity'
import { Laboratory } from './Laboratory'

const CLASSES = {
  Entity,
  Laboratory
}

function getEntityClasses () {
  return CLASSES
}

function entityFactory (name) {
  return CLASSES[name]
}

function boundaryFactory (name) {
  // FIXME: returns a generic boundary
  return entityFactory(name).boundaryClass
}

export { entityFactory, boundaryFactory, getEntityClasses }
