/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import LABWARES_FACTORS from '@/../data/factors/lca/labWaresFactors.json'
import { ImpactFactor } from '@/models/carbon/Factor.js'
import {
  ImpactIntensity,
  ImpactIntensities
} from '@/models/carbon/ImpactIntensity.js'

// MICALIS value (liter)
const AUTOCLAVE_WATER_DEFAULT = 200
// MICALIS value
const AUTOCLAVE_NCYCLE_DEFAULT = 10
// MICALIS value
const LAB_WASHER_USAGE = 11700
// MICALIS value (kWh)
const LAB_WASHER_CONSUMPTION = 10
// MICALIS value (liter)
const LAB_WASHER_DEIOWATER_DEFAULT = 60
// MICALIS value (liter)
const LAB_WASHER_WATER_DEFAULT = 120
// MICALIS value (liter) for prewashing
const LAB_WASHER_DETERGENT_DEFAULT = 0.12
// Energy required to heat 1 liter of water from 15°C to 37°C degree (kWh/l)
const HOT_WATER_CONSUMPTION = 0.04
// Electricity to sterilize e-beam 1kg of plastic (kWh)
const EBEAM_CONSUMPTION = 0.29

export class LabWare {
  constructor (
    weight,
    type,
    country = 'fr',
    manufacturingArea = LabWare.EUROPE,
    sterilized = true,
    decontamination = true,
    nautoclaveCycleAWeek = null,
    autoclaveWeight = null,
    autoclaveVolume = null
  ) {
    this.weight = weight
    this.type = type
    this.country = country
    this.manufacturingArea = manufacturingArea
    this.sterilized = sterilized
    this.decontamination = decontamination
    this.nautoclaveCycleAWeek = nautoclaveCycleAWeek
    this.autoclaveWeight = autoclaveWeight
    this.autoclaveVolume = autoclaveVolume
  }

  get weightkg () {
    return this.weight / 1000
  }

  get manufacturingKey () {
    let area = this.manufacturingArea
    if (this.manufacturingArea !== LabWare.EUROPE) {
      area = LabWare.ROW
    }
    return area
  }

  get autoclaveUsage () {
    if (this.nautoclaveCycleAWeek) {
      // 20 -> life duration (years)
      return this.nautoclaveCycleAWeek * 52 * 20
    } else {
      return LabWare.AUTOCLAVE_NCYCLE_DEFAULT * 52 * 20
    }
  }

  get gloveImpactFactor () {
    return ImpactFactor.createFromObj({
      description_FR: 'Glove',
      description_EN: 'Glove',
      unit: 'unit',
      group: 'glove',
      decomposition: {
        2023: {
          water: 0.4 / 3,
          carbon: 0.4
        }
      }
    })
  }

  get autoclaveImpactFactor () {
    // default for a 600L autoclave
    let intensity = 13166
    if (this.autoclaveWeight) {
      intensity = (this.autoclaveWeight / 3380) * 34220
    } else if (this.autoclaveVolume) {
      intensity = (this.autoclaveVolume / 1700) * 34220
    }
    // Note : Gettinge publication do not provide water impact.
    // We use carbon impact / water impact ration as for gloves.
    // Here we use the washer ratio, 774/343=2.26
    return ImpactFactor.createFromObj({
      description_FR: 'Autoclave',
      description_EN: 'Autoclave',
      unit: 'unit',
      group: 'autoclave',
      decomposition: {
        2023: {
          water: intensity / 2.26,
          carbon: intensity
        }
      }
    })
  }

  __getManufacturingImpact (year = null) {
    let key = this.type + '.' + this.manufacturingKey + '.manufacturing'
    let ifactors = ImpactFactor.createFromObj(LABWARES_FACTORS[key])
    return ImpactIntensity.compute(this.weightkg, ifactors)
  }

  __getEOLImpact (year = null) {
    let key = this.type + '.' + this.manufacturingKey + '.eol'
    let ifactors = ImpactFactor.createFromObj(LABWARES_FACTORS[key])
    return ImpactIntensity.compute(this.weightkg, ifactors)
  }

  __getTransportImpact (year = null) {
    let stef = ImpactFactor.createFromObj(
      LABWARES_FACTORS['road.transport.small']
    )
    let btef = ImpactFactor.createFromObj(
      LABWARES_FACTORS['road.transport.big']
    )
    let sef = ImpactFactor.createFromObj(LABWARES_FACTORS['sea.transport'])
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.manufacturingArea === LabWare.EUROPE) {
      impacts.add(ImpactIntensity.compute(500, btef).divide(1000))
      impacts.add(ImpactIntensity.compute(500 + 150, stef).divide(1000))
    } else if (this.manufacturingArea === LabWare.ASIA) {
      impacts.add(ImpactIntensity.compute(500 + 500, btef).divide(1000))
      impacts.add(ImpactIntensity.compute(20000, sef).divide(1000))
      impacts.add(ImpactIntensity.compute(500 + 150, stef).divide(1000))
    } else if (this.manufacturingArea === LabWare.AMERICA) {
      impacts.add(ImpactIntensity.compute(1000 + 500, btef).divide(1000))
      impacts.add(ImpactIntensity.compute(10000, sef).divide(1000))
      impacts.add(ImpactIntensity.compute(500 + 150, stef).divide(1000))
    }
    return impacts.sum().multiply(this.weightkg)
  }

  static get EUROPE () {
    return 'europe'
  }

  static get ASIA () {
    return 'asia'
  }

  static get AMERICA () {
    return 'america'
  }

  static get ROW () {
    return 'row'
  }

  static get FACTOR_KEYS () {
    let ifactor = ImpactFactor.createFromObj(
      LABWARES_FACTORS['glass.europe.manufacturing']
    )
    return Object.keys(ifactor.decomposition['2023'])
  }

  static get AUTOCLAVE_NCYCLE_DEFAULT () {
    // MICALIS value
    return AUTOCLAVE_NCYCLE_DEFAULT
  }

  static get MANUFACTURING_AREA () {
    return [PlasticLabWare.EUROPE, PlasticLabWare.ASIA, PlasticLabWare.AMERICA]
  }

  static createFromObj (item) {
    return new LabWare(
      item.weight,
      item.type,
      item.manufacturingArea,
      item.sterilized,
      item.nautoclaveCycleAWeek,
      item.autoclaveWeight,
      item.autoclaveVolume
    )
  }
}

export class PlasticLabWare extends LabWare {
  constructor (
    weight,
    type = PlasticLabWare.POLYSTYRENE,
    country = 'fr',
    manufacturingArea = LabWare.EUROPE,
    sterilized = true,
    decontamination = true,
    nautoclaveCycleAWeek = null,
    autoclaveWeight = null,
    autoclaveVolume = null
  ) {
    super(
      weight,
      type,
      country,
      manufacturingArea,
      sterilized,
      decontamination,
      nautoclaveCycleAWeek,
      autoclaveWeight,
      autoclaveVolume
    )
  }

  getMouldingImpact (year = null) {
    let ifactors = ImpactFactor.createFromObj(
      LABWARES_FACTORS['moulding.' + this.manufacturingKey]
    )
    return ImpactIntensity.compute(this.weightkg, ifactors)
  }

  getSterilisationImpact (year = null) {
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.sterilized) {
      let eef = ImpactFactor.createFromObj(
        LABWARES_FACTORS['electricity.' + this.manufacturingArea]
      )
      impacts.add(
        ImpactIntensity.compute(this.weightkg * EBEAM_CONSUMPTION, eef)
      )
    }
    return impacts.sum()
  }

  getDecontaminationImpact (year = null) {
    let wef = ImpactFactor.createFromObj(LABWARES_FACTORS['deionised.water'])
    let pef = ImpactFactor.createFromObj(LABWARES_FACTORS['trajet.preparateur'])
    let eef = ImpactFactor.createFromObj(
      LABWARES_FACTORS['electricity.' + this.country]
    )
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.decontamination) {
      impacts.add(ImpactIntensity.compute(21, eef))
      impacts.add(ImpactIntensity.compute(AUTOCLAVE_WATER_DEFAULT, wef))
      impacts.add(ImpactIntensity.compute(0.2, pef))
      impacts.add(
        ImpactIntensity.compute(1, this.autoclaveImpactFactor).divide(
          this.autoclaveUsage
        )
      )
    }
    return impacts
      .sum()
      .divide(15 / this.weightkg)
      .multiply(this.weightkg)
  }

  getManufacturingImpact (year = null) {
    return this.__getManufacturingImpact(year)
  }

  getEOLImpact (year = null) {
    return this.__getEOLImpact(year)
  }

  getTransportImpact (year = null) {
    return this.__getTransportImpact(year)
  }

  getManufacturingChartImpact (year = null) {
    let impacts = new ImpactIntensities()
    // add production
    impacts.add(this.getManufacturingImpact(year))
    // add moulding impact
    impacts.add(this.getMouldingImpact(year))
    // add sterilisation impact
    impacts.add(this.getSterilisationImpact(year))
    return impacts.sum()
  }

  getImpact (year = null) {
    let impacts = new ImpactIntensities()
    // add production
    impacts.add(this.getManufacturingImpact(year))
    // add eol impact
    impacts.add(this.getEOLImpact(year))
    // add moulding impact
    impacts.add(this.getMouldingImpact(year))
    // add sterilisation impact
    if (this.sterilized) {
      impacts.add(this.getSterilisationImpact(year))
    }
    // add transportation impact
    impacts.add(this.getTransportImpact(year))
    // add decontamination impact
    if (this.decontamination) {
      impacts.add(this.getDecontaminationImpact(year))
    }
    return impacts.sum()
  }

  static get POLYSTYRENE () {
    return 'polystyrene'
  }

  static get POLYCARBONATE () {
    return 'polycarbonate'
  }

  static get POLYPROPENE () {
    return 'polypropene'
  }

  static get TYPES () {
    return [
      PlasticLabWare.POLYSTYRENE,
      PlasticLabWare.POLYCARBONATE,
      PlasticLabWare.POLYPROPENE
    ]
  }

  static createFromObj (item) {
    return new PlasticLabWare(
      item.weight,
      item.type,
      item.country,
      item.manufacturingArea,
      item.sterilized,
      item.decontamination,
      item.nautoclaveCycleAWeek,
      item.autoclaveWeight,
      item.autoclaveVolume
    )
  }
}

export class GlassLabWare extends LabWare {
  constructor (
    weight,
    name,
    country = 'fr',
    manufacturingArea = LabWare.EUROPE,
    sterilized = true,
    decontamination = true,
    filling = 80,
    prewash = false,
    hotWaterUsed = false,
    hotWaterVolume = 2.5,
    heating = 'electric',
    ngloves = 1,
    nreuses = 31,
    nautoclaveCycleAWeek = LabWare.AUTOCLAVE_NCYCLE_DEFAULT,
    autoclaveWeight = null,
    autoclaveVolume = null
  ) {
    super(
      weight,
      'glass',
      country,
      manufacturingArea,
      sterilized,
      decontamination,
      nautoclaveCycleAWeek,
      autoclaveWeight,
      autoclaveVolume
    )
    this.name = name
    this.filling = filling
    this.prewash = prewash
    this.hotWaterUsed = hotWaterUsed
    this.hotWaterVolume = hotWaterVolume
    this.heating = heating
    this.ngloves = ngloves
    this.nreuses = nreuses
  }

  get autoclaveCapacity () {
    return GlassLabWare.DEFAULT_CONFIGURATION[this.name].autoclaveCapacity
  }

  get autoclaveConsumption () {
    // MICALIS average value (kWh per labware)
    return GlassLabWare.DEFAULT_CONFIGURATION[this.name].autoclaveConsumption
  }

  get labwasherCapacity () {
    // MICALIS average value
    return GlassLabWare.DEFAULT_CONFIGURATION[this.name].labwasherCapacity
  }

  get volume () {
    // MICALIS average value
    return GlassLabWare.DEFAULT_CONFIGURATION[this.name].volume
  }

  get autoclaveCapacityUser () {
    let capacity = this.autoclaveCapacity
    if (this.autoclaveWeight) {
      capacity = {
        'tube.10': this.autoclaveWeight / 0.379,
        'tube.20': this.autoclaveWeight / 0.529,
        'tube.30': this.autoclaveWeight / 0.69,
        'erlenmeyer.100': this.autoclaveWeight / 4,
        'erlenmeyer.250': this.autoclaveWeight / 7.75,
        'erlenmeyer.500': this.autoclaveWeight / 12.64,
        'erlenmeyer.1000': this.autoclaveWeight / 20.72,
        'erlenmeyer.2000': this.autoclaveWeight / 38.1,
        'erlenmeyer.5000': this.autoclaveWeight / 80,
        'pipette.2': this.autoclaveWeight / 0.42,
        'pipette.5': this.autoclaveWeight / 0.595,
        'pipette.10': this.autoclaveWeight / 1.116,
        'pipette.25': this.autoclaveWeight / 1.786
      }[this.name]
    } else if (this.autoclaveVolume) {
      capacity = {
        'tube.10': this.autoclaveVolume / 0.227,
        'tube.20': this.autoclaveVolume / 0.317,
        'tube.30': this.autoclaveVolume / 0.416666667,
        'erlenmeyer.100': this.autoclaveVolume / 2.4,
        'erlenmeyer.250': this.autoclaveVolume / 4.04,
        'erlenmeyer.500': this.autoclaveVolume / 6.61,
        'erlenmeyer.1000': this.autoclaveVolume / 10.96,
        'erlenmeyer.2000': this.autoclaveVolume / 22.96,
        'erlenmeyer.5000': this.autoclaveVolume / 48,
        'pipette.2': this.autoclaveVolume / 0.252,
        'pipette.5': this.autoclaveVolume / 0.357,
        'pipette.10': this.autoclaveVolume / 0.67,
        'pipette.25': this.autoclaveVolume / 1.071
      }[this.name]
    }
    return capacity
  }

  get detergentAmount () {
    return this.dippingWaterAmount / 20
  }

  get dippingWaterAmount () {
    if (
      ['pipette.5', 'pipette.2', 'pipette.10', 'pipette.25'].includes(this.name)
    ) {
      return this.volume * 3
    } else if (['tube.10', 'tube.20', 'tube.30'].includes(this.name)) {
      return this.volume * 2
    } else {
      return this.volume
    }
  }

  getPrewashImpact (year = null) {
    let pef = ImpactFactor.createFromObj(LABWARES_FACTORS['trajet.preparateur'])
    let wef = ImpactFactor.createFromObj(LABWARES_FACTORS['potable.water'])
    let def = ImpactFactor.createFromObj(LABWARES_FACTORS['detergent'])
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.prewash) {
      if (
        [
          'tube.10',
          'tube.20',
          'tube.30',
          'pipette.2',
          'pipette.5',
          'pipette.10',
          'pipette.25'
        ].includes(this.name)
      ) {
        impacts.add(ImpactIntensity.compute(1 / 120, pef))
        impacts.add(ImpactIntensity.compute(2.5, wef))
        impacts.add(ImpactIntensity.compute(0.000125, def))
      } else if (this.name.startsWith('erlenmeyer.')) {
        let volume = parseFloat(this.name.split('.')[1]) / 1000 / 10
        impacts.add(ImpactIntensity.compute(1 / 60, pef))
        impacts.add(ImpactIntensity.compute(2.5, wef))
        impacts.add(ImpactIntensity.compute(volume, def))
      }
    }
    return impacts.sum()
  }

  getAutoclaveImpact (year = null) {
    let pef = ImpactFactor.createFromObj(LABWARES_FACTORS['trajet.preparateur'])
    let wef = ImpactFactor.createFromObj(LABWARES_FACTORS['deionised.water'])
    let eef = ImpactFactor.createFromObj(
      LABWARES_FACTORS['electricity.' + this.country]
    )
    let impacts = new ImpactIntensities()
    let impactsw = new ImpactIntensities()
    impactsw.add(ImpactIntensity.compute(AUTOCLAVE_WATER_DEFAULT, wef))
    impactsw.add(ImpactIntensity.compute(1, pef))
    impacts.add(
      impactsw.sum().divide(this.autoclaveCapacity * this.filling * 0.01)
    )
    impacts.add(
      ImpactIntensity.compute(
        1 / this.autoclaveUsage,
        this.autoclaveImpactFactor
      ).divide(this.autoclaveCapacityUser * this.filling * 0.01)
    )
    impacts.add(ImpactIntensity.compute(this.autoclaveConsumption, eef))
    return impacts.sum()
  }

  getSterilisationImpact (year = null) {
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.sterilized) {
      impacts.add(this.getAutoclaveImpact(year))
    }
    return impacts.sum()
  }

  getDecontaminationImpact (year = null) {
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.decontamination) {
      impacts.add(this.getAutoclaveImpact(year))
    }
    return impacts.sum()
  }

  getWashingImpact (year = null) {
    let pef = ImpactFactor.createFromObj(LABWARES_FACTORS['trajet.preparateur'])
    let wef = ImpactFactor.createFromObj(LABWARES_FACTORS['potable.water'])
    let def = ImpactFactor.createFromObj(LABWARES_FACTORS['detergent'])
    let dwef = ImpactFactor.createFromObj(LABWARES_FACTORS['deionised.water'])
    let lwef = ImpactFactor.createFromObj(LABWARES_FACTORS['lab.washer'])
    let eef = ImpactFactor.createFromObj(
      LABWARES_FACTORS['electricity.' + this.country]
    )
    let impacts = new ImpactIntensities()
    impacts.add(ImpactIntensity.compute(1 / LAB_WASHER_USAGE, lwef))
    impacts.add(
      ImpactIntensity.compute(
        LAB_WASHER_CONSUMPTION / this.labwasherCapacity,
        eef
      )
    )
    impacts.add(ImpactIntensity.compute(LAB_WASHER_DEIOWATER_DEFAULT, dwef))
    impacts.add(ImpactIntensity.compute(LAB_WASHER_WATER_DEFAULT, wef))
    impacts.add(ImpactIntensity.compute(LAB_WASHER_DETERGENT_DEFAULT, def))
    impacts.add(ImpactIntensity.compute(this.ngloves, this.gloveImpactFactor))
    impacts.add(ImpactIntensity.compute(1, pef))
    return impacts.sum().divide(this.labwasherCapacity)
  }

  getDippingImpact (year = null) {
    let wef = ImpactFactor.createFromObj(LABWARES_FACTORS['potable.water'])
    let def = ImpactFactor.createFromObj(LABWARES_FACTORS['tdf4'])
    let impacts = new ImpactIntensities()
    impacts.add(ImpactIntensity.compute(this.dippingWaterAmount, wef))
    impacts.add(ImpactIntensity.compute(this.detergentAmount, def))
    return impacts.sum()
  }

  getWaterHeatingImpact (year = null) {
    let impacts = new ImpactIntensities(LabWare.FACTOR_KEYS)
    if (this.prewash && this.hotWaterUsed) {
      if (this.heating === 'gaz') {
        let gef = ImpactFactor.createFromObj(LABWARES_FACTORS['gaz'])
        impacts.add(ImpactIntensity.compute(HOT_WATER_CONSUMPTION, gef))
      } else {
        let eef = ImpactFactor.createFromObj(
          LABWARES_FACTORS['electricity.' + this.country]
        )
        impacts.add(ImpactIntensity.compute(HOT_WATER_CONSUMPTION, eef))
      }
    }
    return impacts.sum().multiply(this.hotWaterVolume)
  }

  getManufacturingImpact (year = null) {
    return this.__getManufacturingImpact(year).divide(this.nreuses)
  }

  getEOLImpact (year = null) {
    return this.__getEOLImpact(year).divide(this.nreuses)
  }

  getTransportImpact (year = null) {
    return this.__getTransportImpact(year).divide(this.nreuses)
  }

  getManualWashingImpact (year = null) {
    let impacts = new ImpactIntensities()
    // add prewash impact
    impacts.add(this.getPrewashImpact(year))
    // add water heating impact
    impacts.add(this.getWaterHeatingImpact(year))
    return impacts.sum()
  }

  getAutoclaveChartImpact (year = null) {
    let impacts = new ImpactIntensities()
    // add sterilisation impact (autoclave)
    impacts.add(this.getSterilisationImpact(year))
    // add decontamination impact
    impacts.add(this.getDecontaminationImpact(year))
    return impacts.sum()
  }

  getImpact (year = null) {
    let impacts = new ImpactIntensities()
    // add production impact
    impacts.add(this.getManufacturingImpact(year))
    // add eol impact
    impacts.add(this.getEOLImpact(year))
    // add sterilisation impact (autoclave)
    impacts.add(this.getSterilisationImpact(year))
    // add washing impact
    impacts.add(this.getWashingImpact(year))
    // add dipping impact
    impacts.add(this.getDippingImpact(year))
    // add decontamination impact (same as sterilisation)
    impacts.add(this.getDecontaminationImpact(year))
    // add transportation impact
    impacts.add(this.getTransportImpact(year))
    // add manual washing impact
    impacts.add(this.getManualWashingImpact(year))
    return impacts.sum()
  }

  static get HEATING_TYPES () {
    return ['electric', 'gaz']
  }

  static get DEFAULT_CONFIGURATION () {
    // MICALIS average value
    return {
      'tube.10': {
        autoclaveCapacity: 2112,
        autoclaveConsumption: 0.01,
        labwasherCapacity: 1689,
        volume: 0.01
      },
      'tube.20': {
        autoclaveCapacity: 1512,
        autoclaveConsumption: 0.014,
        labwasherCapacity: 1210,
        volume: 0.02
      },
      'tube.30': {
        autoclaveCapacity: 1152,
        autoclaveConsumption: 0.018,
        labwasherCapacity: 1000,
        volume: 0.03
      },
      'erlenmeyer.100': {
        autoclaveCapacity: 200,
        autoclaveConsumption: 0.105,
        labwasherCapacity: 112,
        volume: 0.1
      },
      'erlenmeyer.250': {
        autoclaveCapacity: 105,
        autoclaveConsumption: 0.2,
        labwasherCapacity: 112,
        volume: 0.25
      },
      'erlenmeyer.500': {
        autoclaveCapacity: 72,
        autoclaveConsumption: 0.291,
        labwasherCapacity: 112,
        volume: 0.5
      },
      'erlenmeyer.1000': {
        autoclaveCapacity: 36,
        autoclaveConsumption: 0.583,
        labwasherCapacity: 36,
        volume: 1
      },
      'erlenmeyer.2000': {
        autoclaveCapacity: 21,
        autoclaveConsumption: 1.0,
        labwasherCapacity: 21,
        volume: 2
      },
      'erlenmeyer.5000': {
        autoclaveCapacity: 10,
        autoclaveConsumption: 2.1,
        labwasherCapacity: 10,
        volume: 5
      },
      'pipette.2': {
        autoclaveCapacity: 2380,
        autoclaveConsumption: 0.009,
        labwasherCapacity: 196,
        volume: 0.002
      },
      'pipette.5': {
        autoclaveCapacity: 1680,
        autoclaveConsumption: 0.012,
        labwasherCapacity: 196,
        volume: 0.005
      },
      'pipette.10': {
        autoclaveCapacity: 896,
        autoclaveConsumption: 0.023,
        labwasherCapacity: 64,
        volume: 0.01
      },
      'pipette.25': {
        autoclaveCapacity: 560,
        autoclaveConsumption: 0.037,
        labwasherCapacity: 64,
        volume: 0.025
      }
    }
  }

  static createFromObj (item) {
    return new GlassLabWare(
      item.weight,
      item.name,
      item.country,
      item.manufacturingArea,
      item.sterilized,
      item.decontamination,
      item.filling,
      item.prewash,
      item.hotWaterUsed,
      item.hotWaterVolume,
      item.heating,
      item.ngloves,
      item.nreuses,
      item.nautoclaveCycleAWeek,
      item.autoclaveWeight,
      item.autoclaveVolume
    )
  }
}
