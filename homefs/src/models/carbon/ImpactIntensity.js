/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

/** Represent the lca impact of an activity (travels, building). */
export class ImpactIntensity {
  /**
   *
   * Create a new ImpactIntensity
   *
   * @param {Dict} intensity
   *
   */
  constructor (intensity = {}) {
    this.intensity = intensity
  }

  multiply (mfactor) {
    let is = {}
    for (let key of Object.keys(this.intensity)) {
      is[key] = mfactor * this.intensity[key]
    }
    return new ImpactIntensity(is)
  }

  divide (dfactor) {
    let is = {}
    for (let key of Object.keys(this.intensity)) {
      is[key] = this.intensity[key] / dfactor
    }
    return new ImpactIntensity(is)
  }

  getImpactKeys () {
    return Object.keys(this.intensity)
  }

  static compute (value, factor) {
    let intensity = {}
    let f = factor.getFactor()
    for (let key of Object.keys(f)) {
      intensity[key] = value * f[key]
    }
    return new ImpactIntensity(intensity)
  }

  /**
   * Create from another object (typically from the database)
   *
   * @param {*} intensity
   */
  static createFromObj (intensity) {
    return new ImpactIntensity(intensity)
  }
}

/** Collection of ImpactIntensity.
 *
 */
export class ImpactIntensities {
  /**
   * Create and initialize the collection.
   */
  constructor (keys = []) {
    this._intensities = []
    // set default keys if provided
    this._keys = keys
  }

  add (intensity) {
    if (intensity instanceof ImpactIntensity) {
      this._intensities.push(intensity)
    } else if (intensity instanceof ImpactIntensities) {
      this._intensities.push(...intensity._intensities)
    }
  }

  sum () {
    let sum = {}
    if (this._intensities.length === 0) {
      for (let k of this._keys) {
        sum[k] = 0
      }
    }
    for (let cintensity of this._intensities) {
      for (let fkey of cintensity.getImpactKeys()) {
        if (Object.keys(sum).includes(fkey)) {
          sum[fkey] += cintensity.intensity[fkey]
        } else {
          sum[fkey] = cintensity.intensity[fkey]
        }
      }
    }
    return new ImpactIntensity(sum)
  }
}
