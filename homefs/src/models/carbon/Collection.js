import _ from 'lodash'

/**
 * NotImplemented error class for Abstract methods
 */
class NotImplemented extends Error {
  constructor (message = '', ...args) {
    super(message, ...args)
  }
}

/**
 * Manage a list of items in our application
 *
 * It serves several purposes:
 * (1) at data collection time to store the items currently added by the user
 *    - note: this may include invalid data
 * (2) at data retrieval time (from the backend) to reflect the items in the database
 *
 * Correctness
 *
 * Performance
 *
 * hash function
 *
 * The implementation is an hash table for efficient lookup if needed base on a hash function
 * A very precise hash leads to O(1) lookup (the datastructure acts as a
 * dictionary). On the contrary a imprecise hash function leads to O(n) lookup
 * (this mimics an array)
 *
 * Lookup is a 2 steps process: hash + equality test
 * In general the equality test is a weak equality test as items may be
 * incomplete (e.g the user currently filling her data) the equality test is
 * performed on a small subset of fields for instance.
 *
 * Implementations of collection must the hash and areEquals function
 *
 */
export class Collection {
  constructor () {
    // This will store the mapping hash(t) -> index
    this.indexes = new Map()
    // This will store the actual objects
    this.items = []
  }

  /**
   * Get the number of objects
   * @returns the length of the collection
   */
  get length () {
    return this.items.length
  }

  /**
   * Hash function. Has to be implemented by sub-classes.
   * @param {*} item
   * @returns a string
   */
  hash (item) {
    throw new NotImplemented('Abstract method hash has not been implemented')
  }

  /**
   * Base function for comparing items
   *
   * @param {*} item1
   * @param {*} item2
   * @returns true iff item1 and item2 are deemed equal
   */
  areSimilar (item1, item2) {
    throw new NotImplemented('Abstract method hash has not been implemented')
  }

  /**
   *
   * Count the number of item returned by the testFunction
   * @param {function(*): number} accessor the function is called on each item
   * @returns the number of items that makes the testFunction true
   */
  countItems (accessor = () => 1) {
    let nb = 0
    for (let item of this.items) {
      nb += accessor(item)
    }
    return nb
  }

  sortItems () {
    // pass
  }

  /**
   * Assuming the item is already in the list of items
   * make sure to have an index for it.
   *
   * @param {*} item
   */
  _addToIndex (item, index) {
    let h = this.hash(item)
    let indexes = this.indexes.get(h)
    if (indexes === undefined) {
      this.indexes.set(h, [])
      indexes = this.indexes.get(h)
    }
    indexes.push(index)
    // make explicit what we're doing
    // the above already does the job
    this.indexes.set(h, indexes)
  }

  /**
   * Forget about the index for item
   *
   * @param {*} item
   * @param {*} index
   */
  _removeFromIndex (item, index) {
    let h = this.hash(item)
    let indexes = this.indexes.get(h)

    if (indexes === undefined) {
      return
    }

    let iindex = indexes.indexOf(index)
    if (iindex >= 0) {
      indexes.splice(iindex, 1)
    }

    if (indexes.length === 0) {
      this.indexes.delete(h)
    }
  }

  /**
   * Add an item to the collection
   * The hash function is used to know whether we already have the travel recorded.
   * @param {*} item
   */
  add (item) {
    this.items.push(item)
    this._addToIndex(item, this.items.length - 1)
  }

  /**
   * The default update function that keeps reactive property set by vuejs intact.
   * @param {*} index
   * @param {*} ob
   */
  updateFnc (index, obj) {
    this.items.splice(index, 1, obj)
    // Object.assign(this.items[index], obj)
  }

  /**
   * Update an item of the collection.
   * This updates the first object that matches the passed item.
   * and update the index accordingly.
   *
   * @param {*} iobj an object with item, to fields
   */
  update (iobj) {
    let index = this.lookupIndex(iobj.from)
    if (index !== null) {
      // replace the item with the updated version
      // we use assign to keep the

      // beware this works in place so obj.from will be changed !
      this.updateFnc(index, iobj.to)
      // update the index with the new target

      this._removeFromIndex(iobj.from, index)
      this._addToIndex(iobj.to, index)
    }
  }

  /**
   * Reduce the item lists
   */
  reduceItems () {
    return this.items
  }

  /**
   * Some operations require to re-create the index.
   * For instance when we delete a travel the index will get out-of-sync
   * (because posterior index needs to be decremented by 1)
   *
   * O(n) complexity
   */
  reIndex () {
    this.indexes = new Map()
    for (let index in this.items) {
      this._addToIndex(this.items[index], index)
    }
  }

  /**
   * Delete all element which makes cb true.
   *
   * @param {*} cb
   */
  deleteBy (cb) {
    for (let index in this.items) {
      let item = this.items[index]
      if (cb(item)) {
        // set the current item to null
        this.items[index] = null
      }
    }

    // filter the non null items
    this.items = this.items.filter((item) => item !== null)

    this.reIndex()
  }

  /**
   * Delete one item.
   * The first equals to the passed item
   *
   * Complexity: O(n) worst case(when the element is found)
   * @param {*} item
   */
  delete (item) {
    let index = this.lookupIndex(item)
    // remove from index
    if (index != null) {
      this.items.splice(index, 1)
      this._removeFromIndex(item, index)
      this.reIndex()
    }
  }

  reset () {
    this.indexes = new Map()
    this.items = []
  }

  /**
   * Find the first index whose element matches obj
   * null if none can be found.
   *
   * Complexity: O(1)
   *
   * @param {*} obj
   * @returns the index where to find obj in the items (or an equal item)
   */
  lookupIndex (obj) {
    let indexes = this.indexes.get(this.hash(obj))
    if (indexes === undefined) {
      return null
    }
    for (let index of indexes) {
      let item = this.items[index]
      // return the first matching item that is similar
      if (this.areSimilar(item, obj)) {
        return index
      }
    }
    return null
  }

  /**
   *
   * Find if an item already exists into the collection
   *
   * O(1) complexity.
   * @param {*} item
   * @returns the item or null if it can't be found
   */
  lookup (item) {
    let index = this.lookupIndex(item)
    // beware that 0 is a valid index
    if (index !== null) {
      return this.items[index]
    }
    return null
  }

  /**
   * Allows to iterate natively on a collection
   */
  [Symbol.iterator] () {
    return this.items.values()
  }

  /**
   * Use it where vue and our legacy code expect an array
   * @returns an array of items
   */
  toArray () {
    return this.items
  }

  /**
   * Reduce in-place the collection
   */
  reduce () {
    let reduceditems = this.reduceItems()
    this.reset()
    for (let item of reduceditems) {
      this.add(item)
    }
  }

  // The following methods return an array
  // This allows to be compatible with the legacy implementation that uses array
  // as collections
  // Unfortunately this isn't really natural as one could expects
  // Collection.filter to return a Collection
  // FIXME: returns a collections (beware that the client code need probably
  // some change)
  filter (cb) {
    return this.items.filter(cb)
  }

  find (cb) {
    return this.items.find(cb)
  }

  map (cb) {
    return this.items.map(cb)
  }
}

export class ModuleCollection extends Collection {
  /**
   * Module items must have a descriptor getter
   * @returns
   */
  hash (item) {
    return item.descriptor
  }

  /**
   * Module items must have a isSimilar method
   * @param {*} item1
   * @param {*} item2
   * @returns
   */
  areSimilar (item1, item2) {
    return item1.isSimilarTo(item2)
  }

  sortItems () {
    return _.sortBy((item) => item.descriptor)
  }

  /**
   * reduce the items (before sending them to the backend)
   * the aggregation key should correspond to the fields in the DB  with an
   * unique constraint
   * @returns
   */
  reduceItems () {
    let reducedItems = []
    for (let item of this.items) {
      let index = reducedItems.findIndex((obj) => obj.sameAggregateAs(item))
      if (index < 0) {
        reducedItems.push(item)
      } else {
        reducedItems[index] = reducedItems[index].aggregate(item)
      }
    }
    return reducedItems
  }

  /**
   * Delete all the items for a given source
   *
   * FIXME: this assumes elements have a source attribute
   *
   *
   * @param {*} source
   */
  deleteSource (source) {
    this.deleteBy((t) => t.source === source)

    // Further delete data contained in item imported from the source
    // e.g buildings
    for (let item of this.items) {
      item.deleteSource(source)
    }
  }
  /**
   * Delete all items that are invalid
   *
   * FIXME: this assumes elements have an isInvalid method
   *
   * Complexity: O(n)
   *
   */
  deleteInvalid () {
    this.deleteBy((t) => t.isInvalid())
  }
}

/**
 * Manage a list of travels
 *
 * We keep an index up-to-date for efficient lookup
 *
 */
export class TravelCollection extends ModuleCollection {
  /**
   * Add a travel to the collection
   *
   * - travel correspond to one line in a uploaded file
   * in this case we lookup if it correspond to a previously added one.
   * in this case only the section are added to the lookep-up travel
   * - travel might correspond to full travel (with all the sections) coming
   * from the user form. In this case we also look up before.
   *
   * The hash function is used to know whether we already have the travel recorded.
   *
   * @param {*} travel
   */
  add (travel) {
    let t = this.lookup(travel)
    if (!t) {
      // travel isn't in the collection let's add it
      super.add(travel)
    } else {
      t.addTags(travel.tags)
      for (let section of travel.sections) {
        t.addSection(section)
      }
    }
  }

  /**
   * Sort the section for all travels in the collection
   */
  sortItems () {
    for (let t of this.items) {
      t.sortSections()
    }
  }
}
