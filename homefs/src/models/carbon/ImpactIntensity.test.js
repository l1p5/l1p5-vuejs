import {
  ImpactIntensity,
  ImpactIntensities
} from '@/models/carbon/ImpactIntensity.js'
import { ImpactFactor } from '@/models/carbon/Factor.js'

function impactFactorObj () {
  return ImpactFactor.createFromObj({
    decomposition: {
      2023: {
        'climate.change.human.health.long.term': 9.48e-7,
        'climate.change.human.health.short.term': 3.03e-7,
        'human.toxicity.cancer.long.term': 4.77e-12,
        'human.toxicity.cancer.short.term': 1.83e-9,
        'human.toxicity.non-cancer.long.term': 1.32e-19,
        'human.toxicity.non-cancer.short.term': 2.33e-10
      }
    }
  })
}

describe('ImpactIntensity', () => {
  test('creator with values', () => {
    let f = impactFactorObj()
    let i = ImpactIntensity.compute(0, f)
    expect(i.intensity['climate.change.human.health.long.term']).toBe(0)
    expect(i.intensity['human.toxicity.cancer.short.term']).toBe(0)
    let i2 = ImpactIntensity.compute(1, f)
    expect(i2.intensity['climate.change.human.health.long.term']).toBe(9.48e-7)
    expect(i2.intensity['human.toxicity.cancer.short.term']).toBe(1.83e-9)
  })
  test('test multiply', () => {
    let f = impactFactorObj()
    let i = ImpactIntensity.compute(1, f)
    expect(i.intensity['climate.change.human.health.long.term']).toBe(9.48e-7)
    expect(i.intensity['human.toxicity.cancer.short.term']).toBe(1.83e-9)
    let i2 = i.multiply(2)
    expect(i2.intensity['climate.change.human.health.long.term']).toBe(
      2 * 9.48e-7
    )
    expect(i2.intensity['human.toxicity.cancer.short.term']).toBe(2 * 1.83e-9)
  })
})

describe('ImpactIntensities', () => {
  test('test add', () => {
    let f = impactFactorObj()
    let i = ImpactIntensity.compute(1, f)
    let i2 = ImpactIntensity.compute(2, f)
    let is = new ImpactIntensities()
    is.add(i)
    is.add(i2)
    let ri = is.sum()
    expect(ri.intensity['climate.change.human.health.long.term']).toBe(
      3 * 9.48e-7
    )
    expect(ri.intensity['human.toxicity.cancer.short.term']).toBe(3 * 1.83e-9)
  })
  test('test add ImpactIntensities to ImpactIntensities', () => {
    let f = impactFactorObj()
    let i = ImpactIntensity.compute(1, f)
    let i2 = ImpactIntensity.compute(2, f)
    let i3 = ImpactIntensity.compute(3, f)
    let is1 = new ImpactIntensities()
    let is2 = new ImpactIntensities()
    is1.add(i)
    is1.add(i2)
    expect(is1._intensities.length).toBe(2)
    is2.add(i3)
    expect(is2._intensities.length).toBe(1)
    is1.add(is2)
    expect(is1._intensities.length).toBe(3)
    let ri = is1.sum()
    expect(ri.intensity['climate.change.human.health.long.term']).toBe(
      6 * 9.48e-7
    )
    expect(ri.intensity['human.toxicity.cancer.short.term']).toBe(6 * 1.83e-9)
  })
})
