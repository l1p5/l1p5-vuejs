import { readFile } from 'fs/promises'

import { CarbonIntensity } from './CarbonIntensity.js'
import GHGI from './GHGI.js'
import Modules from '../Modules.js'
import { ActiveModules } from '@/models/ActiveModules.js'
import { Boundary, Structure } from './Boundary.js'
import { Configuration } from './Configuration.js'

const path = require('path')

const fakeDataDir = path.resolve(__dirname, '../../..', 'backend/carbon/fake')
const FAKE_CONF_PATH = path.join(fakeDataDir, 'fakeConf.json')

describe('GHGI is safe without consumption data', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('', () => {
    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)

    let structure = Structure.fromNames(
      conf.POSITION_TITLES.value.map((p) => p.name)
    )
    structure
      .setNumber('pt.researcher', 1)
      .setNumber('pt.teacher', 1)
      .setNumber('pt.support', 1)
      .setNumber('pt.student-postdoc', 1)
    let boundary = new Boundary({
      structure,
      surveyLabelMapping: conf.CF_LABEL_MAPPING.value
    })
    let ghgi = new GHGI({
      boundary,
      travelPositionLabels: conf.TRAVEL_POSITION_LABELS.value,
      activeModulesObjs
    })
    // test that's it's safe to call this
    ghgi.compute(conf.toOptions())

    // and we get some correct computed values
    for (let module of Modules.getModules(true)) {
      expect(ghgi.synthesis[module]).toStrictEqual(new CarbonIntensity())
    }
  })
})
