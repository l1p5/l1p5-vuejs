import Purchase from './Purchase'
import { Tag } from '../core/TagCategory'
import Modules from '@/models/Modules.js'
import { ModuleCollection } from './Collection'

function pipette1 () {
  return Purchase.createFromObj({
    code: 'nb05',
    amount: '1',
    tags: [Tag.createFromObj('tag1')]
  })
}

function pipette2 () {
  return Purchase.createFromObj({
    code: 'nb05',
    amount: '2',
    tags: [Tag.createFromObj('tag1')]
  })
}

function pipette3 () {
  return Purchase.createFromObj({
    code: 'nb05',
    amount: '1',
    tags: [Tag.createFromObj('tag2')]
  })
}

function recharge () {
  return Purchase.createFromObj({
    code: 'ad25',
    amount: '1'
  })
}

describe('Reduce purchases', () => {
  test('Reduce two same purchases into one', () => {
    let p1 = pipette1()
    let p2 = pipette2()
    let p3 = recharge()

    let collection = new ModuleCollection()
    collection.add(p1)
    collection.add(p2)
    collection.add(p3)

    let reducedItems = collection.reduceItems()

    expect(reducedItems.length).toBe(2)
    expect(reducedItems.map((p) => p.code)).toEqual(['nb05', 'ad25'])
  })
})

describe('Reduce purchases', () => {
  test('Reduce two same purchases with different tags', () => {
    let p1 = pipette1()
    let p2 = pipette3()
    let p3 = recharge()

    let collection = new ModuleCollection()
    collection.add(p1)
    collection.add(p2)
    collection.add(p3)

    let reducedItems = collection.reduceItems()
    expect(reducedItems.length).toBe(3)
    expect(reducedItems.map((p) => p.code)).toEqual(['nb05', 'nb05', 'ad25'])
  })
})

describe('Getters', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.module).toBe(Modules.PURCHASES)
    expect(p.category).toBe('lab.equipment')
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.module).toBe(Modules.VEHICLES)
    expect(r.category).toBe('lab.life')
  })
})

describe('Compute carbon intensity', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.getCarbonIntensity(2022).intensity).toBe(0.63)
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.getCarbonIntensity(2022).intensity).toBe(0.56)
  })
})

describe('Export purchases', () => {
  test('Pipette1', () => {
    let p = pipette1()
    expect(p.toString()).toEqual(
      'NB05\tpurchases\t3.2\t4.2\tlab.equipment\tPipettes reutilisables\t1\ttag1\t0\t0'
    )
  })
  test('Recharge', () => {
    let r = recharge()
    expect(r.toString()).toBe(
      'AD25\tvehicles\t2.1\t2.1\tlab.life\tRecharge electrique pour vehicules\t1\t\t0\t0'
    )
  })
})
