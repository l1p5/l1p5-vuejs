import { Collection } from './Collection.js'

/**
 * A dummy collection that stores integer
 * as an Dict
 *
 */
class IntegerArrayCollection extends Collection {
  hash (item) {
    return item
  }

  areSimilar (item1, item2) {
    return item1 === item2
  }

  updateFnc (item, value) {
    this.items[item] = value
  }
}

/**
 * A dummy collection that stores integer
 * as an Array
 *
 */
class IntegerDictCollection extends Collection {
  hash (item) {
    return 0
  }

  areSimilar (item1, item2) {
    return item1 === item2
  }

  updateFnc (item, value) {
    this.items[item] = value
  }
}

class ObjectCollection extends Collection {
  hash (item) {
    if (item.id !== null) {
      return item.id
    } else {
      return item.uuid
    }
  }

  areSimilar (item1, item2) {
    return this.hash(item1) === this.hash(item2)
  }
}

describe('Delete some items', () => {
  it.each([IntegerArrayCollection, IntegerDictCollection])(
    'initial %s',
    (Collection) => {
      let c = new Collection()
      c.add(0)
      c.add(1)
      c.add(2)
      c.add(3)

      c.deleteBy((i) => i % 2 === 0)

      expect(c.length).toBe(2)
      expect(c.lookup(0)).toBeNull()
      expect(c.lookup(1)).not.toBeNull()
      expect(c.lookup(2)).toBeNull()
      expect(c.lookup(3)).not.toBeNull()
    }
  )

  test('delete with some identical items', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(2)
    c.add(3)

    c.deleteBy((i) => i % 2 === 0)

    expect(c.length).toBe(2)
    expect(c.lookup(0)).toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    expect(c.lookup(2)).toBeNull()
    expect(c.lookup(3)).not.toBeNull()
  })

  test('delete non existent item does not do anything', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(3)

    c.delete(4)

    expect(c.length).toBe(4)
    expect(c.lookup(0)).not.toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    expect(c.lookup(2)).not.toBeNull()
    expect(c.lookup(3)).not.toBeNull()
  })
})

describe('Delete single item', () => {
  test('delete one item', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(3)

    c.delete(2)

    expect(c.length).toBe(3)
    expect(c.lookup(0)).not.toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    expect(c.lookup(2)).toBeNull()
    expect(c.lookup(3)).not.toBeNull()
  })

  test('delete remote the a single instance of the matching element', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(2)
    c.add(3)

    c.delete(2)

    expect(c.length).toBe(4)
    expect(c.lookup(0)).not.toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    // one 2 is still here
    expect(c.lookup(2)).not.toBeNull()
    expect(c.lookup(3)).not.toBeNull()

    c.delete(2)
    expect(c.length).toBe(3)
    expect(c.lookup(2)).toBeNull()
  })
})

describe('Update single item', () => {
  test('update one item', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(3)

    c.update({ from: 2, to: 7 })

    expect(c.length).toBe(4)
    expect(c.lookup(0)).not.toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    expect(c.lookup(7)).not.toBeNull()
    expect(c.lookup(3)).not.toBeNull()
  })

  test('Update non existent item does not do anything', () => {
    let c = new IntegerArrayCollection()
    c.add(0)
    c.add(1)
    c.add(2)
    c.add(3)

    c.update({ from: 4, to: 7 })

    expect(c.length).toBe(4)
    expect(c.lookup(0)).not.toBeNull()
    expect(c.lookup(1)).not.toBeNull()
    expect(c.lookup(2)).not.toBeNull()
    expect(c.lookup(3)).not.toBeNull()
  })

  test('Update ObjectCollection', () => {
    let c = new ObjectCollection()

    c.add({ id: 'id1', uuid: null, value: 'a' })

    let item1 = c.items[0]

    let item2 = { id: null, uuid: 'uuid2', value: 'b' }
    c.update({ from: item1, to: item2 })

    expect(c.lookupIndex(item1)).toBeNull()
    expect(c.length).toBe(1)
    expect(c.lookup(item2)).not.toBeNull()
  })
})
