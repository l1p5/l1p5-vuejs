import TRANSPORTS_FACTORS from '@/../data/factors/carbon/transportsFactors.json'
import { EmissionFactor } from '@/models/carbon/Factor.js'

/**
 *
 * @param {Object} modifier key, value to modify in the default EmissionFactor Object
 *
 * @returns {EmissionFactor}
 */
function emissionFactorObj (modifier) {
  let ef = {
    decomposition: {
      2018: {
        total: {
          total: 0.2234,
          uncertainty: 0.6
        },
        combustion: {
          total: 0.162,
          uncertainty: 0.6
        },
        upstream: {
          total: 0.0358,
          uncertainty: 0.6
        },
        manufacturing: {
          total: 0.0256,
          uncertainty: 0.6
        }
      }
    }
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    ef[key] = value
  }
  return EmissionFactor.createFromObj(ef)
}

describe('EmissionFactor', () => {
  test('creator with default values', () => {
    let ef = EmissionFactor.createFromObj()
    expect(ef.hasDecomposition()).toBe(true)
    expect(ef.getFactor().total.total).toBe(0)
    expect(ef.getFactor().combustion.co2).toBe(0)
    expect(ef.getFactor().upstream.uncertainty).toBe(0)
    expect(ef.getFactor().manufacturing.other).toBe(0)
  })

  test('creator with 2018 car emissions', () => {
    let ef = emissionFactorObj()
    expect(ef.hasDecomposition()).toBe(true)
    expect(ef.getYear()).toBe('2018')
    expect(ef.getYear('2020')).toBe('2018')
    expect(ef.getDecompositionKeys().length).toBe(4)
    expect(ef.getDecompositionKeys()[0]).toBe('total')
    expect(ef.getDecompositionKeys()[2]).toBe('upstream')
    expect(ef.getFactor().total.total).toBe(0.2234)
    expect(ef.getFactor().combustion.co2).toBe(0)
    expect(ef.getFactor().upstream.uncertainty).toBe(0.6)
    expect(ef.getFactor().manufacturing.other).toBe(0)
  })
})

describe('test TRANSPORTS_FACTORS stability', () => {
  test('contrails uncertainty', () => {
    // reference: https://framagit.org/labos1point5/l1p5-vuejs/-/issues/286
    // We test that future updates of the transportation factors behave
    // correctly with regards to the uncertainty

    /**
     * (too) simple function that test some the emissions factors given by the key
     * Possible improvements would be to take the year into account.
     *
     * @param {*} key
     */
    function test (key) {
      let ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['plane'][key])
      let decomposition = ef.decomposition
      for (let year of Object.keys(decomposition)) {
        // console.log(year, decomposition[year])
        expect(decomposition[year].total.uncertainty).toBe(0.37)

        for (let k of ['combustion', 'upstream']) {
          expect(decomposition[year][k].uncertainty).toBe(0.1)
        }
        // this presence of this key seems to depends the year
        if (decomposition[year].manufacturing) {
          expect(decomposition[year].manufacturing.uncertainty).toBe(0.1)
        }
      }
    }

    test('shorthaul.contrails')
    test('mediumhaul.contrails')
    test('longhaul.contrails')
  })
})
