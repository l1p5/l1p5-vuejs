/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Réduire le chauffage',
    all: 'Tous',
    building: 'Nom du bâtiment',
    level1help:
      'Réduire la température de consigne de chauffe des bâtiments occupés.',
    level2label: '(bâtiment: <strong>__building__</strong>)',
    level2help:
      "Selectionner le bâtiment sur lequel filtrer l'application de la mesure de réduction.",
    description:
      'La consigne de chauffage correspond à la température à laquelle sont réglés les thermostats contrôlant le chauffage des bâtiments. Un <strong>abaissement de cette consigne</strong> permet de <strong>réduire la consommation énergétique</strong> et donc <strong>les émissions de GES</strong> induites. <br /> L’ADEME propose la règle simple suivante : abaisser la consigne de 1°C permet de réduire de 7% la consommation énergétique du chauffage et donc du même facteur les émissions de GES. Cette valeur est déterminée en faisant l’hypothèse d’une température intérieure de 19°C et une température hivernale moyenne en France métropolitaine de 6°C [1].',
    limits:
      'La valeur de 7% est une moyenne. Dans la réalité, le gain peut s’éloigner significativement de cette moyenne  en fonction de la température intérieure, de la température extérieure et des caractéristiques techniques du chauffage. <br /> La <strong>fourchette de variation</strong> est typiquement de <strong>6 à 11%</strong> [2].',
    rebounds:
      'La réduction de la température de consigne des thermostats d’un chauffage collectif peut entraîner l’utilisation ponctuelle de chauffages d’appoints électriques individuels même s’ils ne sont pas autorisés.',
    manual:
      'Le curseur permet de réduire la température de consigne de chauffage jusqu’à un maximum de 5°C, par pas de 0,5°C. De la même façon que pour la mesure « Isoler les bâtiments », il est possible d’appliquer cette mesure par bâtiments. Cette mesure est positionnée automatiquement après la mesure « Isoler les bâtiments » si cette dernière est choisie.',
    humanTitle: function (level1Human, level1Unit, level2Human) {
      return `Réduire la consigne de chauffe des bâtiments ${level2Human} à ${level1Human} ${level1Unit}`
    }
  },
  en: {
    title: 'Reduce heating',
    all: 'All',
    building: 'Building name',
    level1help: 'Lower the heating setpoint temperature in buildings occupied.',
    level2label: '(building: <strong>__building__</strong>)',
    level2help:
      'Select a building on which to filter the application of the mitigation measure.',
    description:
      'The heating setpoint temperature refers to the temperature at which thermostats controlling the building heating are set. <strong>Lowering this setpoint</strong> allows for <strong>reducing energy consumption</strong> and consequently <strong>GHG emissions</strong>. <br /> ADEME proposes the following simple rule: lowering the setpoint by 1°C reduces heating energy consumption by 7% and, consequently, GHG emissions by the same factor. This value is determined assuming an indoor temperature of 19°C and an average winter temperature in metropolitan France of 6°C [1].',
    limits:
      'The 7% value is an average. In reality, the actual savings can significantly deviate from this average depending on the indoor temperature, outdoor temperature, and technical characteristics of the heating system. <br /> The typical <strong>range of variation</strong> is from <strong>6% to 11%</strong> [2].',
    rebounds:
      'Reducing the setpoint temperature of thermostats in a central heating system may result in the occasional use of individual electric heaters, even if they are not permitted.',
    manual:
      'The slider allows you to lower the heating setpoint temperature by a maximum of 5°C, in increments of 0.5°C. Similar to the "Insulate buildings" measure, it is possible to apply this measure on a per-building basis. This measure is automatically positioned after the "Insulate buildings" measure if the latter is selected.',
    humanTitle: function (level1Human, level1Unit, level2Human) {
      return `Lower the heating setpoint temperature in buildings ${level2Human} at ${level1Human} ${level1Unit}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title:
      'Préconisation n° 35 : Baisser la température de consigne du chauffage',
    year: '2020',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://expertises.ademe.fr/entreprises-monde-agricole/performance-energetique-energies-renouvelables/comment-ameliorer-performance-energetique-lindustrie/preconisation-35'
  },
  '[2]': {
    title: '20 faits pour optimiser le chauffage et la climatisation',
    year: '2020',
    authors: 'xpair',
    link: 'https://conseils.xpair.com/consulter_savoir_faire/20-faits-optimiser-chauffage-climatisation/fait-12-1-degre-de-plus-chauffage-fait-augmenter.htm'
  }
}

export default class ReduceHeating extends Rule {
  name = 'ReduceHeating'
  module = Modules.HEATINGS
  lang = LANG
  references = REFERENCES

  level2 = {
    building: {
      values: function (buildings, otherrules = null) {
        let names = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.building.value)
          }
        }
        for (let building of buildings) {
          if (!names.map((obj) => obj.id).includes(building.name)) {
            names.push({
              id: building.name,
              label: building.name,
              isDisabled: used.includes(building.name)
            })
          }
        }
        return names
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1Min () {
    return 0
  }

  get level1Max () {
    return 5
  }

  get level1MaxValue () {
    return 5
  }

  get level1Step () {
    return 0.5
  }

  get level1Unit () {
    return '°C'
  }

  get level1Prefix () {
    return '-'
  }

  get forceAfterRule () {
    return 'InsulateBuilding'
  }

  get ticks () {
    return [0, 1, 2, 3, 4, 5]
  }

  compute (buildings) {
    let consumptionReductionFactor = 0.07 * this.level1
    let buildingsToModify = buildings
    if (this.level2.building.value.length > 0) {
      buildingsToModify = buildings.filter((obj) =>
        this.level2.building.value.includes(obj.name)
      )
    }
    for (let building of buildingsToModify) {
      for (let heating of building.heatings) {
        if (heating.isMonthly) {
          heating.january = (1 - consumptionReductionFactor) * heating.january
          heating.february = (1 - consumptionReductionFactor) * heating.february
          heating.march = (1 - consumptionReductionFactor) * heating.march
          heating.april = (1 - consumptionReductionFactor) * heating.april
          heating.may = (1 - consumptionReductionFactor) * heating.may
          heating.june = (1 - consumptionReductionFactor) * heating.june
          heating.july = (1 - consumptionReductionFactor) * heating.july
          heating.august = (1 - consumptionReductionFactor) * heating.august
          heating.septembre =
            (1 - consumptionReductionFactor) * heating.septembre
          heating.octobre = (1 - consumptionReductionFactor) * heating.octobre
          heating.novembre = (1 - consumptionReductionFactor) * heating.novembre
          heating.decembre = (1 - consumptionReductionFactor) * heating.decembre
        } else {
          heating.total = (1 - consumptionReductionFactor) * heating.total
        }
      }
    }
    return buildings
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let building of this.data) {
      for (let heating of building.heatings) {
        intensity.add(building.getHeatingCarbonIntensity(heating, this.year))
      }
    }
    return intensity
  }

  humanTitle (lang) {
    let level1Human = this.labelFormatter(this.level1)
    let level2Human = this.level2.building.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(
      level1Human,
      this.level1Unit,
      level2Human
    )
  }
}
