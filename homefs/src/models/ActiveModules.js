import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute.js'
import Food from '@/models/carbon/Food'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Purchase from '@/models/carbon/Purchase'
import Travel, { TravelWithLocations } from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'
import {
  ResearchActivity,
  researchActivityFactory
} from '@/models/carbon/ResearchActivity.js'

import carbonService from '@/services/carbonService'

import Modules from '@/models/Modules'

import _ from 'lodash'

/**
 * @typedef {Function} Serializer
 * @param {Object} payload json-like object most likely coming from the DB
 * @returns {Model} model object
 */

/**
 * @typedef {Function} Deserializer
 * @param {Model} model model to serialize
 * @returns {Object} json-like object ready to be sent on the wire
 */

/**
 * Container for all the active modules
 *
 */
class ActiveModules {
  static getClasse (module) {
    return {
      [Modules.BUILDINGS]: Building,
      [Modules.WATER]: Building,
      [Modules.CONSTRUCTION]: Building,
      [Modules.HEATINGS]: Building,
      [Modules.ELECTRICITY]: Building,
      [Modules.REFRIGERANTS]: Building,
      [Modules.PURCHASES]: Purchase,
      [Modules.DEVICES]: ComputerDevice,
      [Modules.VEHICLES]: Vehicle,
      [Modules.TRAVELS]: Travel,
      [Modules.COMMUTES]: Commute,
      [Modules.FOODS]: Food,
      [Modules.RACTIVITIES]: ResearchActivity
    }[module]
  }

  constructor () {
    this.modules = []
  }

  get types () {
    return _.uniq(this.modules.map((m) => m.type))
  }

  /**
   * Register a new module.
   * By doing so the vue component will show up in the web ui.
   *
   * Note that you can't add two modules with the same type.
   *
   * @param {Module} module  - module to register
   * @throws {Error} the module type already active
   */
  register (module) {
    let m = this.modules.find((m) => m.type === module.type)
    if (!m) {
      this.modules.push(module)
      return this
    }
    throw new Error(
      `can't add the module ${module.name}, type ${module.type} already exists`
    )
  }

  /**
   * Lookup a module by type (max 1)
   *
   * @param {string} type  - the type of the module.
   * @returns  {Module}
   */
  byType (type) {
    // might be heatings (or any submodule)
    let parentType = Modules.getParentModule(type)
    return this.modules.find((m) => m.type === parentType)
  }

  /**
   * Lookup a module by name (max 1)
   * @param {string} name  - name of the module (type/descriptor)
   *
   * @returns {Module|undefined}
   */
  byName (name) {
    return this.modules.find((m) => m.name === name)
  }

  /**
   * Lookup several modules by there names. Return only the elements that match.
   *
   * @param {string[]} names
   * @returns {Module}
   */
  byNames (names) {
    return this.modules.filter((m) => names.includes(m.name))
  }

  /**
   * Make the object iterable.
   *
   * @returns {Iterable.<Module>}
   */
  [Symbol.iterator] () {
    return this.modules.values()
  }

  /**
   * Build-up an ActiveModules based on the settings
   *
   * @param {Object[]} settings
   * @returns {ActiveModules}
   */
  static fromSettings (settings) {
    // get the names of the modules
    let names = settings.filter(
      (obj) => obj.section === 'global' && obj.name === 'ACTIVE_MODULES'
    )[0].value
    return ActiveModules.fromNames(names)
  }

  /**
   * Build-up an ActiveModules baed on the names
   *
   * @param {Object[]}} names
   * @returns {ActiveModules}
   */
  static fromNames (names) {
    let ml = new this()
    for (let module of ALL_MODULES.filter((m) => names.includes(m.name))) {
      ml.register(module)
    }
    return ml
  }
}

/**
 * Given a module type returns the associated save function.
 *
 * @param {string} module
 * @returns {Promise<Object>} promise that fulfilled in the data actually saved
 * for this module
 */
function defaultSaveFactory (module) {
  let func = null
  if (module === Modules.BUILDINGS) {
    func = carbonService.saveBuildings
  } else if (module === Modules.VEHICLES) {
    func = carbonService.saveVehicles
  } else if (module === Modules.PURCHASES) {
    func = carbonService.savePurchases
  } else if (module === Modules.DEVICES) {
    func = carbonService.saveComputerDevices
  } else if (module === Modules.TRAVELS) {
    func = carbonService.saveTravels
  } else if (module === Modules.COMMUTES) {
    func = carbonService.saveCommutes
  } else if (module === Modules.FOODS) {
    func = carbonService.saveFoods
  } else if (module === Modules.RACTIVITIES) {
    func = carbonService.saveResearchActivities
  }
  return func
}

/**
 * Represent a module to display and the associated in/out operations.
 *
 * In most of the situation we'll specify the klass.
 * In that case the in/out operaion is taken from createFromObj and toDatabase method.
 *
 * In rare situation you'll probably want to override those methods.
 *
 */
class Module {
  /**
   * constructor
   *
   * @param {string} name
   * @param {string} entryPoint
   * @param {*} klass - type Model class
   * @param {Serializer} dumps - serializer
   * @param {Deserializer} loads - deserializer
   *
   */
  constructor ({
    name = '',
    entryPoint = '',
    klass = null,
    dumps = null,
    loads = null,
    save = null
  } = {}) {
    if (!name || !entryPoint) {
      throw Error('Fields name, entryPoint, collection must be set')
    }
    this.name = name
    this.type = name.split('/')[0]

    this.entryPoint = entryPoint
    this.klass = klass || ActiveModules.getClasse(this.type)
    this.dumps = dumps || ((o) => o.toDatabase())
    let self = this
    this.loads = loads || ((o) => self.klass.createFromObj(o))
    this.save = save || defaultSaveFactory(this.type)
  }
}

/**
 * Definition of all possible modules that can be activated
 *
 */
const DEFAULT_MODULES = [
  new Module({
    name: 'buildings/form',
    entryPoint: () => import('@/components/carbon/forms/buildings/Main.vue')
  }),
  new Module({
    name: 'purchases/form',
    entryPoint: () => import('@/components/carbon/forms/purchases/Main.vue')
  }),
  new Module({
    name: 'devices/form',
    entryPoint: () => import('@/components/carbon/forms/devices/Main.vue')
  }),
  new Module({
    name: 'vehicles/form',
    entryPoint: () => import('@/components/carbon/forms/vehicles/Main.vue')
  }),
  new Module({
    name: 'travels/form',
    entryPoint: () => import('@/components/carbon/forms/travels/Main.vue'),
    loads: (o) => Travel.fromDatabase(o)
  }),
  new Module({
    name: 'commutes/survey',
    entryPoint: () => import('@/components/carbon/forms/commutes/Main.vue')
  }),
  new Module({
    name: 'foods/survey',
    entryPoint: () => import('@/components/carbon/forms/foods/Main.vue')
  }),
  new Module({
    name: 'ractivities/form',
    entryPoint: () => import('@/components/carbon/forms/ractivities/Main.vue'),
    // override since we need a factory for research activity
    loads: (o) => researchActivityFactory(o)
  })
]

// those modules are the default ones (e.g in the conf)
const DEFAULT_MODULE_NAMES = DEFAULT_MODULES.map((m) => m.name)

// For campus we might add the following
// new Module({name: 'commutes/form'}) => allowing to collect data by uploading
// a file (component similar to building, vehicles ...)

// For conference we might add the following
// new Module({name: 'travel/survey'}) => allowing to collect data by sharing a survey
// new Module({name: 'buildingd/simpleForm'}) => same as building but simpler
const ALL_MODULES = DEFAULT_MODULES.concat([
  new Module({
    name: 'travels/form/precise',
    entryPoint: () => import('@/components/carbon/forms/travels/Main.vue'),
    klass: TravelWithLocations,
    loads: (o) => TravelWithLocations.fromDatabase(o)
  })
])

export { ActiveModules, ALL_MODULES, DEFAULT_MODULE_NAMES }
