import { readFile } from 'fs/promises'

import Purchase from '../models/carbon/Purchase.js'
import ComputerDevice from '../models/carbon/ComputerDevice.js'
import Vehicle from '../models/carbon/Vehicle.js'
import Building from '../models/carbon/Building.js'
import Travel from '../models/carbon/Travel.js'
import {
  parseFile,
  intConverter,
  ERROR_MISSING_QUOTES,
  ERROR_MISSING_COLUMN,
  CriticalParsingError,
  ERROR_TOO_MANY_FIELDS,
  ERROR_TOO_FEW_FIELDS,
  ERROR_UNDETECTABLE_DELIMITER,
  ERROR_GENERIC
} from './parser'
import Modules from '../models/Modules.js'
import { setup } from '@ecodiag/ecodiag-lib'
import GES1p5FormatDescription from '@/components/carbon/forms/travels/formats/ges1p5/ges1p5'
import GESlabFormatDescription from '@/components/carbon/forms/travels/formats/geslab/geslab'
import HAVASFormatDescription from '@/components/carbon/forms/travels/formats/havas/havas'

const path = require('path')

function makeDescription () {
  return {
    field1: {
      pattern: /field1/,
      converter: intConverter,
      required: true
    },
    field2: {
      pattern: /field2/,
      required: true,
      default: 'forty-two'
    },

    field3: {
      pattern: /field3/,
      required: false,
      default: 'forty-three'
    }
  }
}

describe('parseFile', () => {
  test('test fields with/without quotes', async () => {
    expect.assertions(9)
    const description = makeDescription()

    // no quotes
    let file = `field1,field2
    0,a`
    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field1).toBe(0)
      expect(content[0].field2).toBe('a')
    })

    // same but with quoted inputs
    file = `field1,field2
    "0","a"`

    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field1).toBe(0)
      expect(content[0].field2).toBe('a')
    })

    // mixed quoted/unquoted
    file = `field1,field2
    "0",a`

    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field1).toBe(0)
      expect(content[0].field2).toBe('a')
    })

    // mixed quoted/unquoted
    // on purpose quote is kept (useful for ecodiag which interprets quotes as inch)
    file = `field1,field2
    "0",a"`

    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field1).toBe(0)
      expect(content[0].field2).toBe('a"')
    })

    // mixed quoted/unquoted
    // papa parse can't parse this
    file = `field1,field2
    "0","a`
    await parseFile(file, description).catch((error) =>
      expect(error.getErrors(ERROR_MISSING_QUOTES).length).toBe(1)
    )
  })

  test('test missing column', async () => {
    expect.assertions(2)

    const description = makeDescription()

    // missing one column
    let file = `field2
    "a`
    await parseFile(file, description).catch((error) =>
      expect(error).toStrictEqual(
        new CriticalParsingError(ERROR_MISSING_COLUMN)
      )
    )

    file = `f_i_e_l_d1,field2
    "0",a`
    await parseFile(file, description).catch((error) =>
      expect(error).toStrictEqual(
        new CriticalParsingError(ERROR_MISSING_COLUMN)
      )
    )
  })

  test('undetectable delimiter', async () => {
    expect.assertions(1)

    const description = makeDescription()
    let file = `field1,field2
    0\ta`
    await parseFile(file, description).catch((error) =>
      expect(error.getErrors(ERROR_UNDETECTABLE_DELIMITER).length).toBe(1)
    )
  })

  test('too few fields', async () => {
    expect.assertions(1)

    const description = makeDescription()
    let file = `field1,field2
    0`
    await parseFile(file, description).catch((error) =>
      expect(error.getErrors(ERROR_TOO_FEW_FIELDS).length).toBe(1)
    )
  })

  test('too many fields', async () => {
    expect.assertions(1)

    const description = makeDescription()
    let file = `field1,field2
    0,a,b`
    await parseFile(file, description).catch((error) =>
      expect(error.getErrors(ERROR_TOO_MANY_FIELDS).length).toBe(1)
    )
  })

  test('should apply default values for optional column', async () => {
    expect.assertions(2)

    const description = makeDescription()
    let file = `field1,field2
      0,a`
    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field2).toBe('a')
      expect(content[0].field3).toBe('forty-three')
    })
  })

  test('should apply default values', async () => {
    expect.assertions(2)

    const description = makeDescription()
    let file = `field1,field2,field3
      0,,b`
    await parseFile(file, description).then(([content, format]) => {
      expect(content[0].field2).toBe('forty-two')
      expect(content[0].field3).toBe('b')
    })
  })
})

describe('parseFile error handling', () => {
  test('callback throw an error', async () => {
    expect.assertions(1)
    const description = makeDescription()
    let file = `field1,field2,field3
      0,a,b`
    await expect(
      parseFile(file, description, (x) => {
        throw new Error('test')
      })
    ).rejects.toEqual(new CriticalParsingError(ERROR_GENERIC))
  })
})

describe('parsesPurchases', () => {
  test('purchasesTemplate.csv', async () => {
    expect.assertions(4)
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/purchasesTemplate.csv'
      ),
      'utf8'
    )
    await parseFile(datafile, Purchase.formatDescription).then(
      ([data, format]) => {
        let purchases = Purchase.createFromLines(data)
        expect(purchases.length).toBe(7)
        // sanity check on the first purchase
        let p = purchases[0]
        // code is lowered
        expect(p.code).toBe('aa64')
        expect(p.amount).toBe(1350)
        expect(p.isValid()).toBeTruthy()
      }
    )
  })

  test('purchase with no code is parsed but invalid', async () => {
    expect.assertions(2)
    let datafile = `codeNACRES;Total
    ;1350`
    await parseFile(datafile, Purchase.formatDescription).then(
      ([data, format]) => {
        let purchases = Purchase.createFromLines(data)
        expect(purchases.length).toBe(1)
        // sanity check on the first purchase
        let p = purchases[0]
        expect(p.isValid()).toBeFalsy()
      }
    )
  })

  test('purchase with no amount is parsed but invalid', async () => {
    expect.assertions(2)
    let datafile = `codeNACRES;Total
    AA64;`
    await parseFile(datafile, Purchase.formatDescription).then(
      ([data, format]) => {
        let purchases = Purchase.createFromLines(data)
        expect(purchases.length).toBe(1)
        // sanity check on the first purchase
        let p = purchases[0]
        expect(p.isValid()).toBeFalsy()
      }
    )
  })

  test('purchase with wrong code is parsed but invalid', async () => {
    expect.assertions(2)
    let datafile = `codeNACRES;Total
    XXXXX;1350`
    await parseFile(datafile, Purchase.formatDescription).then(
      ([data, format]) => {
        let purchases = Purchase.createFromLines(data)
        expect(purchases.length).toBe(1)
        // sanity check on the first purchase
        let p = purchases[0]
        expect(p.isValid()).toBeFalsy()
      }
    )
  })
})

describe('parsesDevices', () => {
  beforeEach(() => {
    // setup is used to setup the ecodiag lib
    // we mimic what we use on our side
    // see the devices components
    // FIXME(msimonin): move this initialization code in our model, it could
    // probably  run at import time
    setup({
      damping_factor: 1,
      ignore_year: true,
      get_default_model: function (type) {
        return 'default'
      }
    })
  })
  test('parseDevices', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/devicesTemplate.csv'
      ),
      'utf8'
    )
    await parseFile(datafile, ComputerDevice.formatDescription).then(
      ([data, format]) => {
        let devices = ComputerDevice.createFromLines(data)
        expect(devices.length).toBe(42)
        // TODO: this test fail
        // expect(devices.every((d) => d.isValid())).toBeTruthy()
        // sanity check on the first device
        let d = devices[0]
        expect(d.type).toBe('laptop')
        // DELL XPS 9365 fallback to default
        expect(d.model).toBe('default')

        // test screen
        // LCD MONITOR 20";DELL;ECRAN LCD
        d = devices[40]
        expect(d.type).toBe('screen')
        expect(d.model).toBe('screen_upto23')
      }
    )
  })

  test('Empty type unknown model is not valid', async () => {
    let datafile = `Modele;Fabricant;Type
    XXX;DELL;`
    await parseFile(datafile, ComputerDevice.formatDescription).then(
      ([data, format]) => {
        let devices = ComputerDevice.createFromLines(data)
        expect(devices.length).toBe(1)
        expect(devices[0].isValid()).toBeFalsy()
      }
    )
  })

  test('Empty model with type is valid', async () => {
    let datafile = `Modele;Fabricant;Type
    ;DELL;Fixe`
    await parseFile(datafile, ComputerDevice.formatDescription).then(
      ([data, format]) => {
        let devices = ComputerDevice.createFromLines(data)
        // rationale (not an empty string)
        expect(devices[0].model).toBe('default')
        expect(devices[0].type).toBe('desktop')
        expect(devices[0].isValid()).toBeTruthy()
      }
    )
  })
})

describe('parseVehicles', () => {
  test('vehiclesTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/vehiclesTemplate.tsv'
      ),
      'utf8'
    )
    expect.assertions(14)
    await parseFile(datafile, Vehicle.formatDescription)
      .then(([lines, format, source]) =>
        Vehicle.createFromLines(lines, format, [], source)
      )
      .then((vehicles) => {
        expect(vehicles.length).toBe(9)
        expect(vehicles[0].name).toBe('car1')
        expect(vehicles[0].engine).toBe('gasoline')
        expect(vehicles[0].type).toBe('car')
        expect(vehicles[0].consumption.total).toBe(1000)
        for (let vehicle of vehicles) {
          expect(vehicle.isValid()).toBeTruthy()
        }
      })
  })

  test('vehicle with no identifiant is parsed but invalid', async () => {
    let datafile = `Identifiant,Type,Motorisation,Distance
    ,voiture,essence,1000`
    expect.assertions(2)
    await parseFile(datafile, Vehicle.formatDescription)
      .then(([lines, format, source]) =>
        Vehicle.createFromLines(lines, format, [], source)
      )
      .then((vehicles) => {
        expect(vehicles.length).toBe(1)
        expect(vehicles[0].isValid()).toBeFalsy()
      })
  })

  test('vehicle with no type is parsed but invalid', async () => {
    let datafile = `Identifiant,Type,Motorisation,Distance
    car1,,essence,1000`
    expect.assertions(2)
    await parseFile(datafile, Vehicle.formatDescription)
      .then(([lines, format, source]) =>
        Vehicle.createFromLines(lines, format, [], source)
      )
      .then((vehicles) => {
        expect(vehicles.length).toBe(1)
        expect(vehicles[0].isValid()).toBeFalsy()
      })
  })

  test('vehicle with no motorisation is parsed but invalid', async () => {
    let datafile = `Identifiant,Type,Motorisation,Distance
    car1,voiture,,1000`
    expect.assertions(2)
    await parseFile(datafile, Vehicle.formatDescription)
      .then(([lines, format, source]) =>
        Vehicle.createFromLines(lines, format, [], source)
      )
      .then((vehicles) => {
        expect(vehicles.length).toBe(1)
        expect(vehicles[0].isValid()).toBeFalsy()
      })
  })

  test('vehicle with no distance is parsed but invalid', async () => {
    let datafile = `Identifiant,Type,Motorisation,Distance
    car1,voiture,essence,`
    expect.assertions(2)
    await parseFile(datafile, Vehicle.formatDescription)
      .then(([lines, format, source]) =>
        Vehicle.createFromLines(lines, format, [], source)
      )
      .then((vehicles) => {
        expect(vehicles.length).toBe(1)
        expect(vehicles[0].isValid()).toBeFalsy()
      })
  })
})

describe('parseBuldings', () => {
  test('buildingsTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/buildingTemplate.csv'
      ),
      'utf8'
    )
    await parseFile(datafile, Building.energyFormatDescription).then(
      ([data, format]) => {
        let buildings = Building.createFromLines(data, 'energies')
        expect(buildings.length).toBe(9)
        // sanity check on the two first buildings
        let b = buildings[0]
        expect(b.isValid()).toBeTruthy()
        expect(b.name).toBe('B1')
        expect(b.area).toBe(4500)
        expect(b.share).toBe(50)
        expect(b.electricity.total).toBe(100000)
        expect(b.water.total).toBe(1000)
        expect(b.selfConsumption).toBe(0)
        expect(b.constructionYear).toBe(2000)
        // 3
        expect(b.heatings.length).toBe(3)

        let ng = b.heatings.filter((h) => h.type === 'naturalgas')[0]
        expect(ng.total).toBe(50000)
        expect(ng.isOwnedByLab).toBeTruthy()

        let fioul = b.heatings.filter((h) => h.type === 'heating.oil')[0]
        expect(fioul.total).toBe(1000)
        expect(fioul.isOwnedByLab).toBeTruthy()

        let chips = b.heatings.filter((h) => h.type === 'wood.chips')[0]
        expect(chips.total).toBe(400)

        b = buildings[1]
        expect(b.isValid()).toBeTruthy()
        expect(b.name).toBe('B2')
        expect(b.area).toBe(2000)
        expect(b.share).toBe(80)
        expect(b.electricity.total).toBe(350000)
        expect(b.water.total).toBe(3000)
        expect(b.selfConsumption).toBe(200)
        expect(b.constructionYear).toBe(2001)

        expect(b.heatings.length).toBe(1)

        let hn = b.heatings.filter((h) => h.type === 'urban.network')[0]
        expect(hn.urbanNetwork).toBe('3107c')
        expect(hn.total).toBe(200000)
      }
    )
  })

  test('building with no name is parsed but invalid', async () => {
    expect.assertions(2)
    let datafile = `batiment;surface;année;occupation;electricité;autoproduction;eau;chauffage electrique;identifiant réseau de chaleur;consommation réseau de chaleur;gaz naturel;propane;fioul;biométhane;granulés;plaquettes;buches;propriétaire
;4500;2000;50;100000;;1000;;;;50000;;1000;;;400;;gaz,fioul
`
    await parseFile(datafile, Building.energyFormatDescription).then(
      ([data, format]) => {
        let buildings = Building.createFromLines(data, 'energies')
        expect(buildings.length).toBe(1)
        expect(buildings[0].isValid()).toBeFalsy()
      }
    )
  })

  test('building with no area is parsed but invalid', async () => {
    let datafile = `batiment;surface;année;occupation;electricité;autoproduction;eau;chauffage electrique;identifiant réseau de chaleur;consommation réseau de chaleur;gaz naturel;propane;fioul;biométhane;granulés;plaquettes;buches;propriétaire
B1;;2000;50;100000;;1000;;;;50000;;1000;;;400;;gaz,fioul
`
    await parseFile(datafile, Building.energyFormatDescription).then(
      ([data, format]) => {
        let buildings = Building.createFromLines(data, 'energies')
        expect(buildings.length).toBe(1)
        expect(buildings[0].isValid()).toBeFalsy()
      }
    )
  })

  test('refrigerant with no total is parsed but invalid', async () => {
    expect.assertions(3)

    let datafile = `nom du batiment;fluide clim;consommation
B1;;1`
    await parseFile(datafile, Building.refrigerantFormatDescription).then(
      ([data, format]) => {
        let refrigerants = Building.createFromLines(data, Modules.REFRIGERANTS)
        expect(refrigerants.length).toBe(1)
        // sanity check on the first refrigerant
        expect(refrigerants[0].building).toBe('B1')
        // name is lowered
        expect(refrigerants[0].valid).toBeFalsy()
      }
    )
  })

  test('refrigerantTemplate.csv', async () => {
    expect.assertions(5)

    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/refrigerantTemplate.csv'
      ),
      'utf8'
    )

    await parseFile(datafile, Building.refrigerantFormatDescription).then(
      ([data, format]) => {
        let refrigerants = Building.createFromLines(data, Modules.REFRIGERANTS)
        expect(refrigerants.length).toBe(8)
        // sanity check on the first refrigerant
        expect(refrigerants[0].building).toBe('B1')
        // name is lowered
        expect(refrigerants[0].value.name).toBe('r116')
        expect(refrigerants[0].value.total).toBe(1)
        expect(refrigerants[0].valid).toBeTruthy()
      }
    )
  })
})

// see https://framagit.org/labos1point5/l1p5-vuejs/-/issues/155
let fromGeslab = `Groupe labo%Numéro mission%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Moyens de transport%Nb de pers. dans la voiture%Aller / Retour%Motif du déplacement%Statut agent%Date de retour
0000%17713%2022-01-03%ORSAY%France%SANTA BARBARA%Etats-Unis%Avion%%OUI%Autres%%2022-01-28
0000%17657%2022-01-08%PETIT MARS%France%AUSSOIS%France%Taxi,Train%1%OUI%Colloques et congrés%%2022-01-15`

let fromHavas = `Client%Activité%Prestation%Date départ/début%Date retour/fin%Montant Net TTC%Lieu départ%Ville départ%Code pays départ%Pays départ%Lieu dest%Ville dest%Code pays dest%Pays dest%Continent dest%Routing%Routing pays%Axe%Type Trajet%Trajet complet%Hôtel / Prestataire
Numclient%Aérien%billet aérien%19/06/2022%24/06/2022%382 €%TOULOUSE%TOULOUSE%FR%France%BELGRADE%BELGRADE%RS%Serbie%Europe%TOULOUSE (FR)-BELGRADE (RS)%FRANCE-SERBIE%International%Aller-retour%1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE%BSP`

let fromHavas2 = `Client%Activité%Prestation%Date départ/début%Date retour/fin%Montant Net TTC%Lieu départ%Ville départ%Code pays départ%Pays départ%Lieu dest%Ville dest%Code pays dest%Pays dest%Continent dest%Routing%Routing pays%Axe%Type Trajet%Trajet complet%Hôtel / Prestataire
Numclient%Aérien%billet aérien%19/06/2022%24/06/2022%382 €%TOULOUSE%TOULOUSE%FR%France%BELGRADE%BELGRADE%RS%Serbie%Europe%TOULOUSE (FR)-BELGRADE (RS)%FRANCE-SERBIE%International%Aller-retour%1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE%BSP
Numclient%Aérien%billet aérien%19/06/2022%24/06/2022%382 €%TOULOUSE%TOULOUSE%FR%France%BELGRADE%BELGRADE%RS%Serbie%Europe%TOULOUSE (FR)-BELGRADE (RS)%FRANCE-SERBIE%International%Aller-retour%1:TOULOUSE -> BELGRADE%BSP
Numclient%Aérien%billet aérien%19/06/2022%24/06/2022%-382 €%TOULOUSE%TOULOUSE%FR%France%BELGRADE%BELGRADE%RS%Serbie%Europe%TOULOUSE (FR)-BELGRADE (RS)%FRANCE-SERBIE%International%Aller-retour%1:TOULOUSE -> BELGRADE%BSP`

// excerpt from public/static/carbon/travelsTemplate.tsv
let fromLabos1point5FR = `# mission%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Mode de déplacement%Nb de personnes dans la voiture%Aller Retour%Motif du déplacement%Statut de l'agent
1%24/01/2019%Grenoble%France%Lyon Saint-Exupéry%France%bus%%OUI%Colloque-congrès%ITA
1%24/01/2019%Lyon Saint Exupéry%FR%Londres%GB%avion%%OUI%Colloque-congrès%ITA
2%24/01/2019%Grenoble%France%Marseille%France%voiture%4%OUI%Colloque-congrès%ITA`

// same but with Engish columns names
let fromLabos1point5EN = `# trip%Departure date%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position
1%24/01/2019%Grenoble%France%Lyon Saint-Exupéry%France%bus%%OUI%Colloque-congrès%ITA
1%24/01/2019%Lyon Saint Exupéry%FR%Londres%GB%avion%%OUI%Colloque-congrès%ITA
2%24/01/2019%Grenoble%France%Marseille%France%voiture%4%OUI%Colloque-congrès%ITA`

// https://framagit.org/labos1point5/l1p5-vuejs/-/issues/318
let pathologicEmptyLine = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
,,,,,,,,,,,
`

let pathologicNoDepartureCity = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
1,03/01/2022,,France,Dublin,Irlande,Avion,,OUI,,,
`

let pathologicNoDepartureCountry = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
1,03/01/2022,Brest,,Dublin,Irlande,Avion,,OUI,,,
`

let pathologicNoDestinationCity = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
1,03/01/2022,Brest,France,,Irlande,Avion,,OUI,,,
`

let pathologicNoDestinationCountry = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
1,03/01/2022,Brest,France,Dublin,,Avion,,OUI,,,
`

let pathologicNoTransportation = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
1,03/01/2022,Brest,France,Dublin,Irlande,,,OUI,,,
`

// pathologic pathologic test round trip is assigned a default value somewhere
// let pathologicNoRoundTrip = `#mission,Date de départ,Ville de départ,Pays de départ,Ville de destination,Pays de destination,Mode de déplacement,Nb de personnes dans la voiture,Aller Retour (OUI si identiques, NON si différents),Motif du déplacement (optionnel),Statut
// 1,03/01/2022,Brest,France,Dublin,Irlande,Avion,,,,,
// `

function doParseLabo1point5 (file) {
  parseFile(
    file,
    GES1p5FormatDescription(
      { ita: 'travel.support' },
      { 'colloque-congres': 'conference' }
    )
  ).then(([content, format]) => {
    expect(content.length).toEqual(3)
    expect(content[0]).toStrictEqual({
      amount: 1,
      carpooling: 1,
      date: '24/01/2019',
      departureCity: 'Grenoble',
      departureCountry: 'FR',
      destinationCity: 'Lyon Saint-Exupéry',
      destinationCountry: 'FR',
      isRoundTrip: true,
      names: ['1'],
      purpose: 'conference',
      status: 'travel.support',
      transportation: 'busintercity',
      tags: []
    })
    // the is the second section of the 1st mission
    expect(content[1]).toStrictEqual({
      amount: 1,
      carpooling: 1,
      date: '24/01/2019',
      departureCity: 'Lyon Saint Exupéry',
      departureCountry: 'FR',
      destinationCity: 'Londres',
      destinationCountry: 'GB',
      isRoundTrip: true,
      names: ['1'],
      purpose: 'conference',
      status: 'travel.support',
      transportation: 'plane',
      tags: []
    })

    // The second mission
    expect(content[2]).toStrictEqual({
      amount: 1,
      carpooling: 4,
      date: '24/01/2019',
      departureCity: 'Grenoble',
      departureCountry: 'FR',
      destinationCity: 'Marseille',
      destinationCountry: 'FR',
      isRoundTrip: true,
      names: ['2'],
      purpose: 'conference',
      status: 'travel.support',
      transportation: 'car',
      tags: []
    })
  })
}

class DummyGeoDecoder {
  hit (item) {
    // all hits
    return {
      name: item.name,
      countryCodeAdded: 'TEST',
      lat: 12,
      lng: 23,
      city: item.name,
      country: item.country
    }
  }
  async warmup (lines) {
    // noop
  }
}

// never hit
class NullGeoDecoder {
  hit (item) {
    return null
  }
  async warmup (lines) {
    // noop
  }
}

describe('parseTravels', () => {
  test('travelsTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/travelsTemplate.csv'
      ),
      'utf8'
    )

    // dummy decoder
    expect.assertions(70)

    await parseFile(datafile, () => [GES1p5FormatDescription(), 'ges1p5'])
      .then(([data, format, source]) =>
        Travel.createFromLines(data, format, [], new DummyGeoDecoder(), source)
      )
      .then((travelSections) => {
        expect(travelSections.length).toBe(23)
        for (let travelSection of travelSections) {
          expect(travelSection.sections.length).toBe(1)
          expect(travelSection.sections[0].departureCityLat === 12).toBeTruthy()
          expect(travelSection.isValid()).toBeTruthy()
        }
      })
  })

  test.each([
    // for the folllowing case we mimic a failed decoding by using the null decoder
    ['empty line', pathologicEmptyLine, new NullGeoDecoder()],
    ['no departure city', pathologicNoDepartureCity, new NullGeoDecoder()],
    [
      'no departure country',
      pathologicNoDepartureCountry,
      new NullGeoDecoder()
    ],
    ['no destination city', pathologicNoDestinationCity, new NullGeoDecoder()],
    [
      'no destination country',
      pathologicNoDestinationCountry,
      new NullGeoDecoder()
    ],
    // for the following decoding is fine but the object is invalid

    // this one is pathopathologic as a default value will be applied
    // ['no round trip', pathologicNoRoundTrip, new DummyGeoDecoder()],
    ['no transportation', pathologicNoTransportation, new DummyGeoDecoder()]
  ])('pathologic case: %s', async (name, pathologic, decoder) => {
    expect.assertions(2)

    await parseFile(pathologic, () => [GES1p5FormatDescription(), 'ges1p5'])
      .then(([data, format, source]) =>
        Travel.createFromLines(data, format, [], decoder, source)
      )
      .then((travelSections) => {
        expect(travelSections.length).toBe(1)
        expect(travelSections[0].isValid()).toBeFalsy()
      })
  })
})

describe('parseTravels test format', () => {
  test('[fr] Labos1point5 import with tabs', () => {
    let txt = fromLabos1point5FR.replace(/%/g, '\t')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import with comma', () => {
    let txt = fromLabos1point5FR.replace(/%/g, ',')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import with semicolon', () => {
    let txt = fromLabos1point5FR.replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with tabs', () => {
    let txt = fromLabos1point5EN.replace(/%/g, '\t')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with comma', () => {
    let txt = fromLabos1point5EN.replace(/%/g, ',')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with semicolon', () => {
    let txt = fromLabos1point5EN.replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import no hyphen', () => {
    let txt = fromLabos1point5FR.replace('é', 'e').replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  test('geslab import', () => {
    // usual case: \t as separator
    let txt = fromGeslab.replace(/%/g, '\t')

    parseFile(
      txt,
      GESlabFormatDescription(
        {},
        { autres: 'other', 'colloques et congres': 'conference' }
      )
    ).then(([content, format, error]) => {
      // NOTE the returned data is more than an array of objects it has a columns
      // attribute which prevents us to use toStrictEqual on the whole data.
      expect(content.length).toEqual(2)
      expect(content[0]).toStrictEqual({
        amount: 1,
        carpooling: 1,
        date: '2022-01-03',
        departureCity: 'ORSAY',
        departureCountry: 'FR',
        destinationCity: 'SANTA BARBARA',
        destinationCountry: 'US',
        isRoundTrip: true,
        names: ['17713'],
        purpose: 'other',
        status: 'travel.unknown',
        transportation: 'plane',
        tags: []
      })
      expect(content[1]).toStrictEqual({
        amount: 1,
        carpooling: 1,
        date: '2022-01-08',
        departureCity: 'PETIT MARS',
        departureCountry: 'FR',
        destinationCity: 'AUSSOIS',
        destinationCountry: 'FR',
        isRoundTrip: true,
        names: ['17657'],
        purpose: 'conference',
        status: 'travel.unknown',
        transportation: 'train',
        tags: []
      })
    })
  })

  test('geslab import with wrong separator throws an exception', async () => {
    // space as a separator
    let txt = fromGeslab.replace(/%/g, ' ')
    expect(() => parseFile(txt, GESlabFormatDescription())).rejects.toThrow(
      new CriticalParsingError()
    )
  })

  test('havas import', () => {
    // usual case: \t as separator
    let txt = fromHavas.replace(/%/g, '\t')

    parseFile(txt, HAVASFormatDescription()).then(
      ([content, format, error]) => {
        expect(content.length).toEqual(4)
        expect(content[0]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '19/06/2022',
          departureCity: 'TOULOUSE',
          departureCountry: 'FR',
          destinationCity: 'AMSTERDAM',
          destinationCountry: null,
          isRoundTrip: false,
          names: '1',
          purpose: 'unknown',
          status: 'travel.unknown',
          transportation: 'plane',
          fulltrip:
            '1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE',
          tags: []
        })
        expect(content[1]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '19/06/2022',
          departureCity: 'AMSTERDAM',
          departureCountry: null,
          destinationCity: 'BELGRADE',
          destinationCountry: 'RS',
          isRoundTrip: false,
          names: '1',
          purpose: 'unknown',
          status: 'travel.unknown',
          transportation: 'plane',
          fulltrip:
            '1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE',
          tags: []
        })
        expect(content[2]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '19/06/2022',
          departureCity: 'BELGRADE',
          departureCountry: 'RS',
          destinationCity: 'PARIS ROISSY',
          destinationCountry: null,
          isRoundTrip: false,
          names: '1',
          purpose: 'unknown',
          status: 'travel.unknown',
          transportation: 'plane',
          fulltrip:
            '1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE',
          tags: []
        })
        expect(content[3]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '19/06/2022',
          departureCity: 'PARIS ROISSY',
          departureCountry: null,
          destinationCity: 'TOULOUSE',
          destinationCountry: 'FR',
          isRoundTrip: false,
          names: '1',
          purpose: 'unknown',
          status: 'travel.unknown',
          transportation: 'plane',
          fulltrip:
            '1:TOULOUSE -> AMSTERDAM - 2:AMSTERDAM -> BELGRADE - 3:[stop] BELGRADE -> PARIS ROISSY - 4:PARIS ROISSY -> TOULOUSE',
          tags: []
        })
      }
    )
  })

  test('havas import with reemboursement', () => {
    let txt = fromHavas2.replace(/%/g, '\t')

    parseFile(txt, HAVASFormatDescription()).then(
      ([content, format, error]) => {
        expect(content.length).toEqual(4)
      }
    )
  })

  /**
   * Intent: check that the name attribute of the error is localized
   *
   */
  test('Missing mandatory field throw an exception', () => {
    async function testError (txt, format, errcode, errparams) {
      // first check that the error is thrown
      expect(parseFile(txt, format)).rejects.toThrow(new Error(errcode))
      // second test that the params of the error is set correctly
      /* try {
        await parseFile(txt, format)
      } catch (e) {
        expect(e.params).toStrictEqual(errparams)
      } */
    }

    // geslab format
    let geslabData =
      'Groupe labo%not-a-valid-field%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Moyens de transport%Nb de pers. dans la voiture%Aller / Retour%Motif du déplacement%Statut agent%Date de retour'
    let gestlabTxt = geslabData.replace(/%/g, '\t')
    testError(
      gestlabTxt,
      GESlabFormatDescription(),
      'error-msg-missing-column',
      {
        name: 'Numéro mission'
      }
    )
    testError(
      gestlabTxt,
      GESlabFormatDescription(),
      'error-msg-missing-column',
      {
        name: '# trip'
      }
    )

    // labo1point5 format
    let l1p5Data = `not-a-valid-field%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Mode de déplacement%Nb de personnes dans la voiture%Aller Retour%Motif du déplacement%Statut de l'agent`
    let l1p5Txt = l1p5Data.replace(/%/g, '\t')
    testError(l1p5Txt, GES1p5FormatDescription(), 'error-msg-missing-column', {
      name: '# mission'
    })
    testError(l1p5Txt, GES1p5FormatDescription(), 'error-msg-missing-column', {
      name: '# trip'
    })

    // Note(msimonin). In english 'Roundtrip' matches both the names.pattern and the isRoundTrip pattern
    // so trying to call with the following won't throw any Exception
    // In other word a file with missing (# trip) field will be valid (like the following)
    // let l1p5DataEn = `not-a-valid-field%Departure date%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position`
    //
    // use another field as missing field
    let l1p5DataEn = `# trip%not-a-valid-field%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position`
    let l1p5TxtEn = l1p5DataEn.replace(/%/g, '\t')
    testError(
      l1p5TxtEn,
      GES1p5FormatDescription(),
      'error-msg-missing-column',
      {
        name: 'Date de départ'
      }
    )
    testError(
      l1p5TxtEn,
      GES1p5FormatDescription(),
      'error-msg-missing-column',
      {
        name: 'Departure date'
      }
    )
  })
})
