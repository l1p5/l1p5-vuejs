import GHGI from '@/models/carbon/GHGI'

import Building from '@/models/carbon/Building'
import Commute from '@/models/carbon/Commute'
import ComputerDevice from '@/models/carbon/ComputerDevice'
import Food from '@/models/carbon/Food'
import Purchase from '@/models/carbon/Purchase'
import Travel from '@/models/carbon/Travel'
import Vehicle from '@/models/carbon/Vehicle'
import { researchActivityFactory } from '../models/carbon/ResearchActivity'

import { Boundary } from '@/models/carbon/Boundary'
import Modules from '@/models/Modules'

import _ from 'lodash'

function loadFactory (module) {
  let func = null
  if (module === Modules.BUILDINGS) {
    func = Building.createFromObj
  } else if (module === Modules.VEHICLES) {
    func = Vehicle.createFromObj
  } else if (module === Modules.PURCHASES) {
    func = Purchase.createFromObj
  } else if (module === Modules.DEVICES) {
    func = ComputerDevice.createFromObj
  } else if (module === Modules.TRAVELS) {
    func = Travel.createFromObj
  } else if (module === Modules.COMMUTES) {
    func = Commute.createFromObj
  } else if (module === Modules.FOODS) {
    func = Food.createFromObj
  } else if (module === Modules.RACTIVITIES) {
    func = researchActivityFactory
  }
  return func
}

/**
 * Build up a GHGI from a json-like payload
 * corner-stone for building GHGI from the database
 *
 *
 * FIXME: expose a simpler API: makeGHGI(payload, conf)
 *
 * @param {Object} payload
 *
 */

function makeGHGI (payload, activeModulesObjs, surveyLabelMapping) {
  let p = _.cloneDeep(payload)
  // inject the position and purpose labels
  let ghgi = GHGI.createFromObj(p, activeModulesObjs)

  let b = Boundary.fromDatabase(payload.boundary, surveyLabelMapping)
  ghgi.boundary = b
  for (let module of Modules.getModules(false)) {
    if (!payload[module]) {
      // no data for this module
      continue
    }

    ghgi.add(module, payload[module])
  }
  return ghgi
}

function updateAllGHGI (payloads, activeModulesObjs, surveyLabelMapping) {
  let allGHGI = []
  for (let payload of payloads) {
    allGHGI.unshift(makeGHGI(payload, activeModulesObjs, surveyLabelMapping))
  }
  return allGHGI
}

export { makeGHGI, updateAllGHGI, loadFactory }
