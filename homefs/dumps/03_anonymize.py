# %%
from pathlib import Path
import pandas as pd

FIRST_YEAR = 2019
# excluded
LAST_YEAR = 2024

GHGI_COLUMNS_TO_DROP = [
    "entity.name",
    "entity.city",
    "entity.country",
    "entity.region",
]

SUFFIX = "20250223"
SOURCE_DIR = "data"
OUT_DIR = "out"

Path(OUT_DIR).mkdir(parents=True, exist_ok=True)


def path_pattern(year, module):
    return Path(f"{SOURCE_DIR}/{year}/GES1point5_{module}_{SUFFIX}.tsv")


def read_df(year, module):
    path = path_pattern(year, module)
    if not path.exists():
        return pd.DataFrame()
    return pd.read_csv(path, sep="\t")


# %%
modules = [
    ("vehicles", ["vehicles.submitted"], ["name", "tags"]),
    ("travels", ["travels.submitted"], ["name", "tags"]),
    # don't output the data for these modules
    # ("commutes", ["commutes.submitted"], ["seqid", "tags", "message"]),
    # ("foods", ["foods.submitted"], ["seqid", "tags", "message"]),
    (
        "buildings",
        [
            "heating.submitted",
            "electricity.submitted",
            "refrigerants.submitted",
            "water.submitted",
            "construction.submitted",
        ],
        ["name", "tags"],
    ),
    ("devices", ["devices.submitted"], ["tags"]),
    ("purchases", ["purchases.submitted"], ["tags"]),
    ("ractivities", ["ractivities.submitted"], ["tags"]),
]

#%%

for module, submitted_cols, cols_remove in modules:
    all_df = pd.DataFrame()
    print("module = ", module)
    for year in range(FIRST_YEAR, LAST_YEAR):
        print("\tyear = ", year)
        df_ghgis = read_df(year, "GHGIs")

        # retain the row if one of the current modules is submitted
        # e.g for building keep the row if the electricity is submitted or the
        # heating, or ..
        indexer = df_ghgis[submitted_cols[0]]
        for c in submitted_cols[1:]:
            indexer = indexer | df_ghgis[c]

        submitted = df_ghgis[indexer]["ghgi.uuid"]
        print(path_pattern(year, module))
        df = read_df(year, module)
        current = df[df["ghgi.uuid"].isin(submitted.values)].copy()
        current["year"] = year
        # print(current)
        all_df = pd.concat([current, all_df], axis=0, ignore_index=True)

    all_df.drop(cols_remove, axis=1).to_csv(
        f"{OUT_DIR}/GES1point5_{module}_{SUFFIX}.tsv", sep="\t", index=False
    )

# %%
# special case for commutes-aggregated
all_commutes = pd.DataFrame()
for year in range(FIRST_YEAR, LAST_YEAR):
    print("\tyear = ", year)
    df_ghgis = read_df(year, "GHGIs")
    submitted = df_ghgis[df_ghgis["commutes.submitted"]]["ghgi.uuid"]
    df_commutes = read_df(year, "commutes-aggregated")
    current = df_commutes[df_commutes["ghgi.uuid"].isin(submitted.values)]
    all_commutes = pd.concat([all_commutes, current], axis=0, ignore_index=True)

# reorder the columns (make sure ghgi.uuid is at the beginning)
columns_order = ["ghgi.uuid"] + [c for c in all_commutes.columns if c != "ghgi.uuid"]
all_commutes.to_csv(
    f"{OUT_DIR}/GES1point5_commutes-aggregated_{SUFFIX}.tsv",
    sep="\t",
    columns=columns_order,
    index=False,
)

# %%
# Finally aggregate all the GHGIs in a single file
all_ghgis = pd.DataFrame()
for year in range(FIRST_YEAR, LAST_YEAR):
    df_ghgis = read_df(year, "GHGIs").drop(GHGI_COLUMNS_TO_DROP, axis=1)
    # list of list
    submitted_cols = [m[1] for m in modules]
    # flatten
    submitted_cols = [c for cols in submitted_cols for c in cols]
    indexer = df_ghgis[submitted_cols[0]]
    for c in submitted_cols[1:]:
        indexer = indexer | df_ghgis[c]
    all_ghgis = pd.concat([all_ghgis, df_ghgis[indexer]], axis=0, ignore_index=True)

all_ghgis.to_csv(f"{OUT_DIR}/GES1point5_GHGIs_{SUFFIX}.tsv", sep="\t", index=False)
