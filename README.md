[![CI status](https://framagit.org/labos1point5/l1p5-vuejs/badges/main/pipeline.svg)](https://framagit.org/labos1point5/l1p5-vuejs/-/pipelines)
[![GPLv3 or later](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Chat](https://img.shields.io/badge/mattermost-dev1point5-blueviolet)](https://team.picasoft.net/labos-1point5/channels/dev-1point5)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://framagit.org/labos1point5/l1p5-vuejs.git/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://framagit.org/labos1point5/l1p5-vuejs.git)

Register to [mattermost](https://team.picasoft.net/signup_user_complete/?id=6r7cx4f1ufbu3n7ie95fytod3y)

# General information

This documentation is the **developper/operator**'s documentation of the tools developed by [Labos 1point5](https://labos1point5.org/) and available as a web app.

There are licensed under GPL-v3 or later.

[[_TOC_]]

# Architecture

* Frontend: VueJS (nodejs)
* Backend: Django (REST API) (python3)

# Configure the application

The application is tunable to some extent:

The file pointed by [`backend/settings/l1p5/l1p5.py`](backend/settings/l1p5/l1p5.py)
allows to configure the base entity for which GHGI will be assessed. By default,
it points to the configuration used here: https://apps.labos1point5.org

Feel free to adapt it to your need. If you are only testing the app, using the
default configuration is perfectly fine (and easier). You'll need to init the
application anyway (see below).

Once your custom configuration file created you need to symlink it: 

```bash
cd backend/settings/l1p5
# remove any existing configuration
rm l1p5.py
# create the symlink
ln -sf l1p5-mycustomconf.py l1p5.py` 
```

You then need to init the application (this will store the configuration in the
database)

```bash
# force overwrite the existing configuration (--help for other options)
python manage.py init --force
```

⚠️ Once init'ed it's not advised to change the configuration for the following keys -- unless you know what you're doing  or there's no data yet --:
- `L1P5_POSITION_TITLES`: the position titles used in the Boundary
- `L1P5_CF_LABEL_MAPPING`: the labels used in the food/commute survey and their corresponding position titles
- `L1P5_TRAVEL_POSITION_LABELS`: The position labels used in Travel forms.


# Dev Environment: manual Installation


To get ready with the development you'll need to have `NodeJS`, `Python3` and all their dependencies and a database.

## NodeJS

As a developper using a node version  manager like [nvm](https://github.com/nvm-sh/nvm) is encouraged.

In developement node version: 16

```bash
npm install
```

## Python3 environment

As a developper using a virtual environment for python is a good practice. We suggest to use the standard [`virtualenv`](https://docs.python.org/3/library/venv.html) or [`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv).

Dependencies are managed using `pip` however some extra system packages might be
necessary:

On Debian and derivatives ```python3-dev default-libmysqlclient-dev build-essential pkg-config```  (This is required to build `mysqclient` pip package)


```bash
# example with virtualenv
python3 -m venv [nom de du virtualenv]
source [nom du virtualenv]/bin/activate
pip install -r rootfs/opt/l1p5/requirements.txt
```

ℹ️ The `mysqlclient` dependency isn't mandatory for a development environment.
You can forget about it and use `sqlite3` as database instead.

## Database

For development purpose, one might use `sqlite3` or `MariaDB` as storage backend for Django.

### SQLite3

On Debian and derivatives, install the `sqlite3` package.  To use `sqlite3` as a
database, you need to set the environment variable `DATABASE_ENGINE` to
`django.db.backends.sqlite3`.

- Run the migration: `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py migrate`
- Initialize the app with the right entity (here Laboratory): `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py init`
- Run the server dev server: `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py runserver`

### MariaDB

On Debian and derivatives, install the `mariadb-server` package.
As MariaDB is the default database backend in `default.py` you can simply run the following

- Initialize the database (first time only):  `sudo mysql < mariadb/init.sql`
- Initialize the database for the tests(first time only):  `sudo mysql < mariadb/init_test.sql`
- Run the tests against MariaDB:  `python3 manage.py test`
- Run the migrations:  `python3 manage.py migrate`
- Initialize the app with the right entity (here Laboratory): `python3 manage.py init --force`



## Start the application (development)

```bash
cd homefs
npm run serve
```

Choose a configuration file and initialize the application.  The backend app
reads the file `backend/settings/l1p5.py` (which can be a symlink). Most likely
you'll want to change this file to fit your needs.

```
# in homefs/backend/settings
ln -sf <conf to apply> l1p5.py # remove the link before if needed

# read the configuration and apply the one-time setup
# in homefs
python manage.py init --force

python manage.py runserver
```

# Dev Environment: with docker


## Requirements

* Docker
* Docker compose

## Configuration

The configuration file `docker-compose.yml` has all of the configuration for the container's mount points and port forwarding. A separate database container is run in parallel and has preconfigured persistant volumes.
By default the following ports are available on the host machine:

* 7000 - The production build for the service running a WSGI process and Apache web server 
* 8080 - The vue.js development server, this will auto reload after any changes are made to front end code 
* 8000 - Django development server, will also auto reload with backend changes. This acts as the API port for development server

## Commands 

### Start the services
`docker compose up` = This will run in the foreground and will dump all stdout/stderr to the terminal 

`docker compose up -d` = This will run the services as a daemon in the background

### Stop the services 
`Ctrl-c` if running in the foreground 

`docker compose down` if in the background or from another terminal in the same directory as docker-compose.yml

### CLI 
When a service/container is already running `container` is the service name in docker-compose.yml

`docker compose exec <container> <command>` for example: `docker compose exec webserver bash`

When a service/container is not running:

`docker compose run <container> <command>`

## Logging 
Logs are in their normal places within the containers, but they are also piped to a local `log` directory mounted into the working directory of the webserver container. This is to ease debugging by accessing directly from the host. 

## Building 
Sometimes re-building the container becomes necessary i.e. a new package is added to the base image, or a python module is added to the backend server etc.

`docker compose build webserver` - will rebuild the image using the Dockerfile.


# Code style

We don't enforce (see below) coding style convention automatically but the following
is what we mainly use:

- javascript: prettier + lint
```bash
npx prettier --no-semi --single-quote --trailing-comma none  --write  <path to files or directory> && npm run lint
```

- python: black
```bash
black <path to file or directory to prettify>
```

## Pre-commits 

We plan to enforce automatic formatting/lint to release developper from this burden.
To do so we have a minimal configuration for [pre-commit](https://pre-commit.com/) (that runs git pre commits automatically).

```
pip install pre-commit

# install the pre-commit (opt-in)
pre-commit --install
```

# Tests

## Populate with some users/data

Check the  `python manage.py populate` command.

- `populate core`: create some users
- `populate carbon`: create some users and labs and GHGIs
- `populate transition`: create some users and labs and actions
- `populate carbon transition`: guess what ?

## Execution

Check `.gitlab-ci.yml`.

- Backend unit test:
    - (sqlite3) `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py test`
    - (mariaDB) `python3 manage.py test`
- Frontend unit test:
    - `npm test`

- Functionnal tests ([playwright](https://playwright.dev/python/))
    - They can be run inside VSCode with the `playwright` plugin. 
    Run `python manage.py populate carbon` before (the tests require users + lab to
    be created)
    - `./tests/runner.sh` (requires Docker)
    - or `npx playwright test`

- On tags (only), the docker image will be build and pushed on Inria registry
(see the [mirror](https://gitlab.inria.fr/l1p5/l1p5-vuejs)).

- Run the pipeline locally:
  - Install [`gitlab-ci-local`](https://github.com/firecow/gitlab-ci-local),
  - Run any job of the pipeline (or the whole pipeline): `gitlab-ci-local precheck` (precheck job)


# Tricks

## Don't run the test after pushing

Skip the tests:

```bash
git push -o ci.skip
```



## One liner to bootstrap the database

```bash
cd homefs
# initialize the DB and populate it with some users
# for instance this might be useful after dropping the DB
sudo mariadb -e "DROP DATABASE IF EXISTS l1p5;" && sudo mariadb < ../mariadb/init.sql  && python manage.py migrate && python manage.py init --force &&  python manage.py populate core
```

# Other docs

* Django : [https://www.djangoproject.com](https://www.djangoproject.com/)
* MariaDB : [https://devdocs.io/mariadb/](https://devdocs.io/mariadb/)
* VueJS : [https://vuejs.org/guide/introduction.html](https://vuejs.org/guide/introduction.html)
* [Docker startup (prod use)](./doc/docker.md)
